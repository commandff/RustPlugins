﻿namespace RustPluginsWatcher {
    partial class RustPluginsWatcher {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RustPluginsWatcher));
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.textBoxDestFolder = new System.Windows.Forms.TextBox();
            this.textBoxSourceFolder = new System.Windows.Forms.TextBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblDest = new System.Windows.Forms.Label();
            this.buttonBrowseSource = new System.Windows.Forms.Button();
            this.buttonBrowseDest = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lblLastChange = new System.Windows.Forms.Label();
            this.lblc = new System.Windows.Forms.Label();
            this.lblLastChangeFile = new System.Windows.Forms.Label();
            this.lblLastChangeTimestamp = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.linkLabelGitlab = new System.Windows.Forms.LinkLabel();
            this.buttonCopyAll = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.Filter = "*.cs";
            this.fileSystemWatcher1.NotifyFilter = ((System.IO.NotifyFilters)(((System.IO.NotifyFilters.Size | System.IO.NotifyFilters.LastWrite) 
            | System.IO.NotifyFilters.CreationTime)));
            this.fileSystemWatcher1.SynchronizingObject = this;
            this.fileSystemWatcher1.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Changed);
            // 
            // textBoxDestFolder
            // 
            this.textBoxDestFolder.Enabled = false;
            this.textBoxDestFolder.Location = new System.Drawing.Point(80, 30);
            this.textBoxDestFolder.Name = "textBoxDestFolder";
            this.textBoxDestFolder.Size = new System.Drawing.Size(382, 23);
            this.textBoxDestFolder.TabIndex = 0;
            // 
            // textBoxSourceFolder
            // 
            this.textBoxSourceFolder.Enabled = false;
            this.textBoxSourceFolder.Location = new System.Drawing.Point(80, 6);
            this.textBoxSourceFolder.Name = "textBoxSourceFolder";
            this.textBoxSourceFolder.Size = new System.Drawing.Size(382, 23);
            this.textBoxSourceFolder.TabIndex = 1;
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(1, 9);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(43, 15);
            this.lblSource.TabIndex = 2;
            this.lblSource.Text = "Source";
            // 
            // lblDest
            // 
            this.lblDest.AutoSize = true;
            this.lblDest.Location = new System.Drawing.Point(1, 33);
            this.lblDest.Name = "lblDest";
            this.lblDest.Size = new System.Drawing.Size(67, 15);
            this.lblDest.TabIndex = 3;
            this.lblDest.Text = "Destination";
            // 
            // buttonBrowseSource
            // 
            this.buttonBrowseSource.Location = new System.Drawing.Point(468, 6);
            this.buttonBrowseSource.Name = "buttonBrowseSource";
            this.buttonBrowseSource.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseSource.TabIndex = 4;
            this.buttonBrowseSource.Text = "Browse ...";
            this.buttonBrowseSource.UseVisualStyleBackColor = true;
            this.buttonBrowseSource.Click += new System.EventHandler(this.buttonBrowseSource_Click);
            // 
            // buttonBrowseDest
            // 
            this.buttonBrowseDest.Location = new System.Drawing.Point(468, 30);
            this.buttonBrowseDest.Name = "buttonBrowseDest";
            this.buttonBrowseDest.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseDest.TabIndex = 5;
            this.buttonBrowseDest.Text = "Browse ...";
            this.buttonBrowseDest.UseVisualStyleBackColor = true;
            this.buttonBrowseDest.Click += new System.EventHandler(this.buttonBrowseDest_Click);
            // 
            // lblLastChange
            // 
            this.lblLastChange.AutoSize = true;
            this.lblLastChange.Location = new System.Drawing.Point(1, 57);
            this.lblLastChange.Name = "lblLastChange";
            this.lblLastChange.Size = new System.Drawing.Size(75, 15);
            this.lblLastChange.TabIndex = 11;
            this.lblLastChange.Text = "Last Change:";
            // 
            // lblc
            // 
            this.lblc.AutoSize = true;
            this.lblc.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.lblc.Location = new System.Drawing.Point(389, 54);
            this.lblc.Name = "lblc";
            this.lblc.Size = new System.Drawing.Size(73, 12);
            this.lblc.TabIndex = 14;
            this.lblc.Text = "©2023 Yonneh.";
            // 
            // lblLastChangeFile
            // 
            this.lblLastChangeFile.AutoSize = true;
            this.lblLastChangeFile.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblLastChangeFile.Location = new System.Drawing.Point(80, 54);
            this.lblLastChangeFile.Name = "lblLastChangeFile";
            this.lblLastChangeFile.Size = new System.Drawing.Size(0, 12);
            this.lblLastChangeFile.TabIndex = 15;
            // 
            // lblLastChangeTimestamp
            // 
            this.lblLastChangeTimestamp.AutoSize = true;
            this.lblLastChangeTimestamp.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblLastChangeTimestamp.Location = new System.Drawing.Point(80, 65);
            this.lblLastChangeTimestamp.Name = "lblLastChangeTimestamp";
            this.lblLastChangeTimestamp.Size = new System.Drawing.Size(0, 12);
            this.lblLastChangeTimestamp.TabIndex = 16;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Segoe UI", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblVersion.Location = new System.Drawing.Point(290, 55);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(96, 11);
            this.lblVersion.TabIndex = 17;
            this.lblVersion.Text = "RustPluginsWatcher v o.0";
            // 
            // linkLabelGitlab
            // 
            this.linkLabelGitlab.AutoSize = true;
            this.linkLabelGitlab.Font = new System.Drawing.Font("Segoe UI", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.linkLabelGitlab.Location = new System.Drawing.Point(290, 63);
            this.linkLabelGitlab.Name = "linkLabelGitlab";
            this.linkLabelGitlab.Size = new System.Drawing.Size(165, 11);
            this.linkLabelGitlab.TabIndex = 13;
            this.linkLabelGitlab.TabStop = true;
            this.linkLabelGitlab.Text = "https://gitlab.com/commandff/RustPlugins/";
            this.linkLabelGitlab.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelGitlab_LinkClicked);
            // 
            // buttonCopyAll
            // 
            this.buttonCopyAll.Location = new System.Drawing.Point(468, 54);
            this.buttonCopyAll.Name = "buttonCopyAll";
            this.buttonCopyAll.Size = new System.Drawing.Size(75, 23);
            this.buttonCopyAll.TabIndex = 18;
            this.buttonCopyAll.Text = "Copy All";
            this.buttonCopyAll.UseVisualStyleBackColor = true;
            this.buttonCopyAll.Click += new System.EventHandler(this.buttonCopyAll_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 78);
            this.Controls.Add(this.buttonCopyAll);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblLastChangeTimestamp);
            this.Controls.Add(this.lblLastChangeFile);
            this.Controls.Add(this.lblc);
            this.Controls.Add(this.linkLabelGitlab);
            this.Controls.Add(this.lblLastChange);
            this.Controls.Add(this.buttonBrowseDest);
            this.Controls.Add(this.buttonBrowseSource);
            this.Controls.Add(this.lblDest);
            this.Controls.Add(this.lblSource);
            this.Controls.Add(this.textBoxSourceFolder);
            this.Controls.Add(this.textBoxDestFolder);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "RustPluginsWatcher";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private FileSystemWatcher fileSystemWatcher1;
        private Button buttonBrowseDest;
        private Button buttonBrowseSource;
        private Label lblDest;
        private Label lblSource;
        private TextBox textBoxSourceFolder;
        private TextBox textBoxDestFolder;
        private FolderBrowserDialog folderBrowserDialog1;
        private Label lblLastChangeTimestamp;
        private Label lblLastChangeFile;
        private Label lblc;
        private Label lblLastChange;
        private Label lblVersion;
        private Button buttonCopyAll;
        private LinkLabel linkLabelGitlab;
    }
}