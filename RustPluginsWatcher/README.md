### Rust Plugins Watcher v1.0.0.
![](https://i.imgur.com/Vg8rKQG.png)
- Compile in VS2022, and run; or download the .zip, extract, and run.
- This is an extremely simple desktop program; that monitors `*.cs` in the `Source` folder; and Copies them to the `Destination` folder, overwriting existing files, when they are changed.
- After a file is copied, it will not copy another for 2 seconds.
- It will refuse to copy an empty (0 byte) file.
- `Copy All` copies `*.cs` from `Source`, to `Destination`; overwriting existing files.
- it keeps a `Last Change` filename and timestamp on the UI, so you can see when it last copied a file.
- Pretty handy for Rust Plugin Development- automatically copies the .cs to a test server, when you save it. I'm sure there are other uses- for that, the source is included, so you can change the file types/logic/whatever you want.
- In the future, updated versions may be found at `https://gitlab.com/commandff/RustPlugins/`.
