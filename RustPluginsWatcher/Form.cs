/*
(Completely standard MIT License:)

Copyright 2023 "Yonneh"

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the �Software�), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


-------------------------------------
23 NOV 2023 - v 1.0.0
- Initial Release.
*/

using static System.Windows.Forms.AxHost;
using System.Configuration;

namespace RustPluginsWatcher {
    public partial class RustPluginsWatcher : Form {
        public RustPluginsWatcher() {
            InitializeComponent();
        }
        System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        private DateTime lastCopy;
        private void Form1_Load(object sender, EventArgs e) {
            textBoxSourceFolder.Text = ConfigurationManager.AppSettings.Get("SourceFolder") ?? "C:\\Projects\\RustPlugins";
            if (!Directory.Exists(textBoxSourceFolder.Text)) textBoxSourceFolder.Text = "";
            textBoxDestFolder.Text = ConfigurationManager.AppSettings.Get("DestFolder") ?? "C:\\steamcmd\\rust_server\\oxide\\plugins";
            if (!Directory.Exists(textBoxDestFolder.Text)) textBoxDestFolder.Text = "";
            if (textBoxSourceFolder.Text != "") fileSystemWatcher1.Path = textBoxSourceFolder.Text;
        }
        private void buttonBrowseSource_Click(object sender, EventArgs e) {
            folderBrowserDialog1.SelectedPath = textBoxSourceFolder.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK) {
                textBoxSourceFolder.Text = folderBrowserDialog1.SelectedPath;
                fileSystemWatcher1.Path = folderBrowserDialog1.SelectedPath;
                config.AppSettings.Settings.Remove("SourceFolder");
                config.AppSettings.Settings.Add("SourceFolder", folderBrowserDialog1.SelectedPath);
                config.Save(ConfigurationSaveMode.Full);
            }
        }
        private void buttonBrowseDest_Click(object sender, EventArgs e) {
            folderBrowserDialog1.SelectedPath = textBoxDestFolder.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK) {
                textBoxDestFolder.Text = folderBrowserDialog1.SelectedPath;
                config.AppSettings.Settings.Remove("DestFolder");
                config.AppSettings.Settings.Add("DestFolder", folderBrowserDialog1.SelectedPath);
                config.Save(ConfigurationSaveMode.Full);
            }
        }
        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e) {
            if (e.ChangeType != WatcherChangeTypes.Changed || DateTime.Now - lastCopy < TimeSpan.FromSeconds(2)) return;
            if (!Directory.Exists(textBoxDestFolder.Text)) return;
            var targetFN = Path.Join(textBoxDestFolder.Text, e.Name);
            var f = new FileInfo(e.FullPath);
            if (f.Length == 0) return;
            f.CopyTo(textBoxDestFolder.Text + "\\" + e.Name, true);
            lblLastChangeFile.Text = e.Name;
            lastCopy = DateTime.Now;
            lblLastChangeTimestamp.Text = lastCopy.ToString();
        }

        private void linkLabelGitlab_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("https://gitlab.com/commandff/RustPlugins/") { UseShellExecute = true });
        }

        private void buttonCopyAll_Click(object sender, EventArgs e) {
            lastCopy = DateTime.Now;
            if (Directory.Exists(textBoxSourceFolder.Text) && Directory.Exists(textBoxDestFolder.Text)) {
                foreach (FileInfo file in new DirectoryInfo(textBoxSourceFolder.Text).GetFiles("*.cs")) {
                    file.CopyTo(textBoxDestFolder.Text + "\\" + file.Name, true);
                    lblLastChangeFile.Text = file.Name;
                    lblLastChangeTimestamp.Text = lastCopy.ToString();
                }
            }
        }
    }
}