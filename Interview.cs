/*
(Completely standard MIT License:)
Copyright 2023 "Yonneh"
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the �Software�), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------------------------------------
15 DEC 2023 - v 0.0.1
- Initial Draft release. Not intended for production use- needs thorough testing.
- Project started.

*/
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using CompanionServer.Handlers;
using Facepunch;
using Facepunch.Extend;
using Newtonsoft.Json;
using UnityEngine;
using WebSocketSharp;
using Oxide.Core;
using Oxide.Plugins;
using Oxide.Core.Configuration;
using Oxide.Core.Database;
using Oxide.Core.Plugins;
using Oxide.Core.Libraries.Covalence;
using Oxide.Core.SQLite.Libraries;
using Oxide.Game.Rust.Libraries;
using Oxide.Game.Rust.Cui;
using ConVar;


namespace Oxide.Plugins {
    [Info("Interview", "Yonneh", "0.0.1")]
    [Description("Provides a nice safe automated environment for interviews with players.")]
    public class Interview : RustPlugin {
        #region **** Globals
        // static self reference
        private static Interview _plugin;
        private static ConfigData config = new();
        private static bool configDirty = false;
        private static Effect _reusableEffect = new Effect();
        #endregion
        #region Oxide Hooks - Server
        internal void Init() {
            _plugin = this;
            LoadDefaultMessages();
        }
        internal void OnServerSave() => SaveConfig();

        internal void Loaded() {

        }
        internal void Unload() {
            SaveConfig();
        }
        #endregion

        protected override void LoadDefaultMessages() {
            lang.RegisterMessages(new Dictionary<string, string>() {
                ["iMSG"] = "[<size=12><color=#70A0A0>Interview</color></size>] ",
                ["interviewInitiated"] = "Your recent actions have earned you an interview with the server staff! Please sit still, we will be with you within {0}. If we have not arrived by then, You will be {1}. If you happen to fall asleep here, or manage to injure yourself, your body will be placed into cold storage automatically. Please request a follow-on interview, if you would like to request any of the items from your corpse be returned. Also, any attempts to depart the immediate area are strongly discouraged. Thank you for your patience, and please have a nice day.",
                ["noTalk"] = "Sorry, could you please refrain from speaking, until you have been cleared to do so, by your interviewer.",
            }, this);
        }

        internal string LangMessage(string userID, string langKey, string[] args) {
            if (langKey.IsNullOrEmpty()) {
                PrintError($"LangMessage({userID ?? "null"},{langKey},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")}): langKey empty!");
                return "Interview Internal Error: langKey empty!";
            }
            string msg = lang.GetMessage(langKey, this, userID);
            if (msg == langKey) {
                PrintError($"LangMessage({userID ?? "null"},{langKey},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")}): langKey not found!");
                return $"Interview Internal Error: langKey {langKey} not found!";
            }
            if (args != null) {
                try {
                    msg = string.Format(msg, args);
                } catch (Exception ex) {
                    PrintError($"LangMessage({userID ?? "null"},{langKey},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")}): Exception {ex}, Message \"{msg}\"");
                    return $"Interview Internal Error: Exception {ex}, Message \"{msg}\"";
                }
            }
            return lang.GetMessage("bsMSG", this) + msg;
        }

        #region Oxide Hooks - Config
        protected override void LoadConfig() {
            base.LoadConfig();
            try {
                config = Config.ReadObject<ConfigData>();
            } catch (Exception ex) {
                PrintError($"Error reading Config: {ex}");
            }
            if (config == null || config.adminPermission.IsNullOrEmpty()) {
                LoadDefaultConfig();
            }
            ValidateConfig();
        }
        private bool ValidateConfig() {
            bool valid = true;
            return valid;
        }
        protected override void LoadDefaultConfig() {
            PrintError("Loading Default Config");
            DirtyConfig();
            config = new ConfigData {
                adminPermission = "Interview.admin",
            };
        }
        protected override void SaveConfig() {
            if (configDirty) {
                Config.WriteObject(config);
                Puts("Saved Configuration.");
                configDirty = false;
            }
        }

        private static void DirtyConfig() {
            if (!configDirty) {
                if (_plugin != null) {
                    _plugin.Puts("Configuration was marked dirty...");
                }
                configDirty = true;
            }
        }
        #endregion

        #region Oxide Hooks - Permissions
        internal void OnUserPermissionGranted(string userId, string perm) {
            //if (allPermissions.Contains(perm)) {
            //_plugin.Puts($"OnUserPermissionGranted(userId:{userId},perm:{perm})");
            //}
        }
        internal void OnUserPermissionRevoked(string userId, string perm) {
            //if (allPermissions.Contains(perm)) {
            //_plugin.Puts($"OnUserPermissionRevoked(userId:{userId},perm:{perm})");
            //}
        }
        #endregion

        #region Functional Utils - void SendEffect(BasePlayer player, string effectPrefab)
        private static void SendEffect(BasePlayer player, string effectPrefab) {
            if (string.IsNullOrWhiteSpace(effectPrefab)) return;
            _reusableEffect.Init(Effect.Type.Generic, player, 0, Vector3.zero, Vector3.forward);
            _reusableEffect.pooledString = effectPrefab;
            EffectNetwork.Send(_reusableEffect, player.net.connection);
        }
        #endregion

        #region void Cmd(BasePlayer player, string command, string[] args)
        internal bool CCmd(ConsoleSystem.Arg arg) {
            BasePlayer player = arg.Player();
            if (player == null) return false;
            Cmd(player, config.command, arg.Args);
            return true;
        }
        internal void Cmd(BasePlayer player, string command, string[] args) {
            //_plugin.Puts($"Cmd({player.UserIDString},{command},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")})");
            bool isAdmin = permission.UserHasPermission(player.UserIDString, config.adminPermission);
            string cmd = args[0].ToLower();
            switch (cmd) {
                case "initiate": // /interview initiate <playername|userID> [options ...]
                    if (!isAdmin) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdInitiate", null));
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdInitiate2", new string[] { player.userID.ToString() }));
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} initiate <playername|userID> [options ...]" }));
                    return;
                default:
                    break;
            }
            // /bshop ***
            player.ChatMessage(LangMessage(player.UserIDString, "cmdErrUnknownCommand", new string[] { $"{command} {string.Join(" ", args)}" }));
            return;
        }
        #endregion

        #region class ConfigData
        internal class ConfigData {
            [JsonProperty(PropertyName = "Admin Permission")] public string adminPermission;
            [JsonProperty(PropertyName = "Command")] public string command;
            [JsonProperty(PropertyName = "Interviewee Permission")] public string intervieweePermission;
            [JsonProperty(PropertyName = "Squelched Permission")] public string squelchedPermission;
            [JsonProperty(PropertyName = "Default Interview Timeout")] public int defaultTimeout;
        }
        #endregion

        #region class PlacedUI
        internal class PlacedUI {
            private static readonly Dictionary<ulong, Timer> Players = new();
            internal static void CloseUI(BasePlayer player) {
                CuiHelper.DestroyUi(player, "bshop_placedui");
                if (Players.ContainsKey(player.userID)) {
                    if (Players[player.userID]?.Destroyed == false) Players[player.userID].DestroyToPool();
                    Players.Remove(player.userID);
                }
            }
            internal static void CloseAll() {
                foreach (var user in Players) if (BasePlayer.TryFindByID(user.Key, out BasePlayer player)) CuiHelper.DestroyUi(player, "bshop_placedui");
                Players.Clear();
            }
            internal static void Show(BasePlayer player, string entName) {
                CloseUI(player);
                LimitEntry limit = config.permissions[PlayerData.data[player.userID].Perm].entLimits[entName];
                string purchaseItem = entName;
                int placed = PlayerData.GetPlaced(player.userID, entName, limit.groupMembers != null);
                if (!limit.groupMembers.IsNullOrEmpty()) purchaseItem = limit.stub.entName;
                int purchased = Purchases.Get(player.userID, purchaseItem);
                int maxPlaced = limit.free + purchased;
                bool isFail = placed + limit.stub.groupContribution > maxPlaced;
                var elements = new CuiElementContainer { };
                var parent = elements.Add(new CuiPanel { Image = { Color = $"0.25 0.31 0.31 {(isFail ? 0.97f : 0.85f):F3}" }, RectTransform = { AnchorMin = "0.33 0.2", AnchorMax = "0.66 0.3" }, FadeOut = (isFail ? 0f : 0.5f), CursorEnabled = false, KeyboardEnabled = false }, "Hud", "bshop_placedui");
                if (isFail) {
                    int canBuy = limit.max - purchased;
                    if (limit.max == -1) canBuy = config.infinity;
                    uint playerCurrency = (uint)Purchases.MugPlayer(player.userID);
                    int canAfford = limit.Purchaseable(playerCurrency, purchased);
                    if (canBuy > canAfford) { canBuy = canAfford; }
                    int needed = (limit.stub.groupContribution + placed) - maxPlaced;
                    if (needed > canBuy || playerCurrency < limit.GetTotalCost(purchased, needed)) SendEffect(player, config.sounds["overLimitSorry"]);
                    else SendEffect(player, config.sounds["overLimit"]);
                    var uiState = elements.Add(new CuiPanel { Image = { Color = "0.25 0.31 0.31 0.98" }, RectTransform = { AnchorMin = "0 1.1", AnchorMax = "1 3" }, CursorEnabled = true, KeyboardEnabled = true, }, parent);
                    string headerName = elements.Add(new CuiPanel { Image = { Color = config.UIColors["headerBackground"] }, RectTransform = { AnchorMin = "0 0.9", AnchorMax = "1 1" } }, uiState);
                    elements.Add(new CuiLabel { Text = { Text = $"Sorry. You may not place that item.", Color = "0.75 0.8 0.8 0.8", FontSize = 14, Align = TextAnchor.UpperLeft, VerticalOverflow = VerticalWrapMode.Overflow }, RectTransform = { AnchorMin = "0.02 0.25", AnchorMax = "0.98 0.8" } }, uiState);
                    elements.Add(new CuiButton { Button = { Close = parent, Color = "0.51 0.31 0.31 1" }, RectTransform = { AnchorMin = "0.02 0.04", AnchorMax = "0.24 0.15" }, Text = { Text = "Cancel", FontSize = 10, Align = TextAnchor.MiddleCenter } }, uiState);
                    if (canBuy == needed) {
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {needed}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.25 0.04", AnchorMax = "0.98 0.15" }, Text = { Text = $"Buy {needed}    <color=#40A040>{limit.GetTotalCost(purchased, needed):N0} {config.currencyName}</color>", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, uiState);
                    } else if (canBuy > needed + (limit.stub.groupContribution * 2)) {
                        int midBuyNum = needed + (limit.stub.groupContribution * 2);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {needed}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.25 0.04", AnchorMax = "0.488 0.25" }, Text = { Text = $"Buy {needed}\n<size=8><color=#40A040>{limit.GetTotalCost(purchased, needed):N0} {config.currencyName}</color></size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, uiState);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {midBuyNum}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.498 0.04", AnchorMax = "0.731 0.25" }, Text = { Text = $"Buy {midBuyNum}\n<size=8><color=#40A040>{limit.GetTotalCost(purchased, midBuyNum):N0} {config.currencyName}</color></size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, uiState);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {canBuy}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.741 0.04", AnchorMax = "0.98 0.25" }, Text = { Text = $"Buy {canBuy}\n<size=8><color=#40A040>{limit.GetTotalCost(purchased, canBuy):N0} {config.currencyName}</color></size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, uiState);
                    } else if (canBuy > needed) {
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {needed}", Color = "0.31 0.31 0.31 1" }, RectTransform = { AnchorMin = "0.25 0.04", AnchorMax = "0.61 0.15" }, Text = { Text = $"Buy {needed}     <size=10><color=#40A040>{limit.GetTotalCost(purchased, needed):N0} {config.currencyName}</color></size>", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, uiState);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {canBuy}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.62 0.04", AnchorMax = "0.98 0.15" }, Text = { Text = $"Buy {canBuy}     <size=10><color=#40A040>{limit.GetTotalCost(purchased, canBuy):N0} {config.currencyName}</color></size>", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, uiState);
                    }
                } else {
                    Players[player.userID] = _plugin.timer.Once(4f, () => {
                        CuiHelper.DestroyUi(player, parent);
                        Players.Remove(player.userID);
                    });
                    placed += limit.stub.groupContribution;
                }
                elements.Add(new CuiLabel { Text = { Text = limit.stub.displayNameColor, Color = "1 1 1 1", FontSize = 16, Align = TextAnchor.MiddleLeft, VerticalOverflow = VerticalWrapMode.Overflow }, RectTransform = { AnchorMin = "0.13 0.3", AnchorMax = "1 0.9" } }, parent);
                if (limit.stub.icon != null) elements.Add(new CuiElement { Parent = parent, Components = { limit.stub.icon, new CuiRectTransformComponent { AnchorMin = "0.01 0.2", AnchorMax = "0.13 0.8" } } });
                if (limit.stub.group != null) {
                    elements.Add(new CuiLabel { Text = { Text = limit.stub.group.stub.displayNameColor, Color = "0.625 0.4375 0.625 0.85", FontSize = 8, Align = TextAnchor.MiddleLeft }, RectTransform = { AnchorMin = "0.1575 0", AnchorMax = "1 0.4" } }, parent);
                    elements.Add(new CuiElement { Parent = parent, Components = { limit.stub.group.stub.icon, new CuiRectTransformComponent { AnchorMin = "0.1 0", AnchorMax = "0.15 0.4" } } });
                }
                if (isFail) elements.Add(new CuiElement { Parent = parent, Components = { new CuiRawImageComponent { Sprite = "assets/icons/lock.png" }, new CuiRectTransformComponent { AnchorMin = "0 0.1", AnchorMax = "0.15 0.92" }, }, });
                string graphThingTop = elements.Add(new CuiPanel { Image = { Color = "0 0 0 0.6" }, RectTransform = { AnchorMin = "0 0.85", AnchorMax = "1 1.1" } }, parent);
                float thisPct = (maxPlaced == 0 ? 1f : (float)(placed > maxPlaced ? maxPlaced : placed) / maxPlaced);
                elements.Add(new CuiPanel { Image = { Color = BlendColorCui(0xE640B010, 0xE6B04010, thisPct) }, RectTransform = { AnchorMin = "0 0.1", AnchorMax = $"{thisPct:F3} 0.9" } }, graphThingTop);
                elements.Add(new CuiLabel { Text = { Text = $"Placed {(isFail ? "" : $"(+<color=#A07070>{limit.stub.groupContribution}</color>) ")}[<color=#70A070>{placed}</color>/<color=#A07070>{maxPlaced}</color>]", Color = "0.75 0.85 0.85 0.85", FontSize = 10, Align = TextAnchor.MiddleRight }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, graphThingTop);
                elements.Add(new CuiLabel { Text = { Text = "Interview", Color = "0.75 1 1 1", FontSize = 5, Align = TextAnchor.LowerRight }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, parent);
                CuiHelper.AddUi(player, elements);
            }
        }
        #endregion

    }
}