﻿/*
(Completely standard MIT License:)

Copyright 2023 "Yonneh"

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


-------------------------------------
3 DEC 2023 - v 0.9.1
- Initial Draft release. Not intended for production use- needs thorough testing.

16 NOV 2023 - v 0.0.5
- Admin UI todo:
  - groupName command handling
  - add/remove limit entry
- Pagination greatly simplified
- Hardened file storage, added more checks.
- split playerData into userid%PlayerData.dirty.Count files (12 by default)
- Added several new bugs.

12 NOV 2023 - v 0.0.4
- Added Admin UI
- Basic UI controls done, todo pagination rewrite
- Main UI Honors Streamer Mode (Hides server name/ip/userID)
- Clean up minor feature creep
- Groups can not contain groups. Don't try- a black hole will form, and the universe will be destroyed.

27 OCT 2023 - v 0.0.1
- initial proof of concept published.
- parts of this plugin were copied from Entity Limit v2.1.3 by The Friendly Chap - https://umod.org/plugins/entity-limit
   This plugin (Building Shop) should not be considered a replacement, or successor to Entity Limit.

*/
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using CompanionServer.Handlers;
using Facepunch;
using Facepunch.Extend;
using Newtonsoft.Json;
using UnityEngine;
using WebSocketSharp;
using Oxide.Core;
using Oxide.Plugins;
using Oxide.Core.Configuration;
using Oxide.Core.Database;
using Oxide.Core.Plugins;
using Oxide.Core.Libraries.Covalence;
using Oxide.Core.SQLite.Libraries;
using Oxide.Game.Rust.Libraries;
using Oxide.Game.Rust.Cui;


namespace Oxide.Plugins {
    [Info("BuildingShop", "Yonneh", "0.9.3")]
    [Description("(Pre-RC1) Limit entities, based on permissions, with a shop, to allow enchroachments.")]
    public class BuildingShop : RustPlugin {
        #region **** Globals
        // static self reference
        private static BuildingShop _plugin;
        // ext. plugins
        [PluginReference]
        private Plugin ServerRewards, Economics;
        private static ConfigData config = new();
        private static bool configDirty = false;
        private static Effect _reusableEffect = new Effect();
        // list Limits permissions, pre-sorted by priority
        private static List<string> limitsPermissions = new();
        // hashset of all permissions, including the admin one; for quick returns from permissions hooks
        private static HashSet<string> allPermissions = new();
        private static string regCom = string.Empty;
        private static List<string> regComs = new();
        #endregion
        #region Oxide Hooks - Server
        internal void Init() {
            _plugin = this;
            if (BaseNetworkable.serverEntities?.entityList != null && BaseNetworkable.serverEntities.entityList.Count > 0) PlayerData.Lookup();
            LoadDefaultMessages();
        }
        internal object OnSaveLoad(Dictionary<BaseEntity, ProtoBuf.Entity> entities) {
            BaseEntity[] loaded = new BaseEntity[] { };
            entities.Keys.CopyTo(loaded, 0);
            PlayerData.Lookup(true, loaded);
            return null;
        }
        internal void OnNewSave(string filename) {
            if (config.wipePurchases) Purchases.Wipe();
            PlayerData.data.Clear();
            PlayerData.Lookup();
        }
        internal void OnServerSave() {
            Purchases.Save();
            SaveConfig();
        }

        internal void OnServerShutdown() {
            Purchases.Save();
        }

        internal void Loaded() {
            Purchases.Load();
        }
        internal void Unload() {
            cmd.RemoveChatCommand(regCom, this);
            cmd.RemoveConsoleCommand(regCom, this);
            regCom = string.Empty;
            for (int i = regComs.Count - 1; i > -1; i--) {
                cmd.RemoveChatCommand(regComs[i], this);
                regComs.RemoveAt(i);
            }
            regComs.Clear();
            PlayerData.Lookup(start: false);
            Purchases.Save();
            BuildingShopUI.CloseAll();
            PlacedUI.CloseAll();
            Purchases.players.Clear();
            PlayerData.entityMap.Clear();
            PlayerData.data.Clear();
            foreach (var i in BeltContent.players) {
                if (BasePlayer.TryFindByID(i.Key, out BasePlayer player)) {
                    foreach (var p in i.Value) {
                        CuiHelper.DestroyUi(player, $"bshop_placingui_belt{p.slot}");
                    }
                }
            }
            BeltContent.players.Clear();
            SaveConfig();
        }
        #endregion
        #region Lang
        protected override void LoadDefaultMessages() {
            lang.RegisterMessages(new Dictionary<string, string>() {
                ["bsMSG"] = "[<size=12><color=#70A0A0>BuildingShop</color></size>] ",
                ["overLimitGlobalPlaced"] = "Sorry, you have already placed {0} of {1} entities on the map!",
                ["overLimitGlobal"] = "Sorry, you are not allowed to build on this map!",
                ["noLimitsPermission"] = "You do not have permission to access this plugin.",
                ["noAdminPermission"] = "This command is only available to Admins and Moderators.",
                ["cmdRefunded"] = "You have been refunded <color=#40A040>{0} {1}</color>({2}), and your purchases have been wiped!",
                ["cmdAdminRefunded"] = "{0} has been refunded.",
                ["cmdAdminRefundFailedNoneSpent"] = "{0} has not spent any {1}!",
                ["cmdAdminRefundFailed"] = "{0} refund failed!",
                ["cmdAdminLoadDefaultConfig"] = "Default Config has been loaded!",
                ["cmdAdminGranted"] = "You have been granted {0}!",
                ["cmdAdminRevoked"] = "You no longer have {0}!",
                ["cmdAdminPurchasesRestored"] = "Player Purchases have been restored.",
                ["cmdErrAdminPurchasesRestoreFailed"] = "Error: There was no Purchases.json.bak to restore from!",
                ["cmdWipedGlobal"] = "All player purchases have been wiped.",
                ["cmdCacheWiped"] = "Player Entity Cache has been wiped, and is being rebuilt... hold onto your butt.",
                ["cmdConfigLutMapped"] = "Config Lookup Table {0} mapped to {1} ({2})!",
                ["cmdUsage"] = "Usage: {0}",
                ["cmdChanged"] = "{0} {1} changed from {2} to {3}!",
                ["cmdCreated"] = "{0} {1} Created",
                ["cmdRemoved"] = "{0} {1} Removed",
                ["cmdAdded"] = "{0} {1} Added {2}",
                ["cmdRemovedFrom"] = "{0} {1} Removed From {2} {3}",
                ["cmdErrUnknownCommand"] = "Error: Unknown Command: <size=10>{0}</size>",
                ["cmdErrAlreadyExists"] = "Error: {0} {1} already exists!",
                ["cmdErrAlreadyMember"] = "Error: {0} {1} is already a member of {2}!",
                ["cmdErrDoesntExist"] = "Error: {0} {1} does not exist!",
                ["cmdErrNotMember"] = "Error: {0} is not a member of {1}!",
                ["cmdErrNotValid"] = "Error: {0} is not a valid {1}!",
                ["cmdErrForeignPermission"] = "Error: Unable to grant permission {0}",
            }, this);
        }

        internal string LangMessage(string userID, string langKey, string[] args) {
            if (langKey.IsNullOrEmpty()) {
                PrintError($"LangMessage({userID ?? "null"},{langKey},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")}): langKey empty!");
                return "BuildingShop Internal Error: langKey empty!";
            }
            string msg = lang.GetMessage(langKey, this, userID);
            if (msg == langKey) {
                PrintError($"LangMessage({userID ?? "null"},{langKey},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")}): langKey not found!");
                return $"BuildingShop Internal Error: langKey {langKey} not found!";
            }
            if (args != null) {
                try {
                    msg = string.Format(msg, args);
                } catch (Exception ex) {
                    PrintError($"LangMessage({userID ?? "null"},{langKey},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")}): Exception {ex}, Message \"{msg}\"");
                    return $"BuildingShop Internal Error: Exception {ex}, Message \"{msg}\"";
                }
            }
            return lang.GetMessage("bsMSG", this) + msg;
        }
        #endregion

        //internal void OnActiveItemChanged(BasePlayer player, Item oldItem, Item newItem) {
        //    string msg = $"OnActiveItemChanged({player._lastSetName}({player.UserIDString}), {oldItem?.info.shortname ?? "null"}, {newItem?.info.shortname ?? "null"})";
        //    Puts(msg);
        //}
        public class BeltContent {
            public int slot;
            public ulong iid = 0;
            public string shortname = string.Empty;
            internal LimitEntry limit = null;
            public BeltContent(int slot) => this.slot = slot;
            internal static Dictionary<ulong, List<BeltContent>> players = new();
            internal static List<CuiElement> redPanels = new() {
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt0", Components = { new CuiImageComponent { Color = "1 0 0 0.25" }, new CuiRectTransformComponent { AnchorMin = "0.350 0.0249", AnchorMax = "0.394 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt1", Components = { new CuiImageComponent { Color = "1 0 0 0.25" }, new CuiRectTransformComponent { AnchorMin = "0.398 0.0249", AnchorMax = "0.443 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt2", Components = { new CuiImageComponent { Color = "1 0 0 0.25" }, new CuiRectTransformComponent { AnchorMin = "0.446 0.0249", AnchorMax = "0.491 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt3", Components = { new CuiImageComponent { Color = "1 0 0 0.25" }, new CuiRectTransformComponent { AnchorMin = "0.494 0.0249", AnchorMax = "0.539 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt4", Components = { new CuiImageComponent { Color = "1 0 0 0.25" }, new CuiRectTransformComponent { AnchorMin = "0.542 0.0249", AnchorMax = "0.587 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt5", Components = { new CuiImageComponent { Color = "1 0 0 0.25" }, new CuiRectTransformComponent { AnchorMin = "0.591 0.0249", AnchorMax = "0.635 0.107" } } },
            };
            internal static List<CuiElement> clearPanels = new() {
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt0", Components = { new CuiImageComponent { Color = "0 0 0 0" }, new CuiRectTransformComponent { AnchorMin = "0.350 0.0249", AnchorMax = "0.394 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt1", Components = { new CuiImageComponent { Color = "0 0 0 0" }, new CuiRectTransformComponent { AnchorMin = "0.398 0.0249", AnchorMax = "0.443 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt2", Components = { new CuiImageComponent { Color = "0 0 0 0" }, new CuiRectTransformComponent { AnchorMin = "0.446 0.0249", AnchorMax = "0.491 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt3", Components = { new CuiImageComponent { Color = "0 0 0 0" }, new CuiRectTransformComponent { AnchorMin = "0.494 0.0249", AnchorMax = "0.539 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt4", Components = { new CuiImageComponent { Color = "0 0 0 0" }, new CuiRectTransformComponent { AnchorMin = "0.542 0.0249", AnchorMax = "0.587 0.107" } } },
                new CuiElement{ Update = true, Parent = "Overlay", Name = "bshop_placingui_belt5", Components = { new CuiImageComponent { Color = "0 0 0 0" }, new CuiRectTransformComponent { AnchorMin = "0.591 0.0249", AnchorMax = "0.635 0.107" } } },
            };
            internal static CuiElement limitText = new CuiElement { Components = { new CuiTextComponent { Color = "0.7 0.7 0.7 0.9", FontSize = 10, Align = TextAnchor.LowerLeft, VerticalOverflow = VerticalWrapMode.Overflow }, new CuiRectTransformComponent { AnchorMin = "0.07 00.02", AnchorMax = "0.93 0.98" } } };
            internal void Remove(BasePlayer player) {
                _plugin.Puts($"Remove({player._lastSetName}) slot {slot}");
                iid = 0;
                shortname = string.Empty;
                limit = null;
                string elemName = $"bshop_placingui_belt{slot}";
                CuiHelper.DestroyUi(player, elemName);
            }
            internal static void Update(BasePlayer player, string shortname, LimitEntry limit) {
                int purchased, placed, groupContribution;
                if (limit.groupMembers != null) {
                    purchased = Purchases.Get(player.userID, limit.stub.entName);
                    placed = PlayerData.GetPlaced(player.userID, limit.stub.entName, true);
                    groupContribution = limit.groupMembers[shortname].groupContribution;
                } else {
                    purchased = Purchases.Get(player.userID, shortname);
                    placed = PlayerData.GetPlaced(player.userID, shortname, false);
                    groupContribution = 1;
                }
                var elements = new CuiElementContainer { };
                foreach (var _b in players[player.userID]) {
                    if (_b.shortname == shortname) {
                        if (placed + groupContribution > limit.free + purchased) {
                            elements.Add(redPanels[_b.slot]);
                        } else {
                            elements.Add(clearPanels[_b.slot]);
                        }
                        limitText.Parent = "bshop_placingui_belt" + _b.slot.ToString();
                        (limitText.Components[0] as CuiTextComponent).Text = $"[{placed}/{limit.free + purchased}]";
                        elements.Add(limitText);
                    }
                }
                CuiHelper.AddUi(player, elements);
            }
            internal void Init(BasePlayer player, Item item) {
                _plugin.Puts($"Init({player._lastSetName},{item.info.shortname}) slot {slot}");
                if (iid != 0) Remove(player);
                iid = item.uid.Value;
                shortname = item.info.shortname;
                string perm = PlayerData.GetPerm(player.userID);
                if (perm.IsNullOrEmpty() || !limitsPermissions.Contains(perm) || !config.permissions[perm].entLimits.ContainsKey(shortname)) return;
                limit = config.permissions[perm].entLimits[shortname];
                Update(player, shortname, limit);
            }
        }
        internal object OnInventoryNetworkUpdate(PlayerInventory inventory, ItemContainer container, ProtoBuf.UpdateItemContainer updateItemContainer, PlayerInventory.Type type, bool broadcast) {
            if (type != PlayerInventory.Type.Belt || !inventory.baseEntity.userID.IsSteamId()) return null;

            List<BeltContent> pBelt;
            if (!BeltContent.players.ContainsKey(inventory.baseEntity.userID)) {
                pBelt = BeltContent.players[inventory.baseEntity.userID] = new() { new(0), new(1), new(2), new(3), new(4), new(5) };

            } else pBelt = BeltContent.players[inventory.baseEntity.userID];
            foreach (var x in pBelt) {
                if (x.iid == 0) continue;
                var newItem = container.GetSlot(x.slot);
                if (newItem != null) {
                    if (newItem.uid.Value != x.iid) {
                        Puts($"OnBeltItemChanged({inventory.baseEntity._lastSetName}({inventory.baseEntity.UserIDString}): [{x.slot}]({x.shortname})<0x{x.iid:X8}> -> [{newItem.position}]{newItem.info.displayName.english}({newItem.info.shortname})<0x{newItem.uid.Value:X8}>x{newItem.amount}");
                        x.Init(inventory.baseEntity, newItem);

                    }
                } else {
                    Puts($"OnBeltItemRemoved({inventory.baseEntity._lastSetName}({inventory.baseEntity.UserIDString}): [{x.slot}]({x.shortname})<0x{x.iid:X8}>");
                    x.Remove(inventory.baseEntity);
                }
            }
            foreach (var i in container.itemList) {
                if (i.position != -1 && pBelt[i.position].iid != i.uid.Value) {
                    Puts($"OnBeltItemAdded({inventory.baseEntity._lastSetName}({inventory.baseEntity.UserIDString}): [{i.position}]{i.info.displayName.english}({i.info.shortname})<0x{i.uid.Value:X8}>x{i.amount}");
                    pBelt[i.position].Init(inventory.baseEntity, i);
                }
            }
            return null;
        }
        #region Oxide Hooks - Building Limits
        internal object OnConstructionPlace(BaseEntity entity, Construction component, Construction.Target constructionTarget, BasePlayer player) {
            string perm = PlayerData.GetPerm(player.userID);
            string shortname = PlayerData.ResolveEntity(entity.ShortPrefabName);
            if (!perm.IsNullOrEmpty() && limitsPermissions.Contains(perm) && config.permissions[perm].entLimits.ContainsKey(shortname)) {
                PlacedUI.Show(player, shortname);
                BeltContent.Update(player, entity.ShortPrefabName, config.permissions[perm].entLimits[shortname]);
            }
            return null;
        }
        internal object CanDeployItem(BasePlayer player, Deployer deployer, NetworkableId entityId) {
            string perm = PlayerData.GetPerm(player.userID);
            if (perm.IsNullOrEmpty() || !limitsPermissions.Contains(perm)) return null;
            ConfigData.PermissionEntry permE = config.permissions[perm];
            if (permE.globalLimit == 0) {
                player.ChatMessage(LangMessage(player.UserIDString, "overLimitGlobal", null));
                SendEffect(player, config.sounds["overLimitSorry"]);
                return false;
            }
            if (permE.globalLimit > 0 && PlayerData.data[player.userID].placedTotal >= permE.globalLimit) {
                player.ChatMessage(LangMessage(player.UserIDString, "overLimitGlobalPlaced", new string[] { PlayerData.data[player.userID].placedTotal.ToString("N0"), permE.globalLimit.ToString("N0") }));
                SendEffect(player, config.sounds["overLimitSorry"]);
                return false;
            }
            string shortname = deployer.GetItem().info.shortname;
            if (permE.entLimits.ContainsKey(shortname)) {
                LimitEntry limit = permE.entLimits[shortname];
                int purchased = Purchases.Get(player.userID, (limit.groupMembers == null ? shortname : limit.stub.entName));
                int placed = PlayerData.GetPlaced(player.userID, (limit.groupMembers == null ? shortname : limit.stub.entName), limit.groupMembers != null);
                int groupContribution = 1;
                if (limit.groupMembers != null) groupContribution = limit.groupMembers[shortname].groupContribution;
                if (placed + groupContribution > limit.free + purchased) {
                    PlacedUI.Show(player, shortname);
                    return false;
                }
            }
            return null;
        }
        internal object CanBuild(Planner planner, Construction entity, Construction.Target target) {
            BasePlayer player = planner.GetOwnerPlayer();
            string perm = PlayerData.GetPerm(player.userID);
            // _plugin.Puts($"CanBuild: {entity.fullName}");
            if (perm.IsNullOrEmpty() || !limitsPermissions.Contains(perm)) return null;
            ConfigData.PermissionEntry permE = config.permissions[perm];
            if (permE.globalLimit == 0) {
                player.ChatMessage(LangMessage(player.UserIDString, "overLimitGlobal", null));
                SendEffect(player, config.sounds["overLimitSorry"]);
                return false;
            }
            if (permE.globalLimit > 0 && PlayerData.data[player.userID].placedTotal >= permE.globalLimit) {
                player.ChatMessage(LangMessage(player.UserIDString, "overLimitGlobalPlaced", new string[] { PlayerData.data[player.userID].placedTotal.ToString("N0"), permE.globalLimit.ToString("N0") }));
                SendEffect(player, config.sounds["overLimitSorry"]);
                return false;
            }
            string shortname = PlayerData.ResolveEntity(entity.fullName.Substring(entity.fullName.LastIndexOf("/", StringComparison.Ordinal) + 1).Replace(".prefab", string.Empty));
            if (permE.entLimits.ContainsKey(shortname)) {
                LimitEntry limit = permE.entLimits[shortname];
                int purchased = Purchases.Get(player.userID, (limit.groupMembers == null ? shortname : limit.stub.entName));
                int placed = PlayerData.GetPlaced(player.userID, (limit.groupMembers == null ? shortname : limit.stub.entName), limit.groupMembers != null);
                int groupContribution = 1;
                if (limit.groupMembers != null) groupContribution = limit.groupMembers[shortname].groupContribution;
                if (placed + groupContribution > limit.free + purchased) {
                    PlacedUI.Show(player, shortname);
                    return false;
                }
            }
            return null;
        }
        internal void OnEntitySpawned(BaseEntity entity) {
            if (entity.OwnerID.IsSteamId()) {
                //_plugin.Puts($"OnEntitySpawned(BaseEntity:{entity.ShortPrefabName}({entity.GetInstanceID()})) owner {entity.OwnerID}");
                PlayerData.StoreEntity(entity);
            }
        }
        internal void OnEntityKill(BaseEntity entity) {
            if (entity.OwnerID.IsSteamId()) {
                //_plugin.Puts($"OnEntityKill(BaseEntity:{entity.ShortPrefabName}({entity.GetInstanceID()})) owner {entity.OwnerID}");
                PlayerData.RemoveEntity(entity);
            }
        }
        #endregion
        #region Oxide Hook - Close UI
        internal void OnEntityDeath(BasePlayer player, HitInfo info) {
            if (!player.userID.IsSteamId()) return;
            //_plugin.Puts($"OnEntityDeath(BasePlayer:{player.UserIDString})");
            BuildingShopUI.CloseUI(player);
            PlacedUI.CloseUI(player);
        }
        internal void OnEntityKill(BasePlayer player) {
            if (!player.userID.IsSteamId()) return;
            //_plugin.Puts($"OnEntityKill(BasePlayer:{player.UserIDString})");
            BuildingShopUI.CloseUI(player);
            PlacedUI.CloseUI(player);
        }
        internal void OnPlayerSleep(BasePlayer player) {
            if (!player.userID.IsSteamId()) return;
            //_plugin.Puts($"OnPlayerSleep(BasePlayer:{player.UserIDString})");
            BuildingShopUI.CloseUI(player);
            PlacedUI.CloseUI(player);
        }
        #endregion
        #region Config
        internal class ConfigData {
            public string adminPermission;
            public List<string> commands;
            public string command;
            public int currency;
            public string currencyName;
            public string currencyPrefix;
            public bool wipePurchases;
            public int infinity;
            public double refundMultiplier;
            public Dictionary<string, string> sounds;
            public Dictionary<string, PermissionEntry> permissions;
            internal class PermissionEntry {
                public int priority = -1;
                public int globalLimit = -1;
                public Dictionary<string, LimitEntry> limits = new();
                [JsonIgnore] internal List<string> sortedLimits = new();
                [JsonIgnore] internal Dictionary<string, LimitEntry> entLimits = new();
                [JsonIgnore] internal static PermissionEntry blank = new();
            }
            public Dictionary<string, string> lut;
            public Dictionary<string, string> UIColors;
            public Dictionary<string, BBlockStub> buildingBlocks;
            internal class BBlockStub {
                public string displayName;
                public string icon;
                public BBlockStub(string displayName, string icon) {
                    this.displayName = displayName;
                    this.icon = icon;
                }
            }
        }
        protected override void LoadConfig() {
            base.LoadConfig();
            try {
                config = Config.ReadObject<ConfigData>();
            } catch (Exception ex) {
                PrintError($"Error reading Config: {ex}");
            }
            if (config == null || config.adminPermission.IsNullOrEmpty()) {
                LoadDefaultConfig();
            }
            ValidateConfig();
        }
        private bool ValidateConfig() {
            bool valid = true;
            limitsPermissions.Clear(); // =  config.permissions.Keys;// (from keyValuePair in config.permissions orderby keyValuePair.Value?.priority ?? -1 descending select keyValuePair.Key).ToList() ?? new();
            foreach (var i in config.permissions.Keys) limitsPermissions.Add(i);
            limitsPermissions.Sort((x, y) => config.permissions[x].priority - config.permissions[y].priority);
            PlayerData.entityMap.Clear();
            allPermissions.Clear();
            allPermissions.Add(config.adminPermission);
            foreach (string val in config.buildingBlocks.Keys) {
                PlayerData.entityMap[val] = val;
            }
            foreach (KeyValuePair<string, string> ent in config.lut) {
                PlayerData.entityMap[ent.Key] = ent.Value;
            }
            foreach (KeyValuePair<string, ConfigData.PermissionEntry> permissionsEntry in config.permissions) {
                if (permissionsEntry.Key.StartsWith("BuildingShop.") && !permission.PermissionExists(permissionsEntry.Key)) {
                    Puts($" Register Limit Permission: {permissionsEntry.Key}");
                    permission.RegisterPermission(permissionsEntry.Key, this);
                }
                allPermissions.Add(permissionsEntry.Key);
                permissionsEntry.Value.entLimits.Clear();
                foreach (KeyValuePair<string, LimitEntry> limit in permissionsEntry.Value.limits) {
                    permissionsEntry.Value.entLimits.Add(limit.Key, limit.Value);
                    limit.Value.stub = new(1);
                    LimitEntry.Resolve(limit.Value, limit.Key);
                    if (limit.Value.groupMembers != null) foreach (var gm in limit.Value.groupMembers) permissionsEntry.Value.entLimits.Add(gm.Key, limit.Value);
                }
                permissionsEntry.Value.sortedLimits = new();// = permissionsEntry.Value.limits.ToList().OrderBy(x => x.Value.stub.categoryName).ThenBy(x => x.Value.stub.displayName).Select(x => x.Key).ToList();
                foreach (var i in permissionsEntry.Value.limits.Keys) permissionsEntry.Value.sortedLimits.Add(i);
                permissionsEntry.Value.sortedLimits.Sort((x, y) => {
                    var _x = permissionsEntry.Value.limits[x];
                    var _y = permissionsEntry.Value.limits[y];
                    int res = string.Compare(_x.stub.categoryName, _y.stub.categoryName);
                    if (res != 0) { return res; }
                    res = string.Compare(_x.stub.displayName, _y.stub.displayName);
                    return res;
                });
                int ord = 0;
                permissionsEntry.Value.sortedLimits.ForEach(x => { permissionsEntry.Value.limits[x].order = ord++; });
            }
            if (config.adminPermission.StartsWith("BuildingShop.") && !permission.PermissionExists(config.adminPermission)) {
                permission.RegisterPermission(config.adminPermission, this);
                Puts($" Register Admin Perm: {config.adminPermission}");
            }
            if (!config.command.Equals(regCom)) {
                cmd.RemoveChatCommand(regCom, this);
                cmd.RemoveConsoleCommand(regCom, this);
                Puts($" Primary Command: {regCom} -> {config.command}");
                cmd.AddChatCommand(config.command, this, Cmd);
                cmd.AddConsoleCommand(config.command, this, CCmd);
                regCom = config.command;
            }
            for (int i = regComs.Count - 1; i > -1; i--) {
                if (!config.commands.Contains(regComs[i])) {
                    Puts($" Remove Chat Command: {regComs[i]}");
                    cmd.RemoveChatCommand(regComs[i], this);
                    regComs.RemoveAt(i);
                }
            }
            foreach (string com in config.commands) {
                if (!regComs.Contains(com)) {
                    Puts($" Add Chat Command: {com}");
                    cmd.AddChatCommand(com, this, Cmd);
                    regComs.Add(com);
                }
            }
            return valid;
        }
        protected override void LoadDefaultConfig() {
            PrintError("Loading Default Config");
            DirtyConfig();
            config = new ConfigData {
                adminPermission = "BuildingShop.admin",
                commands = new List<string>() { "limit", "limits" },
                command = "bshop",
                currency = -932201673,
                currencyName = "Scrap",
                currencyPrefix = "",
                wipePurchases = false,
                infinity = 50,
                refundMultiplier = 0.25f,
                sounds = new(){
                    {"purchase","assets/prefabs/deployable/vendingmachine/effects/vending-machine-purchase-human.prefab" },
                    {"openUITabLimits","assets/prefabs/building/wall.frame.shopfront/effects/metal_transaction_complete.prefab" },
                    {"openUITabAdmin","assets/prefabs/npc/patrol helicopter/effects/rocket_fire.prefab" },
                    {"overLimit","assets/prefabs/npc/autoturret/effects/targetacquired.prefab" },
                    {"overLimitSorry","assets/prefabs/locks/keypad/effects/lock.code.denied.prefab" },
                },
                permissions = new(){ {
                        "BuildingShop.default",
                        new() { priority = 0, globalLimit = 500,
                            limits = new() {
                                {"Foundations", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=50, max=25, stub = new(){ Icon="assets/prefabs/building core/foundation/foundation.png" }, groupMembers = new(){ { "foundation", new() }, { "foundation.triangle", new() } } } },
                                {"Smelting.Slots", new  LimitEntry(){ basePrice=25f, multiplier=1.5f, free=10, max=10, stub = new(){ Name="Smelting Slots", Icon="" }, groupMembers = new(){ { "furnace.large", new(5) }, {"electric.furnace", new(3) }, { "furnace", new(2) } } } },
                                {"Planting.Slots", new LimitEntry(){ basePrice=20f, multiplier=1.025f, free=18, max=108, stub = new(){ Name="Planting Slots", Icon="" }, groupMembers = new(){ { "planter.large", new(9) }, { "rail.road.planter", new(9) }, { "planter.small", new(3) }, { "bathtub.planter", new(3) }, { "minecart.planter", new(2) } } } },
                                {"generator.wind.scrap", new LimitEntry(){ basePrice=250f, multiplier=15f, free=2, max=2 }  },
                                {"electric.solarpanel.large", new LimitEntry(){ basePrice=200f, multiplier=2f, free=0, max=3 }  },
                                {"industrial.conveyor", new LimitEntry(){ basePrice=110f, multiplier=3f, free=5, max=4 }  },
                                {"storageadaptor", new LimitEntry(){ basePrice=25f, multiplier=1.05f, free=75, max=100 }  },
                                {"electric.splitter", new LimitEntry(){ basePrice=25f, multiplier=1.5f, free=10, max=10 }  },
                                {"cupboard.tool", new LimitEntry(){ basePrice=2000f, multiplier=2f, free=2, max=3 } },
                                {"workbench1", new LimitEntry(){ basePrice=100f, multiplier=2f, free=1, max=1 } },
                                {"workbench2", new LimitEntry(){ basePrice=250f, multiplier=2f, free=0, max=2 } },
                                {"workbench3", new LimitEntry(){ basePrice=1000f, multiplier=2f, free=0, max=2 } },
                            }
                        }
                    }, {
                        "BuildingShop.vip",
                        new() { priority = 1, globalLimit = 2000,
                            limits = new() {
                                {"Foundations", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=200, max=-1, stub = new(){ Icon="assets/prefabs/building core/foundation/foundation.png" }, groupMembers = new(){ { "foundation", new() }, { "foundation.triangle", new() } } } },
                                {"Smelting.Slots", new LimitEntry(){ basePrice=25f, multiplier=1.5f, free=20, max=20, stub = new(){ Name="Smelting Slots", Icon="" }, groupMembers = new(){ { "furnace.large", new(5) }, {"electric.furnace", new(3) }, { "furnace", new(2) } } } },
                                {"Planting.Slots", new LimitEntry(){ basePrice=20f, multiplier=1.025f, free=108, max=216, stub = new(){ Name="Planting Slots", Icon="" }, groupMembers = new(){ { "planter.large", new(9) }, { "rail.road.planter", new(9) }, { "planter.small", new(3) }, { "bathtub.planter", new(3) }, { "minecart.planter", new(2) } } } },
                                {"autoturret", new LimitEntry(){ basePrice=1250f, multiplier=4f, free=3, max=3 } },
                                {"generator.wind.scrap", new LimitEntry(){ basePrice=250f, multiplier=15f, free=4, max=2 } },
                                {"electric.solarpanel.large", new LimitEntry(){ basePrice=200f, multiplier=2f, free=6, max=5 } },
                                {"industrial.conveyor", new LimitEntry(){ basePrice=110f, multiplier=3f, free=10, max=6 } },
                                {"storageadaptor", new LimitEntry(){ basePrice=25f, multiplier=1.05f, free=200, max=200 } },
                                {"electric.splitter", new LimitEntry(){ basePrice=25f, multiplier=1.5f, free=30, max=20 } },
                                {"cupboard.tool", new LimitEntry(){ basePrice=2000f, multiplier=2f, free=5, max=8 } },
                                {"workbench1", new LimitEntry(){ basePrice=100f, multiplier=2f, free=5, max=4 } },
                                {"workbench2", new LimitEntry(){ basePrice=250f, multiplier=2f, free=4, max=5 } },
                                {"workbench3", new LimitEntry(){ basePrice=1000f, multiplier=2f, free=4, max=5 } },
                            }
                        }
                    }, {
                        "BuildingShop.nolimit",
                        new() { priority = 999,
                        }
                    }, {
                        "BuildingShop.debug",
                        new() { priority = 9999, globalLimit = 200,
                            limits = new() {
                                {"Floors", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=200, max=100, groupMembers = new(){ {"floor",new() }, {"floor.frame", new() }, {"floor.triangle", new() }, {"floor.triangle.frame", new() } } } },
                                {"Stairs", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=200, max=100, groupMembers = new(){ {"block.stair.lshape", new() }, {"block.stair.spiral", new() }, {"block.stair.spiral.triangle", new() }, {"block.stair.ushape", new() }, {"foundation.steps", new() } } } },
                                {"Foundations", new LimitEntry(){ basePrice=25f, multiplier=1f, free=200, max=-1, stub = new(){ Icon="assets/prefabs/building core/foundation/foundation.png" }, groupMembers = new(){ { "foundation", new() }, { "foundation.triangle", new() } } } },
                                {"Roofs", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=200, max=100, groupMembers = new(){ {"roof", new() }, {"roof.triangle", new() } } } },
                                {"Walls", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=200, max=100, groupMembers = new(){ {"wall", new() }, {"wall.doorway", new() }, {"wall.frame", new() }, {"wall.half", new() }, {"wall.low", new() }, {"wall.window", new() } } } },
                                {"Smelting.Slots", new LimitEntry(){ basePrice=25f, multiplier=1.5f, free=20, max=20, stub = new(){ Name="Smelting Slots", Icon="" }, groupMembers = new(){ { "furnace.large", new(5) }, {"electric.furnace", new(3) }, { "furnace", new(2) } } } },
                                {"Planting.Slots", new LimitEntry(){ basePrice=20f, multiplier=1.025f, free=108, max=216, stub = new(){ Name="Planting Slots", Icon="" }, groupMembers = new(){ { "planter.large", new(9) }, { "rail.road.planter", new(9) }, { "planter.small", new(3) }, { "bathtub.planter", new(3) }, { "minecart.planter", new(2) } } } },
                                {"Fireworks", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=25, max=100, groupMembers = new(){ {"firework.romancandle.blue",new() }, {"firework.volcano", new() }, {"firework.volcano.red", new() }, {"firework.romancandle.violet", new() }, {"firework.volcano.violet", new() }, {"firework.romancandle.red", new() }, {"firework.boomer.orange", new() }, {"firework.boomer.red", new() }, {"firework.boomer.champagne", new() }, {"firework.boomer.blue", new() }, {"firework.boomer.green", new() }, {"firework.boomer.violet", new() }, {"firework.boomer.pattern", new() } } } },
                                {"electric.fuelgenerator.small", new LimitEntry(){ basePrice=200f, multiplier=2f, free=2, max=5 } },
                                {"electric.generator.small", new LimitEntry(){ basePrice=1250f, multiplier=8f, free=2, max=5 } },
                                {"box.repair.bench", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"box.wooden.large", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"autoturret", new LimitEntry(){ basePrice=1250f, multiplier=1.5f, free=3, max=3 } },
                                {"bbq", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"gunrack_wide.horizontal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"gunrack_stand", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"gunrack_tall.horizontal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"gunrack.horizontal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"gunrack.single.1.horizontal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"gunrack.single.2.horizontal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"gunrack.single.3.horizontal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"coffin.storage", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"trap.bear", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"samsite", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"trap.landmine", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"cctv.camera", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"water.purifier", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"box.wooden", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"water.barrel", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"vending.machine", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"stash.small", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"sleepingbag", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"small.oil.refinery", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"research.table", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"pookie.bear", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"sign.wooden.small", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"sign.wooden.medium", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"sign.wooden.large", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"sign.wooden.huge", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"hitchtroughcombo", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"barricade.wood.cover", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"water.catcher.small", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"water.catcher.large", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"wall.external.high", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"legacy.shelter.wood", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"mining.quarry", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"mining.pumpjack", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"Locks", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=200, max=100, stub=new(){ Icon="lock.key" }, groupMembers = new(){ {"lock.key", new() }, {"lock.code", new() } } } },
                                {"barricade.metal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"door.hinged.wood", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"door.hinged.metal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"door.hinged.toptier", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"door.double.hinged.wood", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"door.double.hinged.metal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"door.double.hinged.toptier", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"door.double.hinged.bardoors", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"wall.frame.shopfront", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"wall.frame.garagedoor", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"wall.frame.shopfront.metal", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"industrial.wall.light", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"industrial.wall.light.green", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"industrial.wall.light.red", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.hbhfsensor", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"industrial.combiner", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"industrial.crafter", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"industrial.splitter", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.battery.rechargable.large", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.laserdetector", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.battery.rechargable.medium", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.pressurepad", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"modularcarlift", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"ptz.cctv.camera", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.rf.broadcaster", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.rf.receiver", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.battery.rechargable.small", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"smart.alarm", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"smart.switch", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"storage.monitor", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.switch", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.teslacoil", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"electric.timer", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"bed", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"waterpump", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"skull.trophy", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                                {"connected.speaker", new LimitEntry(){ basePrice=25f, multiplier=1.1f, free=3, max=10 } },
                            },
                       }
                    },
                },
                UIColors = new() {
                    { "mainBackground", "0 0.25 0.25 0.92" },
                    { "headerBackground", "0 0 0 1" },
                    { "titleBackground", "0 0.25 0.25 0.5" },
                    { "contentBackground", "0 0 0 0" },
                    { "rowBackground", "0 0 0 0.9" },
                    { "", "0 0 0 0" },
                },
                buildingBlocks = new() {
                    { "floor", new("Floor", "assets/prefabs/building core/floor/floor.png") },
                    { "floor.frame", new("Floor Frame", "assets/prefabs/building core/floor.frame/floor.frame.png") },
                    { "floor.triangle", new("Floor Triangle", "assets/prefabs/building core/floor.triangle/floor.triangle.png") },
                    { "floor.triangle.frame", new("Floor Triangle Frame", "assets/prefabs/building core/floor.triangle.frame/floor.triangle.frame.png") },
                    { "foundation", new("Foundation", "assets/prefabs/building core/foundation/foundation.png") },
                    { "foundation.triangle", new("Triangle Foundation", "assets/prefabs/building core/foundation.triangle/foundation.triangle.png") },
                    { "roof", new("Roof", "assets/prefabs/building core/roof/roof.png") },
                    { "roof.triangle", new("Roof Triangle", "assets/prefabs/building core/roof.triangle/roof.triangle.png") },
                    { "block.stair.lshape", new("Stairs L Shape", "assets/prefabs/building core/stairs.l/stairs_l.png") },
                    { "block.stair.spiral", new("Stairs Spiral", "assets/prefabs/building core/stairs.spiral/stairs_spiral.png") },
                    { "block.stair.spiral.triangle", new("Stairs Spiral Triangle", "assets/prefabs/building core/stairs.spiral.triangle/stairs.triangle.spiral.png") },
                    { "block.stair.ushape", new("U Shaped Stairs", "assets/prefabs/building core/stairs.u/stairs_u.png") },
                    { "foundation.steps", new("Steps", "assets/prefabs/building core/foundation.steps/foundation.steps.png") },
                    { "ramp", new("A ramp", "assets/prefabs/building core/ramp/ramp.png") },
                    { "wall", new("Wall", "assets/prefabs/building core/wall/wall.png") },
                    { "wall.doorway", new("Doorway", "assets/prefabs/building core/wall.doorway/wall.doorway.png") },
                    { "wall.frame", new("Wall Frame", "assets/prefabs/building core/wall.frame/wall.frame.png") },
                    { "wall.half", new("Half Wall", "assets/prefabs/building core/wall.half/wall.half.png") },
                    { "wall.low", new("Low Wall", "assets/prefabs/building core/wall.low/wall.third.png") },
                    { "wall.window", new("Window", "assets/prefabs/building core/wall.window/wall.window.png") },
                },
                lut = new() {
                    { "industrial.wall.lamp.green.deployed", "industrial.wall.light.green" },
                    { "industrial.wall.lamp.deployed", "industrial.wall.light" },
                    { "industrial.wall.lamp.red.deployed", "industrial.wall.light.red" },
                    { "simplelight", "electric.simplelight" },
                    { "sign.small.wood", "sign.wooden.small" },
                    { "vendingmachine.deployed", "vending.machine" },
                    { "splitter", "electric.splitter" },
                    { "weaponrack_tall.deployed", "gunrack_tall.horizontal" },
                    { "survivalfishtrap.deployed", "fishtrap.small" },
                    { "sam_site_turret_deployed", "samsite" },
                    { "cctv_deployed", "cctv.camera" },
                    { "woodbox_deployed", "box.wooden" },
                    { "stocking_large_deployed", "stocking.large" },
                    { "stocking_small_deployed", "stocking.small" },
                    { "small_stash_deployed", "stash.small" },
                    { "sleepingbag_leather_deployed", "sleepingbag" },
                    { "refinery_small_deployed", "small.oil.refinery" },
                    { "repairbench_deployed", "box.repair.bench" },
                    { "researchtable_deployed", "research.table" },
                    { "pookie_deployed", "pookie.bear" },
                    { "easter_door_wreath_deployed", "easterdoorwreath" },
                    { "christmas_door_wreath_deployed", "xmasdoorwreath" },
                    { "ptz_cctv_deployed", "ptz.cctv.camera" },
                    { "reactivetarget_deployed", "target.reactive" },
                    { "autoturret_deployed", "autoturret" },
                    { "bed_deployed", "bed" },
                    { "rustigeegg_d.deployed", "rustige_egg_d" },
                    { "rustigeegg_c.deployed", "rustige_egg_c" },
                    { "rustigeegg_e.deployed", "rustige_egg_e" },
                    { "rustigeegg_b.deployed", "rustige_egg_b" },
                    { "rustigeegg_a.deployed", "rustige_egg_a" },
                    { "rustigeegg_f.deployed", "rustige_egg_f" },
                    { "romancandle-blue", "firework.romancandle.blue" },
                    { "volcanofirework", "firework.volcano" },
                    { "volcanofirework-red", "firework.volcano.red" },
                    { "romancandle-violet", "firework.romancandle.violet" },
                    { "volcanofirework-violet", "firework.volcano.violet" },
                    { "romancandle", "firework.romancandle.red" },
                    { "mortarorange", "firework.boomer.orange" },
                    { "mortarred", "firework.boomer.red" },
                    { "mortarchampagne", "firework.boomer.champagne" },
                    { "mortarblue", "firework.boomer.blue" },
                    { "mortargreen", "firework.boomer.green" },
                    { "mortarviolet", "firework.boomer.violet" },
                    { "mortarpattern", "firework.boomer.pattern" },
                    { "industrialconveyor.deployed", "industrial.conveyor" },
                    { "industrialcombiner.deployed", "industrial.combiner" },
                    { "industrialcrafter.deployed", "industrial.crafter" },
                    { "industrialsplitter.deployed", "industrial.splitter" },
                    { "coffinstorage", "coffin.storage" },
                    { "small_fuel_generator.deployed", "electric.fuelgenerator.small" },
                    { "generator.small", "electric.generator.small" },
                    { "solarpanel.large.deployed", "electric.solarpanel.large" },
                    { "electrical.modularcarlift.deployed", "modularcarlift" },
                    { "gravestone.stone.deployed", "gravestone" },
                    { "gravestone.wood.deployed", "woodcross" },
                    { "smallcandleset", "smallcandles" },
                    { "graveyardfence", "wall.graveyard.fence" },
                    { "waterbarrel", "water.barrel" },
                    { "sign.medium.wood", "sign.wooden.medium" },
                    { "sign.large.wood", "sign.wooden.large" },
                    { "sign.huge.wood", "sign.wooden.huge" },
                    { "hitchtrough.deployed", "hitchtroughcombo" },
                    { "water_catcher_large", "water.catcher.large" },
                    { "door.hinged.industrial.d", "door.hinged.metal" },
                    { "doorcloser", "door.closer" },
                    { "audioalarm", "electric.audioalarm" },
                    { "legacy_furnace", "furnace" },
                    { "igniter.deployed", "electric.igniter" },
                    { "fluidswitch", "fluid.switch" },
                    { "fluidsplitter", "fluid.splitter" },
                    { "electrical.heater", "electric.heater" },
                    { "poweredwaterpurifier.deployed", "powered.water.purifier" },
                    { "andswitch.entity", "electric.andswitch" },
                    { "timer", "electric.timer" },
                    { "button", "electric.button" },
                    { "cabletunnel", "electric.cabletunnel" },
                    { "electrical.random.switch.deployed", "electric.random.switch" },
                    { "orswitch.entity", "electric.orswitch" },
                    { "doorcontroller.deployed", "electric.doorcontroller" },
                    { "electricfurnace.deployed", "electric.furnace" },
                    { "storagemonitor.deployed", "storage.monitor" },
                    { "smartalarm", "smart.alarm" },
                    { "smallrechargablebattery.deployed", "electric.battery.rechargable.small" },
                    { "rfbroadcaster", "electric.rf.broadcaster" },
                    { "rfreceiver", "electric.rf.receiver" },
                    { "counter", "electric.counter" },
                    { "teslacoil.deployed", "electric.teslacoil" },
                    { "hbhfsensor.deployed", "electric.hbhfsensor" },
                    { "switch", "electric.switch" },
                    /*
                    { "electric.windmill.small", "generator.wind.scrap" },
                    { "spiderweba", "spiderweb" },

                    { "beartrap", "trap.bear" },
                    { "landmine", "trap.landmine" },
                    { "waterpurifier", "water.purifier" },

                    { "weaponrack_wide", "gunrack_wide.horizontal" },
                    { "weaponrack_stand", "gunrack_stand" },
                    { "skull_door_knocker", "skulldoorknocker" },
                    { "railroadplanter", "rail.road.planter" },
                    { "weaponrack_horizontal", "gunrack.horizontal" },
                    { "weaponrack_single2", "gunrack.single.2.horizontal" },
                    { "weaponrack_single3", "gunrack.single.3.horizontal" },
                    { "weaponrack_single1", "gunrack.single.1.horizontal" },
                    { "double_doorgarland", "xmas.double.door.garland" },
                    { "windowgarland", "xmas.window.garland" },
                    { "doorgarland", "xmas.door.garland" },
                    { "xmas_tree", "xmas.tree" },
                    { "chippyarcademachine", "arcade.machine.chippy" },
                    { "advendcalendar", "xmas.advent" },
                    { "barricade.cover.wood", "barricade.wood.cover" },
                    { "water_catcher_small", "water.catcher.small" },
                    { "icewall", "wall.ice.wall" },

                    { "wall.external.high.wood", "wall.external.high" },

                    { "electrical.blocker.deployed", "electric.blocker" },
                    { "large.rechargable.battery.deployed", "electric.battery.rechargable.large" },
                    { "laserdetector", "electric.laserdetector" },
                    { "medium.rechargable.battery", "electric.battery.rechargable.medium" },
                    { "pressurepad", "electric.pressurepad" },
                    { "smartswitch", "smart.switch" },
                    { "xorswitch.entity", "electric.xorswitch" },
                    { "water.pump.deployed", "waterpump" },
                    { "skulltrophy", "skull.trophy" },
                    { "connectedspeaker", "connected.speaker" },
                    { "mining_quarry", "mining.quarry" },
                    { "pump_jack", "mining.pumpjack" },
                    */
                },
            };
            if (ServerRewards != null) {
                config.currency = 1;
                config.currencyName = "RP";
                config.currencyPrefix = "￠";
            } else if (Economics != null) {
                config.currency = 2;
                config.currencyName = "Econ";
                config.currencyPrefix = "$";

            }
        }
        protected override void SaveConfig() {
            if (configDirty) {
                Config.WriteObject(config);
                Puts("Saved Configuration.");
                configDirty = false;
            }
        }
        private static void DirtyConfig() {
            //if (!configDirty) {
            //if (_plugin != null) _plugin.Puts("Configuration was marked dirty...");
            configDirty = true;
            //}
        }
        #endregion
        #region Oxide Hooks - Permissions
        internal void OnGroupPermissionGranted(string groupName, string perm) {
            if (allPermissions.Contains(perm)) {
                string[] members = permission.GetUsersInGroup(groupName);
                //_plugin.Puts($"OnGroupPermissionGranted(groupName:{groupName},perm:{perm}) members ({members.Length}): {string.Join(", ", members)}");
                foreach (string userId in members) {
                    PlayerData.GetPerm(userId);
                }
            }
        }
        internal void OnGroupPermissionRevoked(string groupName, string perm) {
            if (allPermissions.Contains(perm)) {
                string[] members = permission.GetUsersInGroup(groupName);
                //_plugin.Puts($"OnGroupPermissionRevoked(groupName:{groupName},perm:{perm}) members ({members.Length}): {string.Join(", ", members)}");
                foreach (string userId in members) {
                    PlayerData.GetPerm(userId);
                }
            }
        }
        internal void OnUserPermissionGranted(string userId, string perm) {
            if (allPermissions.Contains(perm)) {
                PlayerData.GetPerm(userId);
                //_plugin.Puts($"OnUserPermissionGranted(userId:{userId},perm:{perm})");
            }
        }
        internal void OnUserPermissionRevoked(string userId, string perm) {
            if (allPermissions.Contains(perm)) {
                PlayerData.GetPerm(userId);
                //_plugin.Puts($"OnUserPermissionRevoked(userId:{userId},perm:{perm})");
            }
        }
        internal void OnUserGroupAdded(string userId, string groupName) {
            PlayerData.GetPerm(userId);
            //_plugin.Puts($"OnUserGroupAdded(userId:{userId},groupName:{groupName})");
        }
        internal void OnUserGroupRemoved(string userId, string groupName) {
            PlayerData.GetPerm(userId);
            //_plugin.Puts($"OnUserGroupRemoved(userId:{userId},groupName:{groupName})");
        }
        #endregion
        #region Functional Utils - void SendEffect(BasePlayer player, string effectPrefab)
        private static void SendEffect(BasePlayer player, string effectPrefab) {
            if (string.IsNullOrWhiteSpace(effectPrefab)) return;
            _reusableEffect.Init(Effect.Type.Generic, player, 0, Vector3.zero, Vector3.forward);
            _reusableEffect.pooledString = effectPrefab;
            EffectNetwork.Send(_reusableEffect, player.net.connection);
        }
        #endregion

        #region void Cmd(BasePlayer player, string command, string[] args)
        internal bool CCmd(ConsoleSystem.Arg arg) {
            BasePlayer player = arg.Player();
            if (player == null) return false;
            Cmd(player, config.command, arg.Args);
            return true;
        }
        internal void Cmd(BasePlayer player, string command, string[] args) {
            //_plugin.Puts($"Cmd({player.UserIDString},{command},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")})");
            bool isAdmin = permission.UserHasPermission(player.UserIDString, config.adminPermission);
            string perm = PlayerData.GetPerm(player.userID); /// this contains the null check for playerData for lots of follow-on functions. do not remove <3
            ConfigData.PermissionEntry permE = ConfigData.PermissionEntry.blank;
            if (!perm.IsNullOrEmpty() && limitsPermissions.Contains(perm)) permE = config.permissions[perm];
            if (args.Length == 0) { // /bshop
                BuildingShopUI.Show(player, isAdmin);
                BuildingShopUI.Players[player.userID].Tab(BuildingShopUI.TabIndex.Limits, player);
                return;
            }
            string cmd = args[0].ToLower();
            switch (cmd) {
                case "buy": // /bshop buy <shortname> [qty]
                    if (permE == ConfigData.PermissionEntry.blank) { player.ChatMessage(LangMessage(player.UserIDString, "noLimitsPermission", null)); return; }
                    string shortname = ""; int qty = 1;
                    if (args.Length >= 2) shortname = args[1];
                    if (args.Length >= 3) int.TryParse(args[2], out qty);

                    PlacedUI.CloseUI(player);
                    if (shortname.IsNullOrEmpty() || !permE.sortedLimits.Contains(shortname)) {
                        player.ChatMessage($"Error: Unknown shortname \"{shortname}\"\nUsage: /{config.command} buy <shortname> [qty]\n EX: /{config.command} buy furnace.large");
                        return;
                    }
                    LimitEntry limit = permE.limits[shortname];
                    if (limit.max == 0) {
                        player.ChatMessage($"Sorry, \"{shortname}\" may not be purchased."); return;
                    }
                    int purchaseCount = Purchases.Get(player.userID, shortname);
                    if (purchaseCount >= limit.max && limit.max != -1) {
                        player.ChatMessage($"Sorry, you have already purchased the Maximum ({purchaseCount}/{limit.max}) of {shortname}");
                        return;
                    }
                    int targetCount = purchaseCount + qty;
                    if (targetCount > limit.max && limit.max != -1) targetCount = limit.max;
                    uint upgradeCost = limit.GetTotalCost(purchaseCount, qty);
                    if (!Purchases.ChargePlayer(player.userID, (int)upgradeCost)) {
                        player.ChatMessage($"Sorry, You don't seem to have the {Purchases.DispCurrency(upgradeCost, "#A04040")}!");
                        return;
                    }
                    Purchases.Inc(player.userID, shortname, qty, upgradeCost);
                    if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                        BuildingShopUI.Players[player.userID].UpdateBalance(player, false);
                        BuildingShopUI.Players[player.userID].Reload(player, limit);
                    }
                    SendEffect(player, config.sounds["purchase"]);
                    player.ChatMessage($"Done. You are now the proud owner of {targetCount}/{limit.max} x {shortname}, for <color=#40A040>{upgradeCost:N0} {config.currencyName}</color>!");
                    return;
                case "rebuildcache": // /bshop rebuildcache
                    if (!isAdmin) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    PlayerData.data.Clear();
                    PlayerData.Lookup();
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdCacheWiped", null));
                    return;
                case "wipepurchases": // /bshop wipepurchases
                    if (!isAdmin) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    Purchases.Wipe();
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdWipedGlobal", null));
                    BuildingShopUI.ReloadTab(player);
                    return;
                case "refund": // /bshop refund [userID]
                    if (!isAdmin && config.refundMultiplier == 0d) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    ulong userID;
                    if (!isAdmin || args.Length < 2 || !ulong.TryParse(args[1], out userID)) { userID = player.userID; }
                    if (!Purchases.players.ContainsKey(userID) || Purchases.players[userID].currencySpent < 1) {
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdAdminRefundFailedNoneSpent", new string[] { userID.ToString(), config.currencyName })); return;
                    }
                    if (Purchases.players[userID].Refund(userID, (isAdmin || player.IsAdmin ? 1d : config.refundMultiplier))) {
                        if (userID != player.userID) player.ChatMessage(LangMessage(player.UserIDString, "cmdAdminRefunded", new string[] { userID.ToString() }));
                    } else {
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdAdminRefundFailed", new string[] { userID.ToString() }));
                    }
                    if (BuildingShopUI.Players.ContainsKey(userID) && BasePlayer.TryFindByID(userID, out BasePlayer basePlayer)) {
                        BuildingShopUI.Players[userID].UpdateBalance(basePlayer, false);
                        BuildingShopUI.ReloadTab(basePlayer);
                    }
                    return;
                case "unwipepurchases": // /bshop unwipepurchases
                    if (!isAdmin) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    if (Purchases.UnWipe()) player.ChatMessage(LangMessage(player.UserIDString, "cmdAdminPurchasesRestored", null));
                    else player.ChatMessage(LangMessage(player.UserIDString, "cmdErrAdminPurchasesRestoreFailed", null));
                    BuildingShopUI.ReloadTab(player);
                    return;
                case "admin": // /bshop admin
                    if (!player.IsAdmin && !permission.UserHasGroup(player.UserIDString, "admin")) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    if (!config.adminPermission.StartsWith("BuildingShop.")) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrForeignPermission", new string[] { config.adminPermission })); return; }
                    if (permission.UserHasPermission(player.UserIDString, config.adminPermission)) {
                        permission.RevokeUserPermission(player.UserIDString, config.adminPermission);
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdAdminRevoked", new string[] { config.adminPermission }));
                    } else {
                        permission.GrantUserPermission(player.UserIDString, config.adminPermission, this);
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdAdminGranted", new string[] { config.adminPermission }));
                    }
                    if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                        BuildingShopUI.Players[player.userID].isAdmin = !BuildingShopUI.Players[player.userID].isAdmin;
                        BuildingShopUI.Players[player.userID].DrawHeader(player);
                        BuildingShopUI.Players[player.userID].Tab(BuildingShopUI.TabIndex.Limits, player, true);
                    }
                    return;
                case "loaddefaultconfig": // /bshop loaddefaultconfig
                    if (!isAdmin) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    LoadDefaultConfig();
                    ValidateConfig();
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdAdminLoadDefaultConfig", null));
                    BuildingShopUI.ReloadTab(player);
                    return;
                case "ui": // /bshop ui ...
                    double todo; // update ALL references to args, to take the source array
                    if (args.Length > 1) {
                        string[] UIArgs = new string[args.Length - 1];
                        Array.Copy(args, 1, UIArgs, 0, UIArgs.Length);
                        BuildingShopUI.Cmd(player, UIArgs, isAdmin);
                        return;
                    }
                    break;
                case "perm": // /bshop perm ...
                    if (!isAdmin) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    if (args.Length > 1) {
                        string[] PermArgs = new string[args.Length - 1];
                        Array.Copy(args, 1, PermArgs, 0, PermArgs.Length);
                        CmdPerm(player, PermArgs);
                        return;
                    }
                    break;
                case "config": // /bshop config ...
                    if (!isAdmin) { player.ChatMessage(LangMessage(player.UserIDString, "noAdminPermission", null)); return; }
                    if (args.Length > 1) {
                        string[] CFGArgs = new string[args.Length - 1];
                        Array.Copy(args, 1, CFGArgs, 0, CFGArgs.Length);
                        CmdConfig(player, CFGArgs);
                        return;
                    }
                    break;
                default:
                    break;
            }
            // /bshop ***
            player.ChatMessage(LangMessage(player.UserIDString, "cmdErrUnknownCommand", new string[] { $"{command} {string.Join(" ", args)}" }));
            return;
        }
        #endregion
        #region void CmdPerm(BasePlayer player, string[] args)
        internal void CmdPerm(BasePlayer player, string[] args) {
            //_plugin.Puts($"CmdPerm({player.UserIDString},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")})");
            if (args == null || args.Length == 0) return;
            if (args.Length == 1) {
                if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                    if (!args[0].IsNullOrEmpty() && limitsPermissions.Contains(args[0])) {// /bshop perm <permName>
                        BuildingShopUI.Players[player.userID].selPerm = args[0];
                        BuildingShopUI.ReloadTab(player, 0);
                    } else if (args[0].Equals("None")) {
                        BuildingShopUI.Players[player.userID].selPerm = null;
                        BuildingShopUI.ReloadTab(player, 0);
                    } else {
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm <permName>" }));
                    }
                }
                return;
            }
            if (args.Length < 2 || args[1].IsNullOrEmpty()) {
                player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm <permName> <command> ..." }));
                return;
            }
            switch (args[0]) {
                case "new": // /bshop perm new <permName>
                    if (args[1] == "BuildingShop.") return;
                    if (limitsPermissions.Contains(args[1])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrAlreadyExists", new string[] { "Permission", args[1] })); return; }
                    config.permissions[args[1]] = new();
                    DirtyConfig();
                    ValidateConfig();
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdCreated", new string[] { "Permission", args[1] }));
                    if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                        BuildingShopUI.Players[player.userID].selPerm = args[1];
                        BuildingShopUI.Players[player.userID].DrawHeader(player);
                        BuildingShopUI.ReloadTab(player);
                    }
                    return;
                case "delete": // /bshop perm delete <permName>
                    if (!limitsPermissions.Contains(args[1])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrDoesntExist", new string[] { "Permission", args[1] })); return; }
                    config.permissions.Remove(args[1]);
                    DirtyConfig();
                    ValidateConfig();
                    if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                        if (BuildingShopUI.Players[player.userID].selPerm == args[1]) BuildingShopUI.Players[player.userID].selPerm = null;
                        BuildingShopUI.ReloadTab(player);
                    }
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdRemoved", new string[] { "Permission", "args[1]" }));
                    return;
                case "priority": // /bshop perm priority <permName> <newPriority>
                    if (!limitsPermissions.Contains(args[1])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrDoesntExist", new string[] { "Permission", args[1] })); return; }
                    ConfigData.PermissionEntry permEnt = config.permissions[args[1]];
                    if (args.Length < 3 || !int.TryParse(args[2], out int newPriority)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm priority <permName> <newPriority>" })); return; }
                    if (permEnt.priority != newPriority) {
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Permission " + args[1], "priority", permEnt.priority.ToString("N0"), newPriority.ToString("N0") }));
                        permEnt.priority = newPriority;
                        DirtyConfig();
                        ValidateConfig();
                    }
                    return;
                case "globalLimit": // /bshop perm globalLimit <permName> <newGlobalLimit>
                    if (!limitsPermissions.Contains(args[1])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrDoesntExist", new string[] { "Permission", args[1] })); return; }
                    ConfigData.PermissionEntry permEnt2 = config.permissions[args[1]];
                    if (args.Length < 3 || !int.TryParse(args[2], out int newGlobalLimit)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm globalLimit <permName> <newGlobalLimit>" })); return; }
                    if (permEnt2.globalLimit != newGlobalLimit) {
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Permission " + args[1], "globalLimit", permEnt2.globalLimit.ToString("N0"), newGlobalLimit.ToString("N0") }));
                        permEnt2.globalLimit = newGlobalLimit;
                        DirtyConfig();
                        ValidateConfig();
                    }
                    return;
                default: break;
            }
            string permName = args[0];
            if (permName.IsNullOrEmpty() || !limitsPermissions.Contains(permName)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrDoesntExist", new string[] { "Permission", permName })); return; }
            if (args.Length < 3) {
                if (args[1] == "new") return;// empty command from UI doesn't get error message
                player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm <permName> <entName> <cmd> [...]" }));
                return;
            }
            ConfigData.PermissionEntry permE = config.permissions[permName];
            if (args[1] == "new") { // /bshop perm <permName> new <entName>
                if (args[2].Length == 0) return;
                string newEnt = args[2];
                if (permE.entLimits.ContainsKey(newEnt)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrAlreadyExists", new string[] { "Entity", newEnt })); return; }
                LimitEntry limit = permE.limits[newEnt] = new LimitEntry() { basePrice = 0f, multiplier = 1f, max = 0, free = 0 };
                DirtyConfig();
                ValidateConfig();
                player.ChatMessage(LangMessage(player.UserIDString, "cmdAdded", new string[] { "Entity", newEnt, "Permission", permName }));
                if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                    if (BuildingShopUI.Players[player.userID].firstIndex != limit.order % 10) BuildingShopUI.ReloadTab(player, limit.order % 10);
                    else BuildingShopUI.Players[player.userID].Reload(player, permE.limits[newEnt]);
                }
                return;
            }
            string par = "";
            if (args.Length >= 4) par = args[3];
            string entName = args[1];
            if (entName.IsNullOrEmpty() || !permE.sortedLimits.Contains(entName)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrDoesntExist", new string[] { "Entity", entName })); return; }
            LimitEntry ent = permE.entLimits[entName];
            switch (args[2]) {
                case "delete": // /bshop perm <permName> <entName> delete
                    permE.limits.Remove(entName);
                    permE.sortedLimits.Remove(entName);
                    DirtyConfig();
                    ValidateConfig();
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdRemovedFrom", new string[] { "Entity", entName, "Permission", permName }));
                    BuildingShopUI.ReloadTab(player);
                    return;
                case "free": // /bshop perm <permName> <entName> free <newFree>
                    if (!int.TryParse(par, out int newFree)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm <permName> <entName> free <newFree>" })); return; }
                    if (ent.free == newFree) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { $"Permission {permName} entity {entName}", "Free Count", ent.free.ToString("N0"), newFree.ToString("N0") }));
                    ent.free = newFree;
                    DirtyConfig();
                    return;
                case "max": // /bshop perm <permName> <entName> max <-1|0|newMax>
                    if (!int.TryParse(par, out int newMax)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm <permName> <entName> max <-1|0|newMax>" })); return; }
                    if (ent.max == newMax) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { $"Permission {permName} entity {entName}", "Max Purchases", ent.max.ToString("N0"), newMax.ToString("N0") }));
                    ent.max = newMax;
                    DirtyConfig();
                    BuildingShopUI.Reload_s(player, ent);
                    return;
                case "price": // /bshop perm <permName> <entName> price <newBasePrice>
                    if (!double.TryParse(par, out double newBasePrice)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm <permName> <entName> price <newBasePrice>" })); return; }
                    if (ent.basePrice == newBasePrice) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { $"Permission {permName} entity {entName}", "Base Price", ent.basePrice.ToString("N0"), newBasePrice.ToString("N0") }));
                    ent.basePrice = newBasePrice;
                    DirtyConfig();
                    return;
                case "multiplier": // /bshop perm <permName> <entName> multiplier <newMultiplier>
                    if (!double.TryParse(par, out double newMultiplier)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm <permName> <entName> multiplier <newMultiplier>" })); return; }
                    if (ent.multiplier == newMultiplier) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { $"Permission {permName} entity {entName}", "Price Multiplier", ent.multiplier.ToString("P"), newMultiplier.ToString("P") }));
                    ent.multiplier = newMultiplier;
                    DirtyConfig();
                    return;
                case "groupMember": // /bshop perm <permName> <entName> groupMember new <newGroupMember> [newGroupContribution]|delete <groupMember>
                    if (args.Length < 5) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} perm <permName> <entName> groupMember new <newGroupMember> [newGroupContribution]|delete <groupMember>" })); return; }
                    if (par == "new") {
                        int newGroupContribution = 1;
                        string newGroupMember = args[4];
                        if (!config.buildingBlocks.ContainsKey(newGroupMember) && !ItemManager.itemDictionaryByName.ContainsKey(newGroupMember)) {
                            player.ChatMessage(LangMessage(player.UserIDString, "cmdErrNotValid", new string[] { newGroupMember, "Item Shortname" }));
                            return;
                        }
                        if (ent.groupMembers.ContainsKey(newGroupMember)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrAlreadyMember", new string[] { "Group Member", newGroupMember, ent.groupMembers[newGroupMember].displayNameColor })); return; }
                        if (permE.entLimits.ContainsKey(newGroupMember)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrAlreadyMember", new string[] { "Group Member", newGroupMember, permName })); return; }
                        if (args.Length == 6 && !int.TryParse(args[5], out newGroupContribution)) newGroupContribution = 1;
                        LimitEntry.Stub newGM = new(newGroupContribution);
                        LimitEntry.Resolve(ent, newGroupMember);
                        ent.groupMembers.Add(newGroupMember, newGM);
                        permE.entLimits.Add(newGroupMember, ent);
                        DirtyConfig();
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdAdded", new string[] { newGM.categoryNameColor, newGM.displayNameColor, $"Permission {permName} Group {ent.stub.displayNameColor}" }));
                        player.ChatMessage($"Permission {permName} entity {entName} Group Member {newGroupMember} added: {newGM.categoryNameColor} {newGM.displayNameColor}{(newGroupContribution == 1 ? "" : $" {newGroupContribution}")}");
                        if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                            BuildingShopUI.Players[player.userID].dropdownName = null;
                            BuildingShopUI.Reload_s(player, ent);
                        }
                        return;
                    }
                    if (par == "delete") {
                        string delGroupMember = args[4];
                        if (ent.groupMembers?.ContainsKey(delGroupMember) ?? false) {
                            ent.groupMembers.Remove(delGroupMember);
                            permE.entLimits.Remove(delGroupMember);
                            DirtyConfig();
                            player.ChatMessage(LangMessage(player.UserIDString, "cmdRemoved", new string[] { $"Permission {permName} entity {entName}", "Group Member", delGroupMember }));
                            if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                                BuildingShopUI.Players[player.userID].dropdownName = null;
                                BuildingShopUI.Reload_s(player, ent);
                            }
                        } else {
                            player.ChatMessage(LangMessage(player.UserIDString, "cmdErrNotMember", new string[] { delGroupMember, ent.stub.displayNameColor }));
                        }
                        return;
                    }
                    break;
                default: break;
            }
            player.ChatMessage($"Unhandled: {config.command} perm {string.Join(" ", args)}");
        }
        #endregion
        #region void CmdConfig(BasePlayer player, string[] args)
        internal void CmdConfig(BasePlayer player, string[] args) {
            //_plugin.Puts($"CmdConfig({player.UserIDString},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")})");
            if (args == null || args.Length < 2) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config <setting> <value>" })); return; }
            switch (args[0]) {
                case "adminPermission": // /bshop config adminPermission <newPermissionName>        //[JsonProperty(PropertyName = "Admin Permission")] public string adminPermission;
                    if (args[1] == config.adminPermission || args[1].Length < 3) return;
                    if (limitsPermissions.Contains(args[1])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrAlreadyExists", new string[] { "Permission", args[2] })); return; }
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config", "Admin Permission", config.adminPermission, args[1] }));
                    config.adminPermission = args[1];
                    DirtyConfig();
                    ValidateConfig();
                    return;
                case "command": // /bshop config command <newCommand>        //[JsonProperty(PropertyName = "Command")] public string command;
                    if (args[1] == config.command || args[1].Length == 0) return;
                    cmd.RemoveChatCommand(config.command, this);
                    cmd.RemoveConsoleCommand(config.command, this);
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config", "Primary Command", config.command, args[1] }));
                    config.command = args[1];
                    DirtyConfig();
                    ValidateConfig();
                    return;
                case "commands": // /bshop config commands <add|delete> <command>        //[JsonProperty(PropertyName = "Commands")] public List<string> commands;
                    if (args.Length < 3 || args[2].Length < 1) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config commands <new|delete> <command>" })); return; }
                    if (args[1].Equals("new", StringComparison.OrdinalIgnoreCase)) {
                        if (args[2].Equals(config.command, StringComparison.OrdinalIgnoreCase) || config.commands.Contains(args[2])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrAlreadyExists", new string[] { "Additional Chat Command", args[2] })); return; }
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdCreated", new string[] { "Additional Chat Command", args[2] }));
                        config.commands.Add(args[2]);
                    } else if (args[1].Equals("delete", StringComparison.OrdinalIgnoreCase)) {
                        if (!config.commands.Contains(args[2])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrDoesntExist", new string[] { "Additional Chat Command", args[2] })); return; }
                        player.ChatMessage(LangMessage(player.UserIDString, "cmdRemoved", new string[] { "Additional Chat Command", args[2] }));
                        cmd.RemoveChatCommand(args[2], this);
                        config.commands.Remove(args[2]);
                    } else { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config commands <add|delete> <command>" })); return; ; }
                    DirtyConfig();
                    ValidateConfig();
                    if (BuildingShopUI.Players.ContainsKey(player.userID)) {
                        BuildingShopUI.Players[player.userID].dropdownName = null;
                        BuildingShopUI.Players[player.userID].Build_AdminRow_A(player, 11);
                    }
                    return;
                case "wipePurchases": // /bshop config wipePurchases <true|false>        //[JsonProperty(PropertyName = "Wipe Purchases with map?")] public bool wipePurchases;
                    bool newVal;
                    if (!bool.TryParse(args[1], out newVal)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config wipePurchases <true|false>" })); return; }
                    if (newVal == config.wipePurchases || args[1].Length == 0) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config", nameof(config.wipePurchases), config.wipePurchases.ToString(), newVal.ToString() }));
                    config.wipePurchases = newVal;
                    DirtyConfig();
                    // ValidateConfig();
                    if (BuildingShopUI.Players.ContainsKey(player.userID)) BuildingShopUI.Players[player.userID].Build_AdminRow_A(player, 11);
                    return;
                case "currency": // /bshop config currency <0|1|2|itemID>        //[JsonProperty(PropertyName = "ItemID for purchase costs")] public int currency;
                    int currency;
                    switch (args[1]) {
                        case "disabled": currency = 0; break;
                        case "economics": currency = 1; break;
                        case "serverrewards": currency = 2; break;
                        default: if (!int.TryParse(args[1], out currency)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config currency <disabled|economics|serverrewards|itemID>" })); return; } break;
                    }
                    if (currency == config.currency) return;

                    /*
                    string newCurrencyName = config.currencyName;
                    switch (currency) {
                        case 0: newCurrencyName = "Disabled"; break;
                        case 1: newCurrencyName = "Economics"; break;
                        case 2: newCurrencyName = "ServerRewards"; break;
                        default:



                    }
                    ItemDefinition itemdef = ItemManager.FindItemDefinition(entName);
                    if (itemdef == null) itemdef = ItemManager.FindItemDefinition(entName.Replace(".deployed", string.Empty));
                    if (itemdef != null) {
                        entityMap[entName] = itemdef.shortname;
                        return entityMap[entName];
                    }

                    */

                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config", nameof(config.currency), config.currency.ToString("N0"), currency.ToString("N0") }));
                    config.currency = currency;
                    DirtyConfig();
                    // ValidateConfig();
                    return;
                case "currencyName": // /bshop config currencyName <currencyName>
                    if (args[1] == config.currencyName || args[1].Length == 0) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config currencyName <currencyName>" })); return; }
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config", nameof(config.currencyName), config.currencyName, args[1] }));
                    config.currencyName = args[1];
                    DirtyConfig();
                    // ValidateConfig();
                    return;
                case "currencyPrefix": // /bshop config currencyPrefix <currencyPrefix>
                    if (args[1] == config.currencyPrefix || args[1].Length == 0) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config currencyPrefix <currencyPrefix>" })); return; }
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config", nameof(config.currencyPrefix), config.currencyPrefix, args[1] }));
                    config.currencyPrefix = args[1];
                    DirtyConfig();
                    // ValidateConfig();
                    return;
                case "infinity": // /bshop config infinity <infinity>
                    int infinity;
                    if (!int.TryParse(args[1], out infinity)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config infinity <infinity>" })); }
                    if (infinity == config.infinity) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config", nameof(config.infinity), config.infinity.ToString("N0"), infinity.ToString("N0") }));
                    config.infinity = infinity;
                    DirtyConfig();
                    // ValidateConfig();
                    return;
                case "refundMultiplier": // /bshop config refundMultiplier <refundMultiplier>
                    double refundMultiplier;
                    if (!double.TryParse(args[1], out refundMultiplier) || refundMultiplier < 0d || refundMultiplier > 1d) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config refundMultiplier <refundMultiplier (0.00-1.00)>" })); return; }
                    if (refundMultiplier == config.refundMultiplier) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config", "refundMultiplier", config.refundMultiplier.ToString("P"), refundMultiplier.ToString("P") }));
                    config.refundMultiplier = refundMultiplier;
                    DirtyConfig();
                    // ValidateConfig();
                    return;
                case "sound": // /bshop config sound <soundName> <assetPath>
                    if (args.Length < 3 || !config.sounds.ContainsKey(args[1]) || !args[2].StartsWith("assets/")) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config sounds <soundName> <assetPath>" })); return; }
                    if (args[2] == config.sounds[args[1]]) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config Sound", args[1], config.sounds[args[1]], args[2] }));
                    config.sounds[args[1]] = args[2];
                    DirtyConfig();
                    //ValidateConfig();
                    return;
                case "lut": // /bshop config lut <entityName> <itemName>
                    if (args.Length < 3) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config lut <entityName> <itemName>" })); return; }
                    if (!ItemManager.itemDictionaryByName.ContainsKey(args[2])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrNotValid", new string[] { args[2], "Item Shortname" })); return; }
                    if (config.lut.ContainsKey(args[1]) && args[2] == config.lut[args[1]]) return;
                    LimitEntry.Stub s = new();
                    s.ResolveDisplay(args[2], false);
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdConfigLutMapped", new string[] { args[1], s.displayNameColor, s.categoryNameColor }));
                    config.lut[args[1]] = args[2];
                    DirtyConfig();
                    //ValidateConfig();
                    return;
                case "color": // /bshop config color <colorName> <red> <green> <blue> <alpha>
                    if (args.Length < 6 || !double.TryParse(args[2], out double red) || !double.TryParse(args[3], out double green) || !double.TryParse(args[4], out double blue) ||
                        !double.TryParse(args[5], out double alpha)) { player.ChatMessage(LangMessage(player.UserIDString, "cmdUsage", new string[] { $"{config.command} config color <colorName> <red> <green> <blue> <alpha>" })); return; }
                    if (!config.UIColors.ContainsKey(args[2])) { player.ChatMessage(LangMessage(player.UserIDString, "cmdErrNotValid", new string[] { args[2], "UIColor" })); return; }
                    string newColor = $"{red:G3} {green:G3} {blue:G3} {alpha:G3}";
                    if (config.UIColors[args[1]] == newColor) return;
                    player.ChatMessage(LangMessage(player.UserIDString, "cmdChanged", new string[] { "Config color", args[1], config.UIColors[args[1]], newColor }));
                    config.UIColors[args[1]] = newColor;
                    DirtyConfig();
                    //ValidateConfig();
                    return;
                default: break;
            }
            player.ChatMessage($"Unhandled: {config.command} config {args[0]} \"{args[1]}\"");
        }
        #endregion

        #region class DB
        internal class DB {
            SQLite Sqlite = Interface.Oxide.GetLibrary<SQLite>();
            Connection conn;
        }
        #endregion
        #region class LimitEntry
        internal class LimitEntry {
            public double basePrice;
            public double multiplier;
            public int free;
            public int max;
            [JsonIgnore] public int order;
            public Stub stub;
#nullable enable
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)] public Dictionary<string, Stub>? groupMembers;
#nullable disable

            public class Stub {
                public int groupContribution;
#nullable enable
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)] public string? Name;
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)] public string? Icon;
#nullable disable
                [JsonIgnore] public string entName = string.Empty;
                [JsonIgnore] public string displayName = string.Empty;
                [JsonIgnore] public string categoryName = string.Empty;
                [JsonIgnore] public string displayNameColor = string.Empty;
                [JsonIgnore] public string categoryNameColor = string.Empty;
                [JsonIgnore] public CuiRawImageComponent icon = null;
                [JsonIgnore] public LimitEntry group = null;
                [JsonIgnore] public Dictionary<string, Stub> groupMembers = null;
                public Stub(int groupContribution = 1) {
                    this.groupContribution = groupContribution;
                }
                public void ResolveDisplay(string entName, bool isGroup) {
                    icon = new CuiRawImageComponent();
                    if (Icon.IsNullOrEmpty()) {
                        if (isGroup) {
                            icon.Sprite = "assets/icons/cart.png";
                        } else if (config.buildingBlocks.ContainsKey(entName)) {
                            icon.Sprite = config.buildingBlocks[entName].icon;
                        } else {
                            icon.Url = $"https://rustlabs.com/img/items180/{entName}.png";
                        }
                    } else {
                        if (Icon.StartsWith("assets/")) icon.Sprite = Icon;
                        else if (Icon.StartsWith("http")) icon.Url = Icon;
                        else icon.Url = $"https://rustlabs.com/img/items180/{Icon}.png";
                    }
                    if (isGroup) {
                        if (Name.IsNullOrEmpty()) displayName = entName;
                        else displayName = Name;
                        displayNameColor = $"<color=#C07070>{displayName}</color>";
                        categoryName = "";
                        categoryNameColor = "";
                    } else if (config.buildingBlocks.ContainsKey(entName)) {
                        displayName = config.buildingBlocks[entName].displayName;
                        categoryName = " Construction";
                        displayNameColor = $"<color=#A070A0>{displayName}</color>";
                        categoryNameColor = $"<color=#A070A0>{categoryName}</color>";
                    } else {
                        ItemDefinition itemdef = ItemManager.itemDictionaryByName[entName];
                        displayName = itemdef.displayName.english;
                        categoryName = itemdef.category.ToString();
                        string catCol = "9090A0";
                        switch (itemdef.category) {
                            case ItemCategory.Weapon: catCol = "556b2f"; break;
                            case ItemCategory.Construction: catCol = "483d8b"; break;
                            case ItemCategory.Items: catCol = "9acd32"; break;
                            case ItemCategory.Tool: catCol = "ba55d3"; break;
                            case ItemCategory.Traps: catCol = "e9967a"; break;
                            case ItemCategory.Misc: catCol = "1e90ff"; break;
                            case ItemCategory.Component: catCol = "7fffd4"; break;
                            case ItemCategory.Electrical: catCol = "855a5a"; break;
                            case ItemCategory.Fun: catCol = "008080"; break;
                            case ItemCategory.Attire:
                            case ItemCategory.Medical:
                            case ItemCategory.Food:
                            case ItemCategory.Ammunition:
                            case ItemCategory.All:
                            case ItemCategory.Common:
                            case ItemCategory.Search:
                            case ItemCategory.Favourite:
                            case ItemCategory.Resources: catCol = "C04040"; break;
                        }
                        displayNameColor = $"<color=#{catCol}>{displayName}</color>";
                        categoryNameColor = $"<color=#9090A0>{categoryName}</color>";
                    }
                }
            }
            public static void Resolve(LimitEntry limit, string entName) {
                limit.stub.entName = entName;
                if (limit.groupMembers != null) {
                    limit.stub.ResolveDisplay(entName, true);
                    foreach (KeyValuePair<string, Stub> member in limit.groupMembers) {
                        member.Value.group = limit;
                        member.Value.ResolveDisplay(member.Key, false);
                    }
                } else {
                    limit.stub.ResolveDisplay(entName, false);
                }
            }
            /// Credit for the math behind this, goes to aquafir(Discord).
            public uint GetTotalCost(int stepNumber, int qty) => GetTotalCost(stepNumber + qty) - GetTotalCost(stepNumber);
            public uint GetTotalCost(int stepNumber) => (uint)(multiplier == 1d ? (basePrice * stepNumber) : (basePrice * ((1 - Math.Pow(multiplier, stepNumber)) / (1 - multiplier))));
            public int Purchaseable(uint currency, int owned) => multiplier == 1d ? (int)(currency / basePrice) : (int)Math.Floor(Math.Log(Math.Pow(multiplier, owned) - (currency * (1 - multiplier) / basePrice), multiplier) - owned);
            public int CurrentLevel(uint totalSpent) => Purchaseable(totalSpent, 0);
        }
        #endregion

        #region class PlayerData
        private class PlayerData {
            public ulong userID = 0;
            public int placedTotal = 0;
            public Dictionary<string, HashSet<int>> placedEntities = new();
            public Dictionary<string, int> placedGroups = new();
            public DateTime permsExpire = DateTime.MinValue;
            private string _perm = string.Empty;
            public string Perm {
                get {
                    if (permsExpire < DateTime.Now) {
                        string perm = string.Empty;
                        string UserID = $"{userID}";
                        foreach (string p in limitsPermissions) { if (_plugin.permission.UserHasPermission(UserID, p)) { perm = p; break; } }
                        _perm = perm;
                        permsExpire = DateTime.Now + TimeSpan.FromMinutes(10);
                    }
                    return _perm;
                }
                internal set {
                    bool reset = false;
                    if (_perm != value) reset = true;
                    _perm = value;
                    permsExpire = DateTime.Now + TimeSpan.FromMinutes(180);
                    if (reset) ResetGroups(userID);
                }
            }
            internal int GetPlacedEntities(string entName) { if (placedEntities.TryGetValue(entName, out HashSet<int> entSet)) return entSet.Count; return 0; }
            internal int GetPlacedGroup(string groupName) { if (placedGroups.TryGetValue(groupName, out int cnt)) return cnt; return 0; }

            /// <summary>
            /// private static class PlayerData
            /// </summary>

            internal static int GetPlaced(ulong userID, string entName, bool isGroup) {
                if (data.ContainsKey(userID)) {
                    if (isGroup) return data[userID].GetPlacedGroup(entName);
                    return data[userID].GetPlacedEntities(entName);
                }
                return 0;
            }

            internal static Dictionary<string, string> entityMap = new();
            internal static Dictionary<ulong, PlayerData> data = new();
            public static PlayerData Player(ulong userID) {
                if (!data.ContainsKey(userID)) data[userID] = new() { userID = userID };
                return data[userID];
            }
            public static string GetPerm(ulong userID) => Player(userID).Perm;
            public static string GetPerm(string UserID) {
                ulong.TryParse(UserID, out ulong userID);
                return GetPerm(userID);
            }
            public static void ResetGroups(ulong userId) {
                data[userId].placedGroups.Clear();
                if (!data[userId].Perm.IsNullOrEmpty() && limitsPermissions.Contains(data[userId].Perm) && config.permissions[data[userId].Perm].limits != null) {
                    foreach (var group in config.permissions[data[userId].Perm].limits) {
                        if (group.Value.groupMembers != null) {
                            data[userId].placedGroups[group.Key] = 0;
                            foreach (KeyValuePair<string, LimitEntry.Stub> l in group.Value.groupMembers) {
                                data[userId].placedGroups[group.Key] += l.Value.groupContribution * data[userId].GetPlacedEntities(l.Key);
                            }
                        }
                    }
                }
            }
            public static string ResolveEntity(string entName) {
                if (!entityMap.ContainsKey(entName)) {
                    if (config.buildingBlocks.ContainsKey(entName)) {
                        entityMap[entName] = entName;
                        return entityMap[entName];
                    }
                    if (config.lut.ContainsKey(entName) && !config.lut[entName].IsNullOrEmpty()) {
                        entityMap[entName] = config.lut[entName];
                        return entityMap[entName];
                    }
                    ItemDefinition itemdef = ItemManager.FindItemDefinition(entName);
                    if (itemdef == null) itemdef = ItemManager.FindItemDefinition(entName.Replace(".deployed", string.Empty));
                    if (itemdef != null) {
                        entityMap[entName] = itemdef.shortname;
                        return entityMap[entName];
                    }
                    entityMap[entName] = "";
                    _plugin.Puts($"Failed to resolve {entName}");
                    config.lut[entName] = "";
                    DirtyConfig();
                    return entName;
                }
                if (entityMap[entName].IsNullOrEmpty()) return entName;
                return entityMap[entName];
            }
            public static void StoreEntity(BaseEntity entity) {
                string perm = GetPerm(entity.OwnerID);
                string shortname = ResolveEntity(entity.ShortPrefabName);
                int iid = entity.GetInstanceID();
                if (data[entity.OwnerID].placedEntities.ContainsKey(shortname)) {
                    if (data[entity.OwnerID].placedEntities[shortname].Contains(iid)) {
                        _plugin.PrintWarning($"DEBUG: StoreEntity called on iid:{iid}({shortname}) ownerid:{entity.OwnerID} but it already existed in placedEntities!");
                        return;
                    }
                    data[entity.OwnerID].placedEntities[shortname].Add(iid);
                } else {
                    data[entity.OwnerID].placedEntities[shortname] = new HashSet<int>() { iid };
                }
                if (!perm.IsNullOrEmpty() && limitsPermissions.Contains(perm) && config.permissions[perm].entLimits.ContainsKey(shortname) && config.permissions[perm].entLimits[shortname].groupMembers != null) { // groupcheck
                    var limit = config.permissions[perm].entLimits[shortname];
                    if (data[entity.OwnerID].placedGroups.ContainsKey(limit.stub.entName)) data[entity.OwnerID].placedGroups[limit.stub.entName] += limit.groupMembers[shortname].groupContribution;
                    else data[entity.OwnerID].placedGroups[limit.stub.entName] = limit.groupMembers[shortname].groupContribution;
                }
                data[entity.OwnerID].placedTotal++;
            }
            public static void RemoveEntity(BaseEntity entity) {
                string perm = GetPerm(entity.OwnerID);
                string shortname = ResolveEntity(entity.ShortPrefabName);
                int iid = entity.GetInstanceID();
                if (data[entity.OwnerID].placedEntities.ContainsKey(shortname)) {
                    if (data[entity.OwnerID].placedEntities[shortname].Contains(iid)) {
                        if (data[entity.OwnerID].placedEntities[shortname].Count == 1) {
                            data[entity.OwnerID].placedEntities.Remove(shortname);
                        } else {
                            data[entity.OwnerID].placedEntities[shortname].Remove(iid);
                        }
                        if (limitsPermissions.Contains(perm) && config.permissions[perm].entLimits.ContainsKey(shortname) && config.permissions[perm].entLimits[shortname].groupMembers != null) { // groupcheck
                            var limit = config.permissions[perm].entLimits[shortname];
                            if (data[entity.OwnerID].placedGroups.ContainsKey(limit.stub.entName)) {
                                if (limit.groupMembers[shortname].groupContribution >= data[entity.OwnerID].placedGroups[limit.stub.entName]) {
                                    data[entity.OwnerID].placedGroups.Remove(limit.stub.entName);
                                } else {
                                    data[entity.OwnerID].placedGroups[limit.stub.entName] -= limit.groupMembers[shortname].groupContribution;
                                }
                            } else {
                                _plugin.PrintWarning($"DEBUG: RemoveEntity called on {iid}({shortname}) for {entity.OwnerID}({perm}) but placedGroups was missing group {limit.stub.entName}!");
                            }
                        }
                        data[entity.OwnerID].placedTotal--;
                        return;
                    }
                }
                _plugin.PrintWarning($"DEBUG: RemoveEntity called on {iid}({shortname}) for {entity.OwnerID}({perm}) but it was not stored!");
            }
            private static Coroutine lookup;
            internal static void Lookup(bool start = true, BaseEntity[] entities = null) { if (lookup != null) ServerMgr.Instance.StopCoroutine(lookup); if (start) lookup = ServerMgr.Instance.StartCoroutine(LookupEntities(entities)); }
            internal static IEnumerator LookupEntities(BaseEntity[] entities = null) {
                yield return new WaitForEndOfFrame();
                DateTime startTime = DateTime.Now;
                entities ??= UnityEngine.Object.FindObjectsOfType<BaseEntity>();
                TimeSpan lookupTime = DateTime.Now - startTime;
                yield return new WaitForEndOfFrame();
                startTime = DateTime.Now;
                int playerOwned = 0;
                for (var i = 0; i < entities.Length; i++) {
                    var entity = entities[i];
                    if (entity.IsValid() == false || entity.OwnerID.IsSteamId() == false) continue;
                    StoreEntity(entity);
                    playerOwned++;
                    if (i > 0 && i % 22500 == 0) yield return new WaitForEndOfFrame();
                }
                TimeSpan storageTime = DateTime.Now - startTime;
                _plugin.Puts($"BuildingShop: Done building cache... {entities.Length:N0} entities, {playerOwned:N0} player owned. Storage Time: {storageTime}  Lookup Time: {lookupTime}");
                Interface.Oxide.DataFileSystem.GetFile($"BuildingShop/PlayerData").WriteObject(data);
                _plugin.SaveConfig();
            }
        }
        #endregion
        #region class Purchases
        internal class Purchases {
            public int totalPurchases;
            public long currencySpent;
            public Dictionary<string, int> dat = new();

            public static Dictionary<ulong, Purchases> players = new() { { 0, new() } };
            internal static bool dirty = false;
            public static int Get(ulong userID, string shortname) {
                if (players.ContainsKey(userID) && players[userID].dat.TryGetValue(shortname, out int purchases)) return purchases;
                return 0;
            }
            public static void Inc(ulong userID, string shortname, int qty, long currencySpent) {
                if (!players.ContainsKey(userID)) {
                    players[userID] = new() { dat = { { shortname, qty } } };
                } else if (!players[userID].dat.ContainsKey(shortname)) {
                    players[userID].dat[shortname] = qty;
                } else {
                    players[userID].dat[shortname] += qty;
                }
                if (!players[0].dat.ContainsKey(shortname)) players[0].dat[shortname] = qty;
                else players[0].dat[shortname] += qty;
                players[userID].totalPurchases += qty;
                players[userID].currencySpent += currencySpent;
                players[0].totalPurchases += qty;
                players[0].currencySpent += currencySpent;
                dirty = true;
            }
            public bool Refund(ulong userID, double mult) {
                long refundValue = (long)(mult * currencySpent);
                if (refundValue > 2147483647) return false;
                if (refundValue > 0) {
                    switch (config.currency) {
                        case 0: break;
                        case 1: _plugin.ServerRewards?.Call<bool>("AddPoints", userID, (int)refundValue); break;
                        case 2: _plugin.Economics?.Call<bool>("Deposit", userID, (double)refundValue); break;
                        default: break; //player.inventory.GiveItem(ItemManager.CreateByItemID(config.currency, (int)refundValue)); break;
                    }
                }
                players[0].currencySpent -= players[userID].currencySpent;
                players[0].totalPurchases -= players[userID].totalPurchases;
                players.Remove(userID);
                dirty = true;
                if (BasePlayer.TryFindByID(userID, out BasePlayer player))
                    if (refundValue > 0) player.ChatMessage(_plugin.LangMessage(player.UserIDString, "cmdRefunded", new string[] { Purchases.DispCurrency((uint)refundValue, "#40A040"), mult.ToString("P") }));
                    else player.ChatMessage(_plugin.LangMessage(player.UserIDString, "cmdWiped", null));
                return true;
            }
            // this doesn't belong.
            public static string DispCurrency(uint val, string color) {
                string retVal = $"{config.currencyPrefix}{val:N0} {config.currencyName}";
                if (color.IsNullOrEmpty()) return retVal;
                return $"<color={color}>{retVal}</color>";
            }
            public static bool ChargePlayer(ulong userID, int amount) {
                if (amount == 0) return true;
                switch (config.currency) {
                    case 0: return false;
                    case 1: return (_plugin.ServerRewards?.Call<int>("CheckPoints", userID) ?? 0) >= amount && (_plugin.ServerRewards?.Call<bool>("TakePoints", userID, amount) ?? false);
                    case 2: return _plugin.Economics?.Call<bool>("Withdraw", userID, (double)amount) ?? false;
                    default: if (!BasePlayer.TryFindByID(userID, out BasePlayer player) || amount > player.inventory.GetAmount(config.currency)) return false; player.inventory.Take(null, config.currency, amount); return true;
                }
            }
            public static int MugPlayer(ulong userID) {
                switch (config.currency) {
                    case 0: return 0;
                    case 1: return _plugin.ServerRewards?.Call<int>("CheckPoints", userID) ?? 0;
                    case 2: return (int)(_plugin.Economics?.Call<double>("Balance", userID) ?? 0d);
                    default: return BasePlayer.FindByID(userID)?.inventory.GetAmount(config.currency) ?? 0;
                }
            }
            public static void Load() {
                try {
                    players = Interface.Oxide.DataFileSystem.GetFile("BuildingShop/Purchases")?.ReadObject<Dictionary<ulong, Purchases>>();
                } catch (Exception ex) {
                    _plugin.PrintError($"Failed to load Purchases.json: {ex}");
                }
                if (players == null || !players.ContainsKey(0)) {
                    players = new() { { 0, new() } };
                    dirty = true;
                    _plugin.PrintError("Starting with blank Purchases Data!");
                } else {
                    _plugin.Puts($"Loaded {players.Count - 1} player purchase record(s) from Purchases.json");
                }
            }
            public static void Wipe() {
                Save();
                string fn = Path.Combine(Interface.Oxide.DataFileSystem.Directory, "BuildingShop/Purchases");
                try {
                    if (File.Exists(fn + ".bak")) File.Delete(fn + ".bak");
                    File.Move(fn + ".json", fn + ".bak");
                } catch (Exception ex) {
                    _plugin.PrintError($"Failed to Backup Purchases.json: {ex}");
                }
                players.Clear();
                players[0] = new();
                dirty = true;
                _plugin.Puts($"Moved Purchases.json to Purchases.bak, and wiped Purchases.");
            }
            public static bool UnWipe() {
                string fn = Path.Combine(Interface.Oxide.DataFileSystem.Directory, "BuildingShop/Purchases");
                try {
                    if (File.Exists(fn + ".bak")) {
                        if (File.Exists(fn + ".json")) File.Delete(fn + ".json");
                        File.Move(fn + ".bak", fn + ".json");
                        Load();
                        _plugin.Puts($"Moved Purchases.bak to Purchases.json, and reloaded Purchases.");
                        return true;
                    }
                } catch (Exception ex) {
                    _plugin.PrintError($"Failed to Restore Purchases.json: {ex}");
                }
                return false;
            }
            public static void Save() {
                if (dirty) {
                    Interface.Oxide.DataFileSystem.GetFile("BuildingShop/Purchases").WriteObject(players);
                    _plugin.Puts($"Saved {players.Count} player purchase record(s) to Purchases.json");
                    dirty = false;
                }
            }
        }
        #endregion

        #region class PlacedUI
        internal class PlacedUI {
            private static readonly Dictionary<ulong, Timer> Players = new();
            internal static void CloseUI(BasePlayer player) {
                CuiHelper.DestroyUi(player, "bshop_placedui");
                if (Players.ContainsKey(player.userID)) {
                    if (Players[player.userID]?.Destroyed == false) Players[player.userID].DestroyToPool();
                    Players.Remove(player.userID);
                }
            }
            internal static void CloseAll() {
                foreach (var user in Players) if (BasePlayer.TryFindByID(user.Key, out BasePlayer player)) CuiHelper.DestroyUi(player, "bshop_placedui");
                Players.Clear();
            }
            internal static void Show(BasePlayer player, string entName) {
                CloseUI(player);
                ConfigData.PermissionEntry permE = config.permissions[PlayerData.data[player.userID].Perm];
                LimitEntry limit = permE.entLimits[entName];
                string purchaseItem = entName;
                LimitEntry.Stub stub = limit.stub;
                if (!limit.groupMembers.IsNullOrEmpty()) {
                    purchaseItem = limit.stub.entName;
                    stub = limit.groupMembers[entName];
                }
                int purchased = Purchases.Get(player.userID, purchaseItem);
                int placed = PlayerData.GetPlaced(player.userID, purchaseItem, limit.groupMembers != null);
                int maxPlaced = limit.free + purchased;
                bool isFail = placed + stub.groupContribution > maxPlaced;
                var elements = new CuiElementContainer { };
                var parent = elements.Add(new CuiPanel { Image = { Color = $"0.25 0.31 0.31 {(isFail ? 0.97f : 0.85f):G3}" }, RectTransform = { AnchorMin = "0.33 0.2", AnchorMax = "0.66 0.3" }, FadeOut = (isFail ? 0f : 0.5f), CursorEnabled = false, KeyboardEnabled = false }, "Hud", "bshop_placedui");
                if (isFail) {
                    int canBuy = limit.max - purchased;
                    if (limit.max == -1) canBuy = config.infinity;
                    uint playerCurrency = (uint)Purchases.MugPlayer(player.userID);
                    int canAfford = limit.Purchaseable(playerCurrency, purchased);
                    if (canBuy > canAfford) { canBuy = canAfford; }
                    int needed = (stub.groupContribution + placed) - maxPlaced;
                    if (needed > canBuy || playerCurrency < limit.GetTotalCost(purchased, needed)) SendEffect(player, config.sounds["overLimitSorry"]);
                    else SendEffect(player, config.sounds["overLimit"]);
                    var uiState = elements.Add(new CuiPanel { Image = { Color = "0.25 0.31 0.31 0.98" }, RectTransform = { AnchorMin = "0 1.1", AnchorMax = "1 3" }, CursorEnabled = true, KeyboardEnabled = true, }, parent);
                    string headerName = elements.Add(new CuiPanel { Image = { Color = config.UIColors["headerBackground"] }, RectTransform = { AnchorMin = "0 0.9", AnchorMax = "1 1" } }, uiState);
                    elements.Add(new CuiElement { Parent = uiState, Components = { new CuiRawImageComponent { Sprite = "assets/icons/lock.png" }, new CuiRectTransformComponent { AnchorMin = "0 0.6", AnchorMax = "0.15 0.9" } } });
                    elements.Add(new CuiLabel { Text = { Text = $"Sorry. You may not place that item.", Color = "0.75 0.8 0.8 0.8", FontSize = 14, Align = TextAnchor.UpperLeft, VerticalOverflow = VerticalWrapMode.Overflow }, RectTransform = { AnchorMin = "0.02 0.25", AnchorMax = "0.98 0.8" } }, uiState);
                    elements.Add(new CuiButton { Button = { Close = parent, Color = "0.51 0.31 0.31 1" }, RectTransform = { AnchorMin = "0.02 0.04", AnchorMax = "0.24 0.15" }, Text = { Text = "Cancel", FontSize = 10, Align = TextAnchor.MiddleCenter } }, uiState);
                    if (canBuy == needed) {
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {needed}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.25 0.04", AnchorMax = "0.98 0.15" }, Text = { Text = $"Buy {needed}    {Purchases.DispCurrency(limit.GetTotalCost(purchased, needed), "#40A040")}", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, uiState);
                    } else if (canBuy > needed + (stub.groupContribution * 2)) {
                        int midBuyNum = needed + (stub.groupContribution * 2);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {needed}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.25 0.04", AnchorMax = "0.488 0.25" }, Text = { Text = $"Buy {needed}\n<size=8>{Purchases.DispCurrency(limit.GetTotalCost(purchased, needed), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, uiState);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {midBuyNum}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.498 0.04", AnchorMax = "0.731 0.25" }, Text = { Text = $"Buy {midBuyNum}\n<size=8>{Purchases.DispCurrency(limit.GetTotalCost(purchased, midBuyNum), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, uiState);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {canBuy}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.741 0.04", AnchorMax = "0.98 0.25" }, Text = { Text = $"Buy {canBuy}\n<size=8>{Purchases.DispCurrency(limit.GetTotalCost(purchased, canBuy), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, uiState);
                    } else if (canBuy > needed) {
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {needed}", Color = "0.31 0.31 0.31 1" }, RectTransform = { AnchorMin = "0.25 0.04", AnchorMax = "0.61 0.15" }, Text = { Text = $"Buy {needed}     <size=10>{Purchases.DispCurrency(limit.GetTotalCost(purchased, needed), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, uiState);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {purchaseItem} {canBuy}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.62 0.04", AnchorMax = "0.98 0.15" }, Text = { Text = $"Buy {canBuy}     <size=10>{Purchases.DispCurrency(limit.GetTotalCost(purchased, canBuy), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, uiState);
                    }
                } else {
                    Players[player.userID] = _plugin.timer.Once(4f, () => {
                        CuiHelper.DestroyUi(player, parent);
                        Players.Remove(player.userID);
                    });
                    placed += stub.groupContribution;
                }
                elements.Add(new CuiLabel { Text = { Text = stub.displayNameColor, Color = "1 1 1 1", FontSize = 16, Align = TextAnchor.MiddleLeft, VerticalOverflow = VerticalWrapMode.Overflow }, RectTransform = { AnchorMin = "0.13 0.3", AnchorMax = "1 0.9" } }, parent);
                if (stub.icon != null) elements.Add(new CuiElement { Parent = parent, Components = { stub.icon, new CuiRectTransformComponent { AnchorMin = "0.01 0.2", AnchorMax = "0.13 0.8" } } });
                if (stub.group != null) {
                    elements.Add(new CuiLabel { Text = { Text = limit.stub.displayNameColor, Color = "0.625 0.4375 0.625 0.85", FontSize = 8, Align = TextAnchor.MiddleLeft }, RectTransform = { AnchorMin = "0.1575 0", AnchorMax = "1 0.4" } }, parent);
                    elements.Add(new CuiElement { Parent = parent, Components = { limit.stub.icon, new CuiRectTransformComponent { AnchorMin = "0.1 0", AnchorMax = "0.15 0.4" } } });
                }
                string graphThingTop = elements.Add(new CuiPanel { Image = { Color = "0 0 0 0.6" }, RectTransform = { AnchorMin = "0 0.85", AnchorMax = "1 1.1" } }, parent);
                float thisPct = (maxPlaced == 0 ? 1f : (float)(placed > maxPlaced ? maxPlaced : placed) / maxPlaced);
                elements.Add(new CuiPanel { Image = { Color = BlendColor_cui(0xE640B010, 0xE6B04010, thisPct) }, RectTransform = { AnchorMin = "0 0.1", AnchorMax = $"{thisPct:G3} 0.9" } }, graphThingTop);
                elements.Add(new CuiLabel { Text = { Text = $"Placed {(isFail ? "" : $"(+<color=#A07070>{stub.groupContribution}</color>) ")}[<color=#70A070>{placed}</color>/<color=#A07070>{maxPlaced}</color>]", Color = "0.75 0.85 0.85 0.85", FontSize = 10, Align = TextAnchor.MiddleRight }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, graphThingTop);
                elements.Add(new CuiLabel { Text = { Text = "BuildingShop", Color = "0.75 1 1 1", FontSize = 5, Align = TextAnchor.LowerRight }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, parent);
                if (permE.globalLimit != 0) {
                    int maxGlobal = permE.globalLimit == -1 ? config.infinity * 10 : permE.globalLimit;
                    float globalPct = (float)(PlayerData.data[player.userID].placedTotal > maxGlobal ? maxGlobal : PlayerData.data[player.userID].placedTotal) / maxGlobal;
                    string graphThing = elements.Add(new CuiPanel { Image = { Color = "0 0 0 0.6" }, RectTransform = { AnchorMin = "0 -0.25", AnchorMax = "1 0" } }, parent);
                    if (PlayerData.data[player.userID].placedTotal > 0) elements.Add(new CuiPanel { Image = { Color = BlendColor_cui(0xE6108F10, 0xE68F1010, globalPct) }, RectTransform = { AnchorMin = "0 0", AnchorMax = $"{(float)(PlayerData.data[player.userID].placedTotal > permE.globalLimit ? permE.globalLimit : PlayerData.data[player.userID].placedTotal) / permE.globalLimit:G3} 1" } }, graphThing);
                    string labelBox = elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.9" }, RectTransform = { AnchorMin = "0.2 0.2", AnchorMax = "0.8 0.8" } }, graphThing);
                    elements.Add(new CuiLabel { Text = { Text = $"Global Limit [<color=#70A070>{PlayerData.data[player.userID].placedTotal:N0}</color>/<color=#A07070>{permE.globalLimit:#;∞}</color>]", Color = "0.55 0.75 0.75 0.85", FontSize = 10, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, labelBox);
                }
                CuiHelper.AddUi(player, elements);
            }
        }
        #endregion
        #region class BuildingShopUI
        internal class BuildingShopUI {
            public enum TabIndex : uint { None, Limits, Admin }
            public TabIndex tabIndex = TabIndex.None;
            public int firstIndex, prevIndex, lastIndex;
            public uint playerCurrency;
            public string perm, uiName, contentName, permName, headerName, balanceName, titleName, paginationName, dropdownName, selPerm;
            internal ConfigData.PermissionEntry permE = ConfigData.PermissionEntry.blank;
            public List<string> contentRow = new() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
            public bool isAdmin = false;
            internal static readonly Dictionary<ulong, BuildingShopUI> Players = new();
            internal static void CloseUI(BasePlayer player) {
                if (Players.ContainsKey(player.userID)) {
                    CuiHelper.DestroyUi(player, Players[player.userID].uiName);
                    Players.Remove(player.userID);
                }
            }
            internal static void CloseAll() {
                foreach (KeyValuePair<ulong, BuildingShopUI> user in Players) if (BasePlayer.TryFindByID(user.Key, out BasePlayer player)) CuiHelper.DestroyUi(player, Players[user.Key].uiName);
                Players.Clear();
            }
            public static void ReloadTab(BasePlayer player, int idx = -1) {
                if (Players.ContainsKey(player.userID)) {
                    if (idx > -1) Players[player.userID].firstIndex = idx;
                    Players[player.userID].Tab(Players[player.userID].tabIndex, player, true);
                }
            }
            internal void Paginate(CuiElementContainer elements, string commandPrefix, int perPage) {
                paginationName = elements.Add(new CuiPanel { Image = { Color = config.UIColors["headerBackground"] }, RectTransform = { AnchorMin = "0.32 0.01", AnchorMax = "0.68 0.25" } }, titleName);
                elements.Add(new CuiLabel { Text = { Text = $"{firstIndex + 1} to {(firstIndex + perPage > lastIndex ? lastIndex + 1 : firstIndex + perPage)} of {lastIndex + 1}", Color = "0.75 0.8 0.8 0.8", FontSize = 10, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, paginationName);
                if (firstIndex > perPage) {
                    elements.Add(new CuiButton { Button = { Command = $"{commandPrefix} 0", Color = "0.31 0.31 0.31 0.85" }, RectTransform = { AnchorMin = "0.02 0.15", AnchorMax = "0.175 0.85" }, Text = { Text = "«", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, paginationName);
                    elements.Add(new CuiButton { Button = { Command = $"{commandPrefix} {firstIndex - perPage}", Color = "0.31 0.31 0.31 0.85" }, RectTransform = { AnchorMin = "0.2 0.15", AnchorMax = "0.3 0.85" }, Text = { Text = "<", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, paginationName);
                } else if (firstIndex > 0) {
                    elements.Add(new CuiButton { Button = { Command = $"{commandPrefix} 0", Color = "0.31 0.31 0.31 0.85" }, RectTransform = { AnchorMin = "0.02 0.15", AnchorMax = "0.3 0.85" }, Text = { Text = "«", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, paginationName);
                } else {
                    elements.Add(new CuiPanel { Image = { Color = "0.21 0.21 0.21 0.85" }, RectTransform = { AnchorMin = "0.02 0.15", AnchorMax = "0.3 0.85" } }, paginationName);
                }
                if (lastIndex - firstIndex >= perPage << 1) {
                    elements.Add(new CuiButton { Button = { Command = $"{commandPrefix} {firstIndex + perPage}", Color = "0.31 0.31 0.31 0.85" }, RectTransform = { AnchorMin = "0.7 0.15", AnchorMax = "0.8 0.85" }, Text = { Text = ">", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, paginationName);
                    elements.Add(new CuiButton { Button = { Command = $"{commandPrefix} {lastIndex - (lastIndex % perPage)}", Color = "0.31 0.31 0.31 0.85" }, RectTransform = { AnchorMin = "0.825 0.15", AnchorMax = "0.98 0.85" }, Text = { Text = "»", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, paginationName);
                } else if (lastIndex - firstIndex >= perPage) {
                    elements.Add(new CuiButton { Button = { Command = $"{commandPrefix} {firstIndex + perPage}", Color = "0.31 0.31 0.31 0.85" }, RectTransform = { AnchorMin = "0.7 0.15", AnchorMax = "0.98 0.85" }, Text = { Text = "»", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, paginationName);
                } else {
                    elements.Add(new CuiPanel { Image = { Color = "0.21 0.21 0.21 0.85" }, RectTransform = { AnchorMin = "0.7 0.15", AnchorMax = "0.98 0.85" } }, paginationName);
                }
            }
            internal static void GenInput(CuiElementContainer elements, string inputDisplayName, int charsLimit, string text, string command, string parent, int fontSize = 10) {
                if (!inputDisplayName.IsNullOrEmpty()) elements.Add(new CuiLabel { Text = { Text = inputDisplayName, Color = "0.75 0.75 0.75 1", FontSize = fontSize - 2, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.005 0.005", AnchorMax = "0.995 0.995" } }, parent);
                elements.Add(new CuiElement { Parent = parent, Components = { new CuiInputFieldComponent { Color = "1 1 1 1", FontSize = fontSize, Align = TextAnchor.LowerCenter, CharsLimit = charsLimit, IsPassword = false, Text = text, Command = command }, new CuiRectTransformComponent { AnchorMin = "0 0.05", AnchorMax = "1 0.95" }, }, });
            }
            internal static void GenDropDownButton(CuiElementContainer elements, string inputDisplayName, string selectedText, string command, string parent, string color, int fontSize = 10) {
                if (!inputDisplayName.IsNullOrEmpty()) elements.Add(new CuiLabel { Text = { Text = inputDisplayName, Color = "0.75 0.75 0.75 1", FontSize = fontSize - 2, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.005 0.005", AnchorMax = "0.995 0.995" } }, parent);
                elements.Add(new CuiButton { Button = { Command = command + parent, Color = color }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" }, Text = { Text = selectedText, Color = "0.75 0.75 0.75 0.8", FontSize = fontSize, Align = inputDisplayName.IsNullOrEmpty() ? TextAnchor.MiddleCenter : TextAnchor.LowerCenter } }, parent);
            }
            internal static void GenBarGraph(CuiElementContainer elements, string parent, int curLevel, int maxLevel, uint minColor, uint maxColor, string label, TextAnchor align) {
                if (maxLevel == -1) maxLevel = config.infinity;
                float thisPct = 1f;
                if (maxLevel > 0) thisPct = (float)(curLevel > maxLevel ? maxLevel : curLevel) / maxLevel;
                if (thisPct > 0f) {
                    elements.Add(new CuiPanel { Image = { Color = BlendColor_cui(minColor, maxColor, thisPct) }, RectTransform = { AnchorMin = "0 0", AnchorMax = $"{thisPct:G3} 1" } }, parent);
                }
                elements.Add(new CuiLabel { Text = { Text = label.Replace("{0}", BlendColor_tag(minColor, maxColor, thisPct)), Color = "0.7 0.7 0.7 0.9", FontSize = 8, Align = align }, RectTransform = { AnchorMin = "0 -2", AnchorMax = "1 3" } }, parent);
                elements.Add(new CuiLabel { Text = { Text = $"{thisPct:P}", Color = "0.6 0.6 0.6 0.9", FontSize = 6, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, parent);
            }
            public void Tab(TabIndex tab, BasePlayer player, bool reload = false) {
                if (tabIndex == tab && firstIndex == prevIndex && !reload) { return; } // ignore tab changes to same tab, except for page change
                prevIndex = firstIndex;
                TabIndex prevTab = tabIndex;
                tabIndex = tab;
                if (!dropdownName.IsNullOrEmpty()) { CuiHelper.DestroyUi(player, dropdownName); dropdownName = null; }
                if (!titleName.IsNullOrEmpty()) CuiHelper.DestroyUi(player, titleName);
                else if (!paginationName.IsNullOrEmpty()) CuiHelper.DestroyUi(player, paginationName);
                if (!contentName.IsNullOrEmpty()) { CuiHelper.DestroyUi(player, contentName); contentRow = new() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" }; }
                var elements = new CuiElementContainer { };
                contentName = elements.Add(new CuiPanel { Image = { Color = config.UIColors["contentBackground"] }, RectTransform = { AnchorMin = "0.01 0.007", AnchorMax = "0.99 0.785" } }, uiName);
                titleName = elements.Add(new CuiPanel { Image = { Color = config.UIColors["titleBackground"] }, RectTransform = { AnchorMin = "0 0.8", AnchorMax = "1 0.96" } }, uiName);
                elements.Add(new CuiElement { Parent = titleName, Components = { new CuiRawImageComponent { Url = "https://rustlabs.com/img/items180/building.planner.png" }, new CuiRectTransformComponent { AnchorMin = "0.01 0.33", AnchorMax = "0.09 1" }, }, });
                switch (tab) {
                    case TabIndex.None:
                        break;
                    case TabIndex.Limits:
                        elements.Add(new CuiLabel { Text = { Text = "Building Limits", Color = "0.75 1 1 1", FontSize = 18, Align = TextAnchor.MiddleLeft }, RectTransform = { AnchorMin = "0.1 0.7", AnchorMax = "0.7 1" } }, titleName);
                        elements.Add(new CuiLabel { Text = { Text = $"Here, you can extend your building limits, with {config.currencyName}." + (config.wipePurchases ? " <size=10>Purchases made here, will be wiped, along with the map.</size>" : ""), Color = "0.75 0.8 0.8 0.8", FontSize = 12, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.1 0.4", AnchorMax = "1 0.7" } }, titleName);
                        if (!perm.IsNullOrEmpty() && limitsPermissions.Contains(perm)) permE = config.permissions[perm];
                        if (permE.globalLimit == 0) {
                            elements.Add(new CuiLabel { Text = { Text = $"Global Limit: 0", Color = "0.95 0.8 0.8 0.8", FontSize = 12, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = "0.7 0.01", AnchorMax = "0.99 0.25" } }, titleName);
                        } else {
                            int maxPlaced = permE.globalLimit == -1 ? config.infinity * 10 : permE.globalLimit;
                            float thisPct = (float)(PlayerData.data[player.userID].placedTotal > maxPlaced ? maxPlaced : PlayerData.data[player.userID].placedTotal) / maxPlaced;
                            string graphThing = elements.Add(new CuiPanel { Image = { Color = "0 0 0 0.6" }, RectTransform = { AnchorMin = "0.7 0.01", AnchorMax = "0.99 0.25" } }, titleName);
                            if (PlayerData.data[player.userID].placedTotal > 0) elements.Add(new CuiPanel { Image = { Color = BlendColor_cui(0xE6108F10, 0xE68F1010, thisPct) }, RectTransform = { AnchorMin = "0 0", AnchorMax = $"{(float)(PlayerData.data[player.userID].placedTotal > permE.globalLimit ? permE.globalLimit : PlayerData.data[player.userID].placedTotal) / permE.globalLimit:G3} 1" } }, graphThing);
                            string labelBox = elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.9" }, RectTransform = { AnchorMin = "0.2 0.2", AnchorMax = "0.8 0.8" } }, graphThing);
                            elements.Add(new CuiLabel { Text = { Text = $"Global Limit [<color=#70A070>{PlayerData.data[player.userID].placedTotal:N0}</color>/<color=#A07070>{permE.globalLimit:#;∞}</color>]", Color = "0.55 0.75 0.75 0.85", FontSize = 12, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, labelBox);
                        }
                        if (permE.limits != null && permE.limits.Count > 0) {
                            lastIndex = permE.sortedLimits.Count - 1;
                            if (firstIndex > lastIndex) firstIndex = 0;
                            if (prevTab != tab) SendEffect(player, config.sounds["openUITabLimits"]);
                            if (lastIndex > -1) {
                                Paginate(elements, $"{config.command} ui limit", 10);
                                for (int j = 0; j < 10; j++) {
                                    if (j + firstIndex > lastIndex) break;
                                    BuildLimitEntry_Row(player, elements, j);
                                }
                            }
                        } else {
                            string noLimitsRow = contentRow[0] = elements.Add(new CuiPanel { Image = { Color = config.UIColors["rowBackground"] }, RectTransform = { AnchorMin = $"0 {1f - 0.045f:G3}", AnchorMax = $"1 {1f:G3}" } }, contentName);
                            elements.Add(new CuiLabel { Text = { Text = $"You do not seem to have any limits!", Color = "0.75 0.8 0.8 0.8", FontSize = 12, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, noLimitsRow);
                        }
                        break;
                    case TabIndex.Admin:
                        elements.Add(new CuiLabel { Text = { Text = "BuildingShop Admin", Color = "0.75 1 1 1", FontSize = 18, Align = TextAnchor.MiddleLeft }, RectTransform = { AnchorMin = "0.1 0.7", AnchorMax = "0.7 1" } }, titleName);
                        elements.Add(new CuiLabel { Text = { Text = $"Add/Remove/Edit permissions, Change configuration settings, Wipe Purchases, etc", Color = "0.75 0.8 0.8 0.8", FontSize = 12, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.1 0.4", AnchorMax = "1 0.7" } }, titleName);
                        if (selPerm.IsNullOrEmpty()) selPerm = (limitsPermissions == null || limitsPermissions.Count == 0 ? "None" : perm.IsNullOrEmpty() ? limitsPermissions[0] : perm);
                        Build_AdminRow_C(player, 13, elements);
                        Build_AdminRow_B(player, 12, elements);
                        Build_AdminRow_A(player, 11, elements);
                        if (!selPerm.IsNullOrEmpty() && selPerm != "None" && limitsPermissions.Contains(selPerm)) {
                            ConfigData.PermissionEntry selPermE = config.permissions[selPerm];
                            GenInput(elements, "Priority", 5, selPermE.priority.ToString("G"), $"{config.command} perm priority {selPerm}", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.165 0.95", AnchorMax = "0.20 0.99" } }, contentName), 8);
                            GenInput(elements, "Global Limit", 6, selPermE.globalLimit.ToString("G"), $"{config.command} perm globalLimit {selPerm}", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.205 0.95", AnchorMax = "0.245 0.99" } }, contentName), 8);
                            GenInput(elements, "Add Entity:", 96, string.Empty, $"{config.command} perm {selPerm} new", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.4 0.95", AnchorMax = "0.6 0.99" } }, contentName));
                            lastIndex = selPermE.sortedLimits.Count - 1;
                            if (firstIndex > lastIndex) firstIndex = 0;
                            if (lastIndex > -1) {
                                Paginate(elements, $"{config.command} ui adm", 10);
                                for (int j = 9; j > -1; j--) {
                                    if (j + firstIndex > lastIndex) continue;
                                    BuildPermissionEntry_Row(elements, j);
                                }
                            }
                        }
                        GenDropDownButton(elements, "Selected Permission:", selPerm, config.command + " ui adm perm show2 ", elements.Add(new CuiPanel { Image = { Color = "0 0 0 0" }, RectTransform = { AnchorMin = "0 0.95", AnchorMax = "0.16 0.99" } }, contentName), "0.31 0.31 0.31 0.85", 8);
                        if (prevTab != tab) SendEffect(player, config.sounds["openUITabAdmin"]);
                        break;
                }
                CuiHelper.AddUi(player, elements);
                UpdatePerm(player);
            }
            internal void Build_AdminRow_A(BasePlayer player, int idx, CuiElementContainer elements = null) {
                bool CallAddUI = false;
                if (elements == null) { elements = new(); CallAddUI = true; }
                if (!contentRow[idx].IsNullOrEmpty()) CuiHelper.DestroyUi(player, contentRow[idx]);
                float yPos = 1f - (0.05f * (1 + idx));
                contentRow[idx] = elements.Add(new CuiPanel { Image = { Color = config.UIColors["rowBackground"] }, RectTransform = { AnchorMin = $"0 {yPos - 0.045f:G3}", AnchorMax = $"1 {yPos:G3}" } }, contentName);
                GenInput(elements, "Admin Permission:", 96, config.adminPermission, $"{config.command} config adminPermission", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.005 0", AnchorMax = "0.245 1" } }, contentRow[idx]));
                GenInput(elements, "Primary Command:", 16, config.command, $"{config.command} config command", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.255 0", AnchorMax = "0.45 1" } }, contentRow[idx]));
                GenDropDownButton(elements, "Alternate Commands:", string.Join(", ", config.commands) + " ↓", config.command + " ui adm perm showDDCommands ", elements.Add(new CuiPanel { Image = { Color = "0 0 0 0" }, RectTransform = { AnchorMin = "0.55 0", AnchorMax = "0.745 1" } }, contentRow[idx]), "0 0 0 0");
                elements.Add(new CuiButton { Button = { Command = $"{config.command} config wipePurchases " + (!config.wipePurchases).ToString(), Color = "0 0 0 0" }, RectTransform = { AnchorMin = "0.755 0", AnchorMax = "0.995 1" }, Text = { Text = $"<size=16><color=#ffffff>{(config.wipePurchases ? "☑" : "☐")}</color></size>   Wipe Purchases With Map", Color = "0.75 0.75 0.75 0.8", FontSize = 10, Align = TextAnchor.MiddleCenter } }, contentRow[idx]);
                if (CallAddUI) CuiHelper.AddUi(player, elements);
            }
            internal void Build_AdminRow_B(BasePlayer player, int idx, CuiElementContainer elements = null) {
                bool CallAddUI = false;
                if (elements == null) { elements = new(); CallAddUI = true; }
                if (!contentRow[idx].IsNullOrEmpty()) CuiHelper.DestroyUi(player, contentRow[idx]);
                float yPos = 1f - (0.05f * (1 + idx));
                contentRow[idx] = elements.Add(new CuiPanel { Image = { Color = config.UIColors["rowBackground"] }, RectTransform = { AnchorMin = $"0 {yPos - 0.045f:G3}", AnchorMax = $"1 {yPos:G3}" } }, contentName);
                GenInput(elements, "Currency ID:", 16, config.currency.ToString("G"), $"{config.command} config currency", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.005 0", AnchorMax = "0.245 1" } }, contentRow[idx]));
                GenInput(elements, "Currency Name:", 32, config.currencyName, $"{config.command} config currencyName", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.255 0", AnchorMax = "0.45 1" } }, contentRow[idx]));
                GenInput(elements, "Infinite Value:", 6, config.infinity.ToString("G"), $"{config.command} config infinity", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.55 0", AnchorMax = "0.745 1" } }, contentRow[idx]));
                GenInput(elements, "Refund Multiplier:", 6, config.refundMultiplier.ToString("F3"), $"{config.command} config refundMultiplier", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.755 0", AnchorMax = "0.995 1" } }, contentRow[idx]));
                if (CallAddUI) CuiHelper.AddUi(player, elements);
            }
            internal void Build_AdminRow_C(BasePlayer player, int idx, CuiElementContainer elements = null) {
                bool CallAddUI = false;
                if (elements == null) { elements = new(); CallAddUI = true; }
                if (!contentRow[idx].IsNullOrEmpty()) CuiHelper.DestroyUi(player, contentRow[idx]);
                float yPos = 1f - (0.05f * (1 + idx));
                contentRow[idx] = elements.Add(new CuiPanel { Image = { Color = config.UIColors["rowBackground"] }, RectTransform = { AnchorMin = $"0 {yPos - 0.045f:G3}", AnchorMax = $"1 {yPos:G3}" } }, contentName);
                GenInput(elements, "Currency Prefix:", 5, config.currencyPrefix, $"{config.command} config currencyPrefix", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.005 0", AnchorMax = "0.245 1" } }, contentRow[idx]));
                elements.Add(new CuiButton { Button = { Command = $"{config.command} unwipepurchases", Color = "0.31 0.81 0.31 0.75" }, RectTransform = { AnchorMin = "0.33 0.5", AnchorMax = "0.42 1" }, Text = { Text = "Un-Wipe Purchases", Color = "0.75 0.75 0.75 0.8", FontSize = 8, Align = TextAnchor.MiddleCenter } }, contentRow[idx]);
                elements.Add(new CuiButton { Button = { Command = $"{config.command} wipepurchases", Color = "0.81 0.31 0.31 0.75" }, RectTransform = { AnchorMin = "0.33 0", AnchorMax = "0.42 0.5" }, Text = { Text = "Wipe Purchases", Color = "0.75 0.75 0.75 0.8", FontSize = 8, Align = TextAnchor.MiddleCenter } }, contentRow[idx]);
                elements.Add(new CuiButton { Button = { Command = $"{config.command} rebuildcache", Color = "0.81 0.31 0.31 0.75" }, RectTransform = { AnchorMin = "0.58 0.25", AnchorMax = "0.67 0.75" }, Text = { Text = "Rebuild Cache", Color = "0.75 0.75 0.75 0.8", FontSize = 8, Align = TextAnchor.MiddleCenter } }, contentRow[idx]);
                elements.Add(new CuiButton { Button = { Command = $"{config.command} loaddefaultconfig", Color = "0.91 0.21 0.21 0.75" }, RectTransform = { AnchorMin = "0.83 0.25", AnchorMax = "0.92 0.75" }, Text = { Text = "Load Default Config", Color = "0.75 0.75 0.75 0.8", FontSize = 8, Align = TextAnchor.MiddleCenter } }, contentRow[idx]);
                if (CallAddUI) CuiHelper.AddUi(player, elements);
            }
            internal static void Cmd(BasePlayer player, string[] args, bool isAdmin) {
                //_plugin.Puts($"BuildingShopUI.Cmd({player.UserIDString},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")},{isAdmin})");
                if (args.Length < 1) return;
                if (!BuildingShopUI.Players.ContainsKey(player.userID)) return;
                switch (args[0]) {
                    case "limit": // /bshop ui limit
                        if (args.Length >= 2 && int.TryParse(args[1], out Players[player.userID].firstIndex)) { // /bshop ui limit <firstIndex>
                            Players[player.userID].Tab(TabIndex.Limits, player);
                            return;
                        }
                        Players[player.userID].firstIndex = 0;
                        Players[player.userID].Tab(TabIndex.Limits, player);
                        return;
                    case "adm": // /bshop ui adm
                        if (!isAdmin) return;
                        if (args.Length >= 2) {
                            if (int.TryParse(args[1], out int idx)) { // /bshop ui adm <firstIndex>
                                Players[player.userID].firstIndex = idx;
                                Players[player.userID].Tab(TabIndex.Admin, player);
                                return;
                            }
                            switch (args[1]) {
                                case "perm": // /bshop ui adm perm
                                    if (args.Length < 4) return;
                                    switch (args[2]) {
                                        case "show": // /bshop ui adm perm show <anchor>
                                            List<string> ddPerms = new() { };
                                            if (!PlayerData.data[player.userID].Perm.IsNullOrEmpty()) ddPerms.Add("None");
                                            foreach (string p in limitsPermissions) if (p != PlayerData.data[player.userID].Perm) ddPerms.Add(p);
                                            Players[player.userID].OpenDropdown(player, args[3], config.command + " ui adm perm limit ", ddPerms, false, null, true);
                                            return;
                                        case "show2": // /bshop ui adm perm show2 <anchor>
                                            List<string> dd2Perms = new() { "None" };
                                            dd2Perms.AddRange(limitsPermissions);
                                            Players[player.userID].OpenDropdown(player, args[3], config.command + " perm ", dd2Perms, true, "BuildingShop.", true);
                                            return;
                                        case "showDDCommands": // /bshop ui adm perm showDDCommands <anchor>
                                            Players[player.userID].OpenDropdown(player, args[3], config.command + " config commands ", config.commands, true, "", false);
                                            return;
                                        case "showDDGM": // /bshop ui adm perm showDDGM <perm> <entName> <anchor>
                                            if (!limitsPermissions.Contains(args[3]) || !config.permissions[args[3]].sortedLimits.Contains(args[4]) || config.permissions[args[3]].limits[args[4]].groupMembers == null) return;
                                            List<string> listItems = new();
                                            foreach (var x in config.permissions[args[3]].limits[args[4]].groupMembers) listItems.Add($"{x.Key} {x.Value.groupContribution}");
                                            Players[player.userID].OpenDropdown(player, args[5], config.command + $" perm {args[3]} {args[4]} groupMember ", listItems, true, "", false);
                                            return;
                                        case "limit": // /bshop ui adm perm limit <newPerm>
                                            string newPerm = args[3];
                                            if (newPerm.Equals(PlayerData.data[player.userID].Perm, StringComparison.OrdinalIgnoreCase)) return;
                                            foreach (string perm in limitsPermissions) if (!perm.Equals(newPerm) && perm.StartsWith("BuildingShop.") && _plugin.permission.UserHasPermission(player.UserIDString, perm)) _plugin.permission.RevokeUserPermission(player.UserIDString, perm);
                                            if (newPerm.StartsWith("BuildingShop.") && !_plugin.permission.UserHasPermission(player.UserIDString, newPerm)) _plugin.permission.GrantUserPermission(player.UserIDString, newPerm, _plugin);
                                            // if using external permissions, we do not actually grant/revoke them; but "fake it" until it is refreshed. There is matching code that attempts to cache this permission for 3 hours.
                                            if (limitsPermissions.Contains(newPerm)) PlayerData.data[player.userID].Perm = newPerm;
                                            else PlayerData.data[player.userID].Perm = "";
                                            Players[player.userID].perm = PlayerData.data[player.userID].Perm;
                                            Players[player.userID].DrawHeader(player);
                                            ReloadTab(player);
                                            return;
                                        default: break;
                                    }
                                    break;
                                default: break;
                            }
                        } else {
                            Players[player.userID].firstIndex = 0;
                            Players[player.userID].Tab(TabIndex.Admin, player);
                            return;
                        }
                        break;
                    default: break;
                }
                player.ChatMessage($"Unhandled: {config.command} ui {string.Join(" ", args)}");
            }
            public static void Reload_s(BasePlayer player, LimitEntry _limit) {
                if (Players.ContainsKey(player.userID)) Players[player.userID].Reload(player, _limit);
            }
            public void Reload(BasePlayer player, LimitEntry _limit) {
                switch (tabIndex) {
                    case TabIndex.None: break;
                    case TabIndex.Limits: ReloadLimitEntry_Row(player, _limit); break;
                    case TabIndex.Admin: ReloadPermissionEntry_Row(player, _limit); break;
                }
            }
            public void ReloadLimitEntry_Row(BasePlayer player, LimitEntry _limit) {
                int idx = _limit.order - firstIndex;
                if (idx >= 0 && idx < 10) {
                    var elements = new CuiElementContainer { };
                    if (!contentRow[idx << 1].IsNullOrEmpty()) { CuiHelper.DestroyUi(player, contentRow[idx << 1]); }
                    BuildLimitEntry_Row(player, elements, idx);
                    CuiHelper.AddUi(player, elements);
                }
            }
            public void BuildLimitEntry_Row(BasePlayer player, CuiElementContainer elements, int idx) {
                string entName = config.permissions[perm].sortedLimits[idx + firstIndex];
                LimitEntry limit = config.permissions[perm].entLimits[entName];
                float yPos = (1f - (0.1f * (idx)));
                string parent = contentRow[idx << 1] = elements.Add(new CuiPanel { Image = { Color = config.UIColors["rowBackground"] }, RectTransform = { AnchorMin = $"0 {yPos - 0.08f:G3}", AnchorMax = $"1 {yPos:G3}" } }, contentName);
                int placed = PlayerData.GetPlaced(player.userID, entName, limit.groupMembers != null);
                if (limit.groupMembers != null) {
                    StringBuilder groupMembers = new();
                    foreach (KeyValuePair<string, LimitEntry.Stub> member in limit.groupMembers) {
                        groupMembers.Append($"{member.Value.displayNameColor}(<color=#70C070>{PlayerData.data[player.userID].GetPlacedEntities(member.Key)}</color>{(member.Value.groupContribution == 1 ? string.Empty : $"*{member.Value.groupContribution}")}), ");
                    }
                    if (groupMembers.Length > 2) groupMembers.Length -= 2;
                    else { groupMembers.Append("(empty)"); }
                    elements.Add(new CuiLabel { Text = { Text = $"{limit.stub.displayNameColor} <size=8>{groupMembers.ToString()}</size>", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleLeft, VerticalOverflow = VerticalWrapMode.Overflow }, RectTransform = { AnchorMin = "0.032 0.05", AnchorMax = "0.53 0.95" } }, parent);
                } else {
                    elements.Add(new CuiLabel { Text = { Text = $"<size=8>{limit.stub.categoryNameColor}</size>\n {limit.stub.displayNameColor}", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleLeft, VerticalOverflow = VerticalWrapMode.Overflow }, RectTransform = { AnchorMin = "0.032 0.05", AnchorMax = "0.53 0.95" } }, parent);
                }
                int purchased = Purchases.Get(player.userID, entName);
                if (limit.stub.icon != null) elements.Add(new CuiElement { Parent = parent, Components = { limit.stub.icon, new CuiRectTransformComponent { AnchorMin = "0.003 0.2", AnchorMax = "0.0275 0.8" }, }, });

                int totalLimit = limit.free + purchased;
                int needed = 1;
                if (placed >= totalLimit) {
                    elements.Add(new CuiElement { Parent = parent, Components = { new CuiRawImageComponent { Sprite = "assets/icons/lock.png" }, new CuiRectTransformComponent { AnchorMin = "-0.003 0", AnchorMax = "0.017 0.4" }, }, });
                    needed = placed - totalLimit;
                }
                if (needed < 1) needed = 1;
                int maxNum = limit.max == -1 ? config.infinity : limit.max - purchased;
                if (limit.max != 0) {
                    GenBarGraph(
                        elements, elements.Add(new CuiPanel { Image = { Color = "0 0 0 0.75" }, RectTransform = { AnchorMin = "0.6 0.9", AnchorMax = "1 1.05" } }, parent),
                        purchased, limit.max, 0xE6108F10, 0xE68F1010,
                        $"  Purchased [<color=#{(placed == 0 ? "B3B3B3" : BlendColor_tag(0xB3B3B3, 0xD06060, (double)(purchased > limit.max ? limit.max : purchased) / (limit.max == -1 ? config.infinity : limit.max)))}>{purchased}</color>/{limit.max:#;∞}]  ",
                        TextAnchor.MiddleLeft
                        );
                }
                int canAfford = limit.Purchaseable(playerCurrency, purchased);
                if (maxNum > canAfford) maxNum = canAfford;
                string parentName = elements.Add(new CuiPanel { Image = { Color = "0 0 0 0" }, RectTransform = { AnchorMin = "0.6 0", AnchorMax = $"1 1" } }, parent);
                if (maxNum >= 1) {
                    int canBuy = needed;
                    if (canBuy > maxNum) canBuy = maxNum;
                    if (maxNum <= needed) {
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {entName} {canBuy}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.03 0.25", AnchorMax = "0.97 0.75" }, Text = { Text = $"Buy {canBuy}    {Purchases.DispCurrency(limit.GetTotalCost(purchased, canBuy), "#40A040")}", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, parentName);
                    } else if (maxNum - needed > 10) {
                        int midBuyNum = needed + (limit.stub.groupContribution * 2);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {entName} {needed}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.03 0.15", AnchorMax = "0.32 0.85" }, Text = { Text = $"Buy {needed}\n<size=8>{Purchases.DispCurrency(limit.GetTotalCost(purchased, needed), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, parentName);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {entName} {midBuyNum}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.34 0.15", AnchorMax = "0.65 0.85" }, Text = { Text = $"Buy {midBuyNum}\n<size=8>{Purchases.DispCurrency(limit.GetTotalCost(purchased, midBuyNum), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, parentName);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {entName} {maxNum}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.67 0.15", AnchorMax = "0.97 0.85" }, Text = { Text = $"Buy {maxNum}\n<size=8>{Purchases.DispCurrency(limit.GetTotalCost(purchased, maxNum), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 10, Align = TextAnchor.MiddleCenter } }, parentName);
                    } else {
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {entName} {needed}", Color = "0.31 0.31 0.31 1" }, RectTransform = { AnchorMin = "0.03 0.25", AnchorMax = "0.49 0.75" }, Text = { Text = $"Buy {needed}     <size=10>{Purchases.DispCurrency(limit.GetTotalCost(purchased, needed), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, parentName);
                        elements.Add(new CuiButton { Button = { Command = $"{config.command} buy {entName} {maxNum}", Color = "0.31 0.31 0.31 0.9" }, RectTransform = { AnchorMin = "0.51 0.25", AnchorMax = "0.97 0.75" }, Text = { Text = $"Buy {maxNum}     <size=10>{Purchases.DispCurrency(limit.GetTotalCost(purchased, maxNum), "#40A040")}</size>", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, parentName);
                    }
                } else if (limit.max == -1 || limit.max > purchased) {
                    elements.Add(new CuiLabel { Text = { Text = $"Cost: {Purchases.DispCurrency(limit.GetTotalCost(purchased, needed), "#A03030")}", Color = "0.8 0.8 0.8 1", FontSize = 10, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = "0.03 0.25", AnchorMax = "0.97 0.75" } }, parentName);
                } else if (limit.max == 0) {
                    elements.Add(new CuiLabel { Text = { Text = $"Not Available in Shop", Color = "0.8 0.8 0.8 1", FontSize = 8, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = "0.03 0.25", AnchorMax = "0.97 0.75" } }, parentName);

                }
                GenBarGraph(
                    elements, elements.Add(new CuiPanel { Image = { Color = "0 0 0 0.6" }, RectTransform = { AnchorMin = "0 0.9", AnchorMax = "0.6 1.05" } }, parent),
                    placed, totalLimit, 0xE6108F10, 0xE68F1010,
                    $" [<color=#{(placed == 0 ? "B3B3B3" : BlendColor_tag(0xB3B3B3, 0xD06060, (double)(totalLimit < 1 ? 1d : placed > totalLimit ? totalLimit : placed) / totalLimit))}>{placed}</color>/{totalLimit}] Placed ",
                    TextAnchor.MiddleLeft
                    );
            }
            public void ReloadPermissionEntry_Row(BasePlayer player, LimitEntry _limit) {
                int idx = _limit.order - firstIndex;
                if (idx >= 0 && idx < 10) {
                    var elements = new CuiElementContainer { };
                    if (!contentRow[idx + 1].IsNullOrEmpty()) { CuiHelper.DestroyUi(player, contentRow[idx + 1]); }
                    BuildPermissionEntry_Row(elements, idx);
                    CuiHelper.AddUi(player, elements);
                }
            }
            public void BuildPermissionEntry_Row(CuiElementContainer elements, int idx) {
                string entName = config.permissions[selPerm].sortedLimits[idx + firstIndex];
                LimitEntry limit = config.permissions[selPerm].limits[entName];
                float yPos = 1f - (0.05f * (1 + idx));
                string parent = contentRow[idx + 1] = elements.Add(new CuiPanel { Image = { Color = config.UIColors["rowBackground"] }, RectTransform = { AnchorMin = $"0 {yPos - 0.045f:G3}", AnchorMax = $"1 {yPos:G3}" } }, contentName);
                elements.Add(new CuiButton { Button = { Command = $"{config.command} perm {selPerm} {entName} delete", Color = "1 0 0 0.75" }, RectTransform = { AnchorMin = "0.005 0.33", AnchorMax = "0.015 0.66" }, Text = { Text = "✕", FontSize = 6, Align = TextAnchor.MiddleCenter } }, parent);
                elements.Add(new CuiLabel { Text = { Text = limit.stub.categoryNameColor, Color = "0.75 0.85 0.85 0.85", FontSize = 7, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.02 0", AnchorMax = "0.25 1" } }, parent);
                elements.Add(new CuiLabel { Text = { Text = " " + limit.stub.displayNameColor, Color = "0.75 0.85 0.85 0.85", FontSize = 12, Align = TextAnchor.LowerLeft }, RectTransform = { AnchorMin = "0.02 0", AnchorMax = "0.2 1" } }, parent);
                if (!limit.groupMembers.IsNullOrEmpty()) {
                    StringBuilder _sb = new();
                    foreach (var x in limit.groupMembers) _sb.Append(x.Value.displayName + ", ");
                    string gm = "";
                    if (_sb.Length > 2) {
                        _sb.Length -= 2;
                        gm = _sb.ToString();
                    }
                    GenDropDownButton(elements, "Group Members:", gm + " ↓", $"{config.command} ui adm perm showDDGM {selPerm} {entName} ", elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.2 0.05", AnchorMax = "0.63 0.95" } }, parent), "0 0 0 0", 8);
                }
                string freeinputName = elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.64 0.05", AnchorMax = "0.72 0.95" } }, parent);
                elements.Add(new CuiLabel { Text = { Text = "Free", Color = "0.6 0.6 0.6 1", FontSize = 6, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.005 0.005", AnchorMax = "0.995 0.995" } }, freeinputName);
                elements.Add(new CuiElement { Parent = freeinputName, Components = { new CuiInputFieldComponent { Color = "1 1 1 1", FontSize = 8, Align = TextAnchor.LowerCenter, CharsLimit = 32, IsPassword = false, Text = $"{limit.free}", Command = $"{config.command} perm {selPerm} {entName} free" }, new CuiRectTransformComponent { AnchorMin = "0.1 0", AnchorMax = "0.9 1" }, }, });
                string maxinputName = elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.73 0.05", AnchorMax = "0.81 0.95" } }, parent);
                elements.Add(new CuiLabel { Text = { Text = "Max Purchases", Color = "0.6 0.6 0.6 1", FontSize = 6, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.005 0.005", AnchorMax = "0.995 0.995" } }, maxinputName);
                elements.Add(new CuiElement { Parent = maxinputName, Components = { new CuiInputFieldComponent { Color = "1 1 1 1", FontSize = 8, Align = TextAnchor.LowerCenter, CharsLimit = 32, IsPassword = false, Text = $"{limit.max}", Command = $"{config.command} perm {selPerm} {entName} max" }, new CuiRectTransformComponent { AnchorMin = "0.1 0", AnchorMax = "0.9 1" }, }, });
                if (limit.max != 0) {
                    string basePriceinputName = elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.82 0.05", AnchorMax = "0.90 0.95" } }, parent);
                    elements.Add(new CuiLabel { Text = { Text = "Base Price", Color = "0.6 0.6 0.6 1", FontSize = 6, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.005 0.005", AnchorMax = "0.995 0.995" } }, basePriceinputName);
                    elements.Add(new CuiElement { Parent = basePriceinputName, Components = { new CuiInputFieldComponent { Color = "1 1 1 1", FontSize = 8, Align = TextAnchor.LowerCenter, CharsLimit = 32, IsPassword = false, Text = $"{limit.basePrice:G6}", Command = $"{config.command} perm {selPerm} {entName} price" }, new CuiRectTransformComponent { AnchorMin = "0.1 0", AnchorMax = "0.9 1" }, }, });
                    string multiplierinputName = elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = "0.91 0.05", AnchorMax = "0.99 0.95" } }, parent);
                    elements.Add(new CuiLabel { Text = { Text = "Multiplier", Color = "0.6 0.6 0.6 1", FontSize = 6, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.005 0.005", AnchorMax = "0.995 0.995" } }, multiplierinputName);
                    elements.Add(new CuiElement { Parent = multiplierinputName, Components = { new CuiInputFieldComponent { Color = "1 1 1 1", FontSize = 8, Align = TextAnchor.LowerCenter, CharsLimit = 32, IsPassword = false, Text = $"{limit.multiplier:G6}", Command = $"{config.command} perm {selPerm} {entName} multiplier" }, new CuiRectTransformComponent { AnchorMin = "0.1 0", AnchorMax = "0.9 1" }, }, });
                }
            }
            internal void OpenDropdown(BasePlayer player, string mirrorParentName, string commandPrefix, List<string> listItems, bool withDelete = false, string withNew = null, bool asButton = true) {
                if (!dropdownName.IsNullOrEmpty()) { CuiHelper.DestroyUi(player, dropdownName); dropdownName = null; return; }
                var elements = new CuiElementContainer { };
                int j = 0;
                float rs = 1f / (listItems.Count + (withNew == null ? 0 : 1));
                float f = 1f + rs;
                dropdownName = elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 1" }, RectTransform = { AnchorMin = $"0 -{listItems.Count + (withNew == null ? 0 : 1)}", AnchorMax = "1 0" } }, mirrorParentName);
                f -= rs;
                if (withNew != null) {
                    string newinputName = elements.Add(new CuiPanel { Image = { Color = "0.15 0.15 0.15 0.5" }, RectTransform = { AnchorMin = $"0 {f - rs:G3}", AnchorMax = $"1 {f:G3}" } }, dropdownName);
                    elements.Add(new CuiLabel { Text = { Text = "New:", Color = "0.6 0.6 0.6 1", FontSize = 6, Align = TextAnchor.UpperLeft }, RectTransform = { AnchorMin = "0.005 0.005", AnchorMax = "0.995 0.995" } }, newinputName);
                    elements.Add(new CuiElement { Parent = newinputName, Components = { new CuiInputFieldComponent { Color = "1 1 1 1", FontSize = 8, Align = TextAnchor.MiddleCenter, CharsLimit = 128, IsPassword = false, Text = withNew, Command = commandPrefix + "new" }, new CuiRectTransformComponent { AnchorMin = "0 0", AnchorMax = "1 1" }, }, });
                    f -= rs;
                }
                foreach (string i in listItems) {
                    if (withDelete && !i.Equals("None")) {
                        elements.Add(new CuiButton { Button = { Command = commandPrefix + $"delete {i}", Color = "0.71 0.31 0.31 0.65" }, RectTransform = { AnchorMin = $"0 {f - rs:G3}", AnchorMax = $"0.1 {f:G3}" }, Text = { Text = "✕", Color = "1 1 1 1", FontSize = 8, Align = TextAnchor.MiddleCenter } }, dropdownName);
                    }
                    if (asButton) elements.Add(new CuiButton { Button = { Command = commandPrefix + i, Color = "0.31 0.31 0.31 0.85" }, RectTransform = { AnchorMin = $"{(withDelete && !i.Equals("None") ? "0.11" : "0")} {f - rs:G3}", AnchorMax = $"1 {f:G3}" }, Text = { Text = i, Color = "0.75 0.75 0.75 0.8", FontSize = 8, Align = TextAnchor.MiddleCenter } }, dropdownName);
                    else elements.Add(new CuiLabel { Text = { Text = i, Color = "0.75 0.75 0.75 0.8", FontSize = 8, Align = TextAnchor.MiddleCenter }, RectTransform = { AnchorMin = $"{(withDelete && !i.Equals("None") ? "0.11" : "0")} {f - rs:G3}", AnchorMax = $"1 {f:G3}" } }, dropdownName);

                    j++; f -= rs;
                }
                CuiHelper.AddUi(player, elements);
            }
            internal void DrawHeader(BasePlayer player) {
                var elements = new CuiElementContainer { };
                if (!headerName.IsNullOrEmpty()) CuiHelper.DestroyUi(player, headerName);
                headerName = elements.Add(new CuiPanel { Image = { Color = config.UIColors["headerBackground"] }, RectTransform = { AnchorMin = "0 0.96", AnchorMax = "1 1" } }, uiName);
                elements.Add(new CuiLabel { Text = { Text = (player.GetInfoBool("global.streamermode", defaultVal: false) ? "BuildingShop" : ConVar.Server.hostname), Color = "0.75 1 1 1", FontSize = 14, Align = TextAnchor.MiddleLeft }, RectTransform = { AnchorMin = "0.02 0", AnchorMax = "0.9 1" } }, headerName);
                if (player.IsAdmin) elements.Add(new CuiButton { Button = { Command = config.command + " admin", Color = "0.71 0.55 0.55 0.65" }, RectTransform = { AnchorMin = "0.65 0.7", AnchorMax = "0.70 1" }, Text = { Text = isAdmin ? "REVOKE" : "GRANT", Color = "1 1 1 1", FontSize = 6, Align = TextAnchor.MiddleCenter } }, headerName);
                if (isAdmin) {
                    elements.Add(new CuiButton { Button = { Command = config.command + " ui limit", Color = "0.31 0.51 0.51 0.85" }, RectTransform = { AnchorMin = "0.55 0.1", AnchorMax = "0.6 0.9" }, Text = { Text = "Limits", Color = "1 1 1 1", FontSize = 12, Align = TextAnchor.MiddleCenter } }, headerName);
                    elements.Add(new CuiButton { Button = { Command = config.command + " ui adm", Color = "0.41 0.25 0.25 0.85" }, RectTransform = { AnchorMin = "0.65 0.1", AnchorMax = "0.70 0.68" }, Text = { Text = "Admin", Color = "1 1 1 1", FontSize = 8, Align = TextAnchor.MiddleCenter } }, headerName);
                }
                elements.Add(new CuiButton { Button = { Close = uiName, Color = "1 0 0 0.75" }, RectTransform = { AnchorMin = "0.974 0", AnchorMax = "1 1" }, Text = { Text = "✕", FontSize = 16, Align = TextAnchor.MiddleCenter } }, headerName);
                CuiHelper.AddUi(player, elements);
                UpdateBalance(player, true);
            }
            internal void UpdateBalance(BasePlayer player, bool force) {
                if (config.currency != 0) {
                    uint newPlayerCurrency = (uint)Purchases.MugPlayer(player.userID);
                    if (force || playerCurrency != newPlayerCurrency) {
                        var elements = new CuiElementContainer { };
                        playerCurrency = newPlayerCurrency;
                        if (!balanceName.IsNullOrEmpty()) CuiHelper.DestroyUi(player, balanceName);
                        balanceName = elements.Add(new CuiPanel { Image = { Color = "0 0 0 0.15" }, RectTransform = { AnchorMin = "0.7 0", AnchorMax = "0.97 1" } }, headerName);
                        if (Purchases.players.ContainsKey(player.userID) && Purchases.players[player.userID].currencySpent > 0) {
                            elements.Add(new CuiLabel { Text = { Text = $"Balance: {Purchases.DispCurrency(playerCurrency, "#40A040")}\n<size=8>Spent: <color=#40A040>{Purchases.players[player.userID].currencySpent:N0}</color></size>", Color = "0.75 0.35 0.65 0.35", FontSize = 10, Align = TextAnchor.MiddleRight, VerticalOverflow = VerticalWrapMode.Overflow }, RectTransform = { AnchorMin = "0.33 0", AnchorMax = "1 1" } }, balanceName);
                            if ((config.currency == 1 || config.currency == 2) && (isAdmin || config.refundMultiplier > 0)) elements.Add(new CuiButton { Button = { Command = config.command + " refund", Color = "0.71 0.31 0.31 0.85" }, RectTransform = { AnchorMin = "0 0.3", AnchorMax = "0.3 0.7" }, Text = { Text = $"Refund ({(isAdmin ? 1d : config.refundMultiplier):P})", Color = "0.75 0.75 0.75 0.8", FontSize = 8, Align = TextAnchor.MiddleCenter } }, balanceName);
                        } else {
                            elements.Add(new CuiLabel { Text = { Text = $"Balance: {Purchases.DispCurrency(playerCurrency, "#40A040")}", Color = "0.75 0.35 0.65 0.35", FontSize = 10, Align = TextAnchor.MiddleRight }, RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1" } }, balanceName);
                        }
                        CuiHelper.AddUi(player, elements);
                    }
                }
            }
            internal void UpdatePerm(BasePlayer player) {
                perm = PlayerData.data[player.userID].Perm;
                var elements = new CuiElementContainer { };
                if (!permName.IsNullOrEmpty()) CuiHelper.DestroyUi(player, permName);
                if (isAdmin) {
                    permName = elements.Add(new CuiPanel { Image = { Color = "0 0 0 0" }, RectTransform = { AnchorMin = "0.9 0.65", AnchorMax = "0.99 0.85" } }, titleName);
                    GenDropDownButton(elements, "Your Limit:", (perm.IsNullOrEmpty() ? "None" : StripPrefix(perm)), config.command + " ui adm perm show ", permName, "0.31 0.31 0.31 0.85", 8);
                } else {
                    permName = elements.Add(new CuiLabel { Text = { Text = (perm.IsNullOrEmpty() ? "None" : StripPrefix(perm)), Color = "0.15 0.15 0.15 0.90", FontSize = 8, Align = TextAnchor.MiddleRight }, RectTransform = { AnchorMin = "0.9 0.75", AnchorMax = "0.99 0.9" } }, titleName);
                }
                CuiHelper.AddUi(player, elements);
            }
            internal static void Show(BasePlayer player, bool isAdmin) {
                if (Players.ContainsKey(player.userID)) {
                    CuiHelper.DestroyUi(player, Players[player.userID].uiName);
                    Players.Remove(player.userID);
                }
                var elements = new CuiElementContainer { };
                var uiState = Players[player.userID] = new BuildingShopUI() {
                    isAdmin = isAdmin,
                    perm = PlayerData.data[player.userID].Perm,
                    uiName = elements.Add(new CuiPanel { Image = { Color = config.UIColors["mainBackground"] }, RectTransform = { AnchorMin = "0.2 0.150", AnchorMax = "0.8 0.850" }, FadeOut = 0f, CursorEnabled = true, KeyboardEnabled = true, }, "Hud"),
                };
                CuiHelper.AddUi(player, elements);
                uiState.DrawHeader(player);
            }
        }
        #endregion
        internal static string StripPrefix(string input) => input.Substring(input.IndexOf(".") + 1);
        internal static int Clamp(int value, int min, int max) {
            if (value > max) return max;
            if (value < min) return min;
            return value;
        }
        internal static uint BlendColor(uint min, uint max, double pct) {
            if (pct <= 0d) return min;
            if (pct >= 1d) return max;
            int Min, Max;
            byte Val;
            uint newColor = 0;
            for (int i = 24; i >= 0; i -= 8) {
                Min = (int)((min >> i) & 0xFF);
                Max = (int)((max >> i) & 0xFF);
                Val = (byte)(Min + (int)(pct * (Max - Min)));
                newColor |= (uint)Val << i;
            }
            //_plugin.Puts($"BlendColor(min=0x{min:X8},max=0x{max:X8},pct={pct:G6}): 0x{newColor:X8}");
            return newColor;
        }
        // in = ARGB (a is discarded)
        // out = hex color code
        internal static string BlendColor_tag(uint min, uint max, double pct) {
            return $"{BlendColor(min, max, pct) & 0xFFFFFF:X6}";
        }
        // in = ARGB
        // out = Cui Color string
        internal static string BlendColor_cui(uint min, uint max, double pct) {
            uint n = BlendColor(min, max, pct);
            return $"{((n >> 16) & 0xFF) / 256d:G3} {((n >> 8) & 0xFF) / 256d:G3} {(n & 0xFF) / 256d:G3} {(n >> 24) / 256d:G3}";
        }
    }
}//2303