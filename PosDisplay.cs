﻿/*
(Completely standard MIT License:)
Copyright 2023 "Yonneh"
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------------------------------------
17 DEC 2023 - v 1.0.1
- Added config.
- Added lang.

17 DEC 2023 - v 1.0.0
- Initial Release.
- Project started.

*/
using System;
using System.Collections.Generic;
using CompanionServer.Handlers;
using Newtonsoft.Json;
using UnityEngine;
using Oxide.Game.Rust.Cui;
using Oxide.Core;

namespace Oxide.Plugins {
    [Info("PosDisplay", "Yonneh", "1.0.3")]
    [Description("(RC1) Displays your location relative to the nearest monument.")]
    public class PosDisplay : RustPlugin {
        private static PosDisplay _plugin;
        #region Housekeeping
        internal void Init() {
            _plugin = this;
            LoadDefaultMessages();
        }
        internal void OnServerSave() => PositionUI.SaveUsers();
        internal void OnServerShutdown() => PositionUI.SaveUsers();
        internal void Loaded() {
            _langMessages = lang.GetMessages(lang.GetServerLanguage(), this);
            PositionUI.BuildUI();
            PositionUI.LoadUsers();
        }
        internal void Unload() {
            RateLimiter.Destroy();
            cmd.RemoveChatCommand(regChatCommand, this);
            cmd.RemoveConsoleCommand(regChatCommand, this);
            regChatCommand = string.Empty;
            PositionUI.SaveUsers();
            PositionUI.CloseAll();
        }
        internal void OnPlayerSleepEnded(BasePlayer player) { if (PositionUI.Users.ContainsKey(player.userID)) PositionUI.Cmd_Show(player); }
        internal object OnPlayerSleep(BasePlayer player) { if (PositionUI.activePlayers.Contains(player.userID)) PositionUI.Cmd_Close(player); return null; }
        internal object OnPlayerDeath(BasePlayer player, HitInfo _) { if (PositionUI.activePlayers.Contains(player.userID)) PositionUI.Cmd_Close(player); return null; }
        internal void OnUserPermissionRevoked(string id, string permName) {
            if (config.requirePermission && config.usePermission.Equals(permName, StringComparison.OrdinalIgnoreCase)) {
                if (ulong.TryParse(id, out ulong userID) && userID.IsSteamId() && PositionUI.Users.Remove(userID)) {
                    PositionUI.usersDirty = true;
                    if (PositionUI.activePlayers.Contains(userID))
                        if (BasePlayer.TryFindByID(userID, out BasePlayer player)) PositionUI.Cmd_Close(player);
                        else PositionUI.activePlayers.Remove(userID);
                }

            }
        }
        #endregion
        #region Config
        private static ConfigData config;
        private static string regChatCommand = string.Empty;
        internal class ConfigData {
            public bool requirePermission;
            public string usePermission;
            public string chatCommand;
            public string hudParentName;
            public float refreshTimeSeconds;
            public int rateLimit;
            public int excessiveLimit;
            public float refillRateInSeconds;
        }
        protected override void LoadConfig() {
            base.LoadConfig();
            try {
                config = Config.ReadObject<ConfigData>();
            } catch (Exception ex) {
                PrintError($"Error reading Config: {ex}");
                LoadDefaultConfig();
            }
            ValidateConfig();
        }
        protected override void LoadDefaultConfig() {
            PrintError("Loading Default Config");
            config = new ConfigData {
                requirePermission = true,
                usePermission = "posdisplay.use",
                chatCommand = "posdisplay",
                hudParentName = "Overlay",
                refreshTimeSeconds = 0.25f,
                rateLimit = 5,
                excessiveLimit = 25,
                refillRateInSeconds = 5f,
            };
            SaveConfig();
        }
        protected override void SaveConfig() => Config.WriteObject(config);
        private void ValidateConfig() {
            if (config.usePermission.StartsWith("posdisplay.", StringComparison.OrdinalIgnoreCase) && !permission.PermissionExists(config.usePermission)) {
                permission.RegisterPermission(config.usePermission, this);
                Puts($" Register Perm: {config.usePermission}");
            }
            if (!config.chatCommand.Equals(regChatCommand, StringComparison.OrdinalIgnoreCase)) {
                cmd.RemoveChatCommand(regChatCommand, this);
                cmd.RemoveConsoleCommand(regChatCommand, this);
                Puts($" Primary Command: {regChatCommand} -> {config.chatCommand}");
                regChatCommand = config.chatCommand;
                cmd.AddChatCommand(regChatCommand, this, Cmd);
                cmd.AddConsoleCommand(regChatCommand, this, CCmd);
            }
            if (config.refreshTimeSeconds < 0.25f) config.refreshTimeSeconds = 0.25f;
            if (config.refreshTimeSeconds > 5f) config.refreshTimeSeconds = 5f;

            if (!(PositionUI._timer?.Destroyed ?? true)) PositionUI._timer.DestroyToPool();
            if (PositionUI.activePlayers.Count > 0) PositionUI._timer = _plugin.timer.Every(config.refreshTimeSeconds, PositionUI.UpdateAll);

            if (!(RateLimiter._timer?.Destroyed ?? true)) RateLimiter._timer.DestroyToPool();
            RateLimiter._timer = timer.Every(config.refillRateInSeconds, RateLimiter.OnTimer);
        }
        #endregion
        #region Lang
        protected override void LoadDefaultMessages() {
            lang.RegisterMessages(new Dictionary<string, string>() {
                ["noPermission"] = "Sorry, permission '{0}' is required, to use this command.",
                ["openText"] = "{0} v{1} by {2}.   To Copy, type in chat, click here, press Ctrl+C",
                ["locText"] = "{4}    {0} ({1:F0}°, {2:F}m) {3}",
                ["rateLimitKick"] = "RateLimiter: Excessive Limit ({2}) reached [{0}/{1}].",
                ["rateLimitReached"] = "[{0}/{1}({2})] Rate Limit Reached. You may try again in {3}.",
                ["HudPanelColor"] = "0.25 0.31 0.31 0.8",
                ["HudTextColor"] = "0 0.8 0.5 0.95",
                ["HudAnchorMin"] = "0.344 0.004",
                ["HudAnchorMax"] = "0.641 0.022",
            }, this, lang.GetServerLanguage());
        }
        internal static Dictionary<string, string> _langMessages = null;
        internal string LangMessage(string langKey, params object[] args) {
            if (!_langMessages.ContainsKey(langKey)) {
                return $"Language Error: Key \"{langKey}\" not found!";
            }
            string msg = _langMessages[langKey];
            if (args != null) {
                try {
                    msg = string.Format(msg, args);
                } catch (Exception ex) {
                    PrintError($"LangMessage({langKey},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")}): Message \"{msg}\", Exception {ex}");
                    return $"Internal Exception: Message \"{msg}\"";
                }
            }
            return msg;
        }
        internal static string FormatDuration(int duration) => $"{duration / 3600:00}h:{(duration / 60) % 60:00}m:{duration % 60:00}s";
        #endregion
        #region Console/Chat Commands
        internal bool CCmd(ConsoleSystem.Arg arg) {
            BasePlayer player = arg.Player();
            if (player == null) return false;
            Cmd(player, string.Empty, arg.Args);
            return true;
        }
        internal void Cmd(BasePlayer player, string _, string[] Args) {
            if (RateLimiter.AddTokenAndTestLimit(player)) return;
            if (config.requirePermission && !permission.UserHasPermission(player.UserIDString, config.usePermission)) {
                if (PositionUI.Users.Remove(player.userID)) { PositionUI.Cmd_Close(player); PositionUI.usersDirty = true; } // catch for permissions changes preventing players from closing the ui
                player.ChatMessage(LangMessage("noPermission", config.usePermission));
                return;
            }
            string a = string.Empty;
            if (Args.Length > 0) a = Args[0];
            switch (a) {
                case "close": PositionUI.Cmd_Close(player); break;
                case "update": PositionUI.Cmd_Update(player); break;
                case "show": PositionUI.Cmd_Show(player); break;
                default: PositionUI.Cmd_Toggle(player); break;
            }
        }
        #endregion
        #region UI
        internal class PositionUI {
            private static string hud = string.Empty;
            private static CuiElement updateElement = null;
            internal static Timer _timer = null;
            internal class User {
                public ulong id;
                public bool hasUI = false;
                [JsonIgnore] public Vector3 pos;
                [JsonIgnore] public MonumentInfo monument = null;
                public User(ulong id, Vector3 pos) {
                    this.id = id; this.pos = pos;
                }
                internal void UpdateMonument() {
                    float lastDist = float.MaxValue;
                    if (monument != null) lastDist = (pos - monument.transform.position).magnitude;
                    foreach (var m in TerrainMeta.Path.Monuments) {
                        float dist = (pos - m.transform.position).magnitude;
                        if (lastDist > dist) { lastDist = dist; monument = m; }
                    }
                }
                internal bool UpdatePosition(Vector3 newPos) {
                    if ((pos - newPos).magnitude > 0.01f) { pos = newPos; return true; }
                    return false;
                }
                internal void Update(BasePlayer player) {
                    if (UpdatePosition(player.transform.position)) {
                        Vector3 relPos = monument.transform.InverseTransformPoint(pos);
                        Vector3 vector = monument.transform.position - pos;
                        float heading = Mathf.Atan2(vector.x, vector.z) * 57.29578f - player.transform.eulerAngles.y;
                        if (heading < 0f) heading += 360f;
                        (updateElement.Components[0] as CuiInputFieldComponent).Text = _plugin.LangMessage("locText", monument.displayPhrase.english, heading, vector.magnitude, relPos, pos);
                        CuiHelper.AddUi(player, new CuiElementContainer { updateElement });
                    }
                }
            }
            internal static Dictionary<ulong, User> Users = new();
            internal static readonly HashSet<ulong> activePlayers = new();
            internal static bool usersDirty = false;
            public static void LoadUsers() {
                try { Users = Interface.Oxide.DataFileSystem.GetFile("PosDisplay")?.ReadObject<Dictionary<ulong, User>>(); } catch (Exception ex) { _plugin.PrintError($"Failed to load PosDisplay.json: {ex}"); }
                if (Users == null) { Users = new(); usersDirty = true; } else foreach (var user in Users) if (BasePlayer.TryFindByID(user.Key, out BasePlayer player)) Cmd_Show(player);
            }
            public static void SaveUsers() { if (usersDirty) { Interface.Oxide.DataFileSystem.GetFile("PosDisplay").WriteObject(Users); usersDirty = false; } }
            internal static void BuildUI() {
                updateElement = new CuiElement { Name = "PosDisplay_HUD_Loc", Parent = "PosDisplay_HUD", Components = { new CuiInputFieldComponent { Color = _plugin.LangMessage("HudTextColor"), FontSize = 9, Align = TextAnchor.MiddleCenter, IsPassword = false, Text = _plugin.LangMessage("openText", _plugin.Name, _plugin.Version, _plugin.Author) }, new CuiRectTransformComponent { AnchorMin = "0 -1", AnchorMax = "0.925 2" } } };
                CuiElementContainer elements = new() { };
                elements.Add(new CuiPanel { Image = { Color = _plugin.LangMessage("HudPanelColor") }, RectTransform = { AnchorMin = _plugin.LangMessage("HudAnchorMin"), AnchorMax = _plugin.LangMessage("HudAnchorMax") } }, config.hudParentName, "PosDisplay_HUD");
                elements.Add(updateElement);
                elements.Add(new CuiButton { Button = { Command = "posdisplay update", Color = "0.31 0.31 0.31 0.7" }, RectTransform = { AnchorMin = "0.93 0.1", AnchorMax = "0.96 0.9" }, Text = { Text = $"_", Color = "1 1 1 0.9", FontSize = 9, Align = TextAnchor.MiddleCenter } }, "PosDisplay_HUD", "PosDisplay_HUD_ReloadBtn");
                elements.Add(new CuiButton { Button = { Command = "posdisplay close", Color = "0.31 0.31 0.31 0.7" }, RectTransform = { AnchorMin = "0.965 0.1", AnchorMax = "0.995 0.9" }, Text = { Text = $"Ⓧ", Color = "1 1 1 0.9", FontSize = 9, Align = TextAnchor.MiddleCenter } }, "PosDisplay_HUD", "PosDisplay_HUD_CloseBtn");
                hud = CuiHelper.ToJson(elements);
                (updateElement.Components[0] as CuiInputFieldComponent).FontSize = 8;
                updateElement.Update = true;
            }
            internal static void CloseAll() {
                if (!(_timer?.Destroyed ?? true)) _timer.DestroyToPool();
                foreach (var user in activePlayers) if (BasePlayer.TryFindByID(user, out BasePlayer player)) CuiHelper.DestroyUi(player, "PosDisplay_HUD");
                activePlayers.Clear();
            }

            internal static void UpdateAll() { foreach (var user in activePlayers) if (BasePlayer.TryFindByID(user, out BasePlayer player) && Users.TryGetValue(user, out User u)) u.Update(player); }
            internal static void Cmd_Close(BasePlayer player) {
                CuiHelper.DestroyUi(player, "PosDisplay_HUD");
                if (activePlayers.Contains(player.userID)) {
                    activePlayers.Remove(player.userID);
                    if (activePlayers.Count == 0) _timer.DestroyToPool();
                    Users[player.userID].hasUI = false;
                    usersDirty = true;
                }
            }
            internal static void Cmd_Update(BasePlayer player) { if (activePlayers.Contains(player.userID)) { Users[player.userID].UpdateMonument(); Users[player.userID].pos.x += 1f; } }
            internal static void Cmd_Toggle(BasePlayer player) { if (activePlayers.Contains(player.userID)) Cmd_Close(player); else Cmd_Show(player); }
            internal static void Cmd_Show(BasePlayer player) {
                if (!activePlayers.Contains(player.userID)) {
                    if (config.requirePermission && !_plugin.permission.UserHasPermission(player.UserIDString, config.usePermission)) return;
                    if (!Users.TryGetValue(player.userID, out User _user)) {
                        _user = Users[player.userID] = new(player.userID, player.transform.position);
                    } else _user.UpdatePosition(player.transform.position);
                    _user.hasUI = true;
                    usersDirty = true;
                    _user.UpdateMonument();
                    CuiHelper.AddUi(player, hud);
                    if (_timer?.Destroyed ?? true) _timer = _plugin.timer.Every(config.refreshTimeSeconds, UpdateAll);
                }
            }
        }
        #endregion
        #region Rate Limiter
        public static class RateLimiter {
            private static readonly Dictionary<ulong, Token> _users = new();
            private class Token { public int count = 1; public DateTime nextLimit = DateTime.MinValue; public bool TryMessage() { if (DateTime.UtcNow >= nextLimit) { nextLimit = DateTime.UtcNow.AddSeconds(2); return true; } return false; } }
            private static readonly List<ulong> purgeList = new();
            internal static Timer _timer = null;
            internal static void OnTimer() {
                foreach (var i in _users) {
                    if (i.Value.count < 1) purgeList.Add(i.Key);
                    else i.Value.count--;
                }
                foreach (ulong i in purgeList) _users.Remove(i);
                purgeList.Clear();
            }
            public static bool AddTokenAndTestLimit(BasePlayer player) {
                if (config.rateLimit > 0) {
                    Token _t;
                    if (_users.ContainsKey(player.userID)) { _t = _users[player.userID]; _t.count++; } else { _t = _users[player.userID] = new(); }
                    if (_t.count >= config.rateLimit) {
                        if (config.excessiveLimit > 0 && _t.count >= config.excessiveLimit) {
                            player.Kick(_plugin.LangMessage("rateLimitKick", _t.count, config.rateLimit, config.excessiveLimit));
                            return true;
                        }
                        if (_users[player.userID].TryMessage()) player.ChatMessage(_plugin.LangMessage("rateLimitReached", _t.count, config.rateLimit, config.excessiveLimit, FormatDuration((int)(_timer.Delay * ((_t.count - config.rateLimit) + 1)))));
                        return true;
                    }
                }
                return false;
            }
            public static void Destroy() {
                if (_timer != null && !_timer.Destroyed) _timer.Destroy();
                _users.Clear();
                purgeList.Clear();
            }
        }
        #endregion
    }
}
//309