# Yonneh's Rust Plugins.
- Oxide dependencies: https://umod.org/games/rust



**UNDER CONSTRUCTION**
Plugins here are under development, not intended for production use. The released version will be on uMod.

# Rust Plugins Watcher v1.0.0.
- Compile in VS2022, and run; or download the .zip, extract, and run.
- This is an extremely simple desktop program; that monitors `*.cs` in the `Source` folder; and Copies them to the `Destination` folder, overwriting existing files, when they are changed.
- After a file is copied, it will not copy another for 2 seconds.
- It will refuse to copy an empty (0 byte) file.
- `Copy All` copies `*.cs` from `Source`, to `Destination`; overwriting existing files.
- it keeps a `Last Change` filename and timestamp on the UI, so you can see when it last copied a file.
- Pretty handy for Rust Plugin Development- automatically copies the .cs to a test server, when you save it. I'm sure there are other uses- for that, the source is included, so you can change the file types/logic/whatever you want.
- In the future, updated versions may be found at `https://gitlab.com/commandff/RustPlugins/`.


# BuildingShop
BuildingShop is a Rust Entity Limit Plugin, with an integrated shop to allow people to exchange a bit of extra scrap/ServerRewards/Economics for extended limits.


## Under Construction
- This is an Initial Draft of BuildingShop. The plugin is functionally complete; but still has a couple of rough edges. I welcome ANY feedback I can get. If there is a feature you would like to see, found one of my mistakes, or just have questions- I'm '`@yonneh.` on discord, https://discord.gg/oxide
- BuildingShop still needs proper shake-down testing, to see where the duct tape breaks. I have only tested it on a local server, with 2 players, and 400k entities (mostly pasted bases)

## Repository
- for some reason, I can not select GitLab as a repo option. The most current bugs are available at https://gitlab.com/commandff/RustPlugins/-/blob/master/BuildingShop.cs


## UI



### Limits Tab
![](https://i.imgur.com/oqAO5FD.png)



### Placed Tab
![](https://i.imgur.com/zYE25pw.png)
- Lists all entities placed by the player. This is cached data (There is no lookup here)



### Admin Tab
![](https://i.imgur.com/dZ9fKz8.png)



### Purchase Option Dialog
![](https://i.imgur.com/oVJGGtm.png)
todo- completely rewrite this.
Gives the user the option to extend their limits, when they attempt to place an entity and they are at or over their current limit.



## Commands
- There are a few. Most of BuildingShop's commands are accessed from the UI.

### /bshop
- opens the Building Shop UI

### /bshop buy <shortname> [qty]

### /bshop rebuildcache (adminPermission)
- wipes, and rebuilds the entire player entity cache.
- This needs to be done after changes to the limits groups, to re-calculate the placed #, but is not done automatically; because it can take up to a second on lower end servers.

### /bshop wipepurchases (adminPermission)
- Moves BuildingShop/Purchases.json to BuildingShop/Purchases.bak, and wipes the in-memory cache.

### /bshop refund [userID]
- if used with adminPermission, refunds 100% to userID.

### /bshop unwipepurchases (adminPermission)
- attempts to restore the most recently wiped purchases from Purchases.bak

### /bshop admin (owner/moderator)
- Toggles between granting and revoking adminPermission

### /bshop reloadconfig (adminPermission)
### /bshop loaddefaultconfig (adminPermission)
### /bshop help

### /bshop perm ... (adminPermission)
- These commands change the `permissions` section of the config.
#### /bshop perm <permName>
#### /bshop perm new <permName>
#### /bshop perm delete <permName>
#### /bshop perm priority <permName> <priority>
#### /bshop perm globalLimit <permName> <globalLimit>
#### /bshop perm revoke <userID>
#### /bshop perm grant <userID> <permName>
#### /bshop perm <permName> new <entName>
#### /bshop perm <permName> <entName> delete
#### /bshop perm <permName> <entName> free <newFree>
#### /bshop perm <permName> <entName> max <-1|0|newMax>
#### /bshop perm <permName> <entName> groupContribution <newGroupContribution>
#### /bshop perm <permName> <entName> price <newBasePrice>
#### /bshop perm <permName> <entName> multiplier <newMultiplier>
#### /bshop perm <permName> <entName> group [<newGroupName> [groupContribution]]

### /bshop config ... (adminPermission)
- These commands change the core config settings.
#### /bshop config adminPermission <newPermissionName>
#### /bshop config command <newCommand>
#### /bshop config commands <add|delete> <command>
#### /bshop config wipePurchases <true|false>
#### /bshop config currency <0|1|2|itemID>
#### /bshop config currencyName <currencyName>
#### /bshop config infinity <infinity>
#### /bshop config refundMultiplier <refundMultiplier>
#### /bshop config sound <soundName> <assetPath>
#### /bshop config lut <entityName> <itemName>

### /bshop ui ... (requires UI to be open)
- The following ui commands are used exclusively from the UI, for changing pages/opening dropdowns/etc.
- They quietly fail if you do not have the UI open.
- /bshop ui limit
- /bshop ui limit <firstIndex>
- /bshop ui placed
- /bshop ui placed <firstIndex>
- /bshop ui adm
- /bshop ui adm <firstIndex>
- /bshop ui adm perm
- /bshop ui adm perm show <anchor>
- /bshop ui adm perm show2 <anchor>
- /bshop ui adm perm showDDCommands <anchor>
- /bshop ui adm perm limit <perm>
- /bshop ui adm toggleGroup

## Config
### "Admin Permission"
- String, Default: "BuildingShop.admin"
- Name of the permission required to access the Admin functions of this plugin. This may be set to an existing permission, from another plugin.

### "Command"
- String, Default: "bshop"
- Primary Chat and Console Command
- This command needs to be unique; as it is used internally for the UI. "limit", and several other short commands conflict with existing console commands.

### "Commands"
- List<string>, Default: limit, limits
- Additional chat-only commands, to redirect other common commands to the BuildingShop UI

### "ItemID for purchase costs"
- int, Default: -932201673 (scrap)
- Sets the currency used.
- 0 Disables all purchases
- 1 ServerRewards
- 2 Economics
- Any other int: ItemDefinition itemID (`Identifier` from https://rustlabs.com/item/scrap)

### "Currency Name for purchase costs"
- String, Default: "Scrap"
- Appended to purchase costs

### "Wipe Purchases with map?"
- Boolean, Default: false

### "Value of infinity"
- int, Default: 50
- This is used in place of LimitEntry.PriceCurve.max, when the max is -1.
- Sets the Buy Max button #
- Gives an ending point for bar graphs



### "Non Admin Refund Multiplier"
- double, Default 0.25
- 0 disables non-admin refunds
- any number above 1 will just multiply their currency- don't do that :P
- Refunds issued by admins (to themselves, or other players) are always 100% (make sure you have the adminPermission granted first, `/bshop admin` to toggle!)



### Sounds
- Dictionary<string,string>
  - Setting these to `""` these will disable the sound. https://github.com/OrangeWulf/Rust-Docs/blob/master/Extended/Effects.md
  - "purchase"
    - Default: "assets/prefabs/deployable/vendingmachine/effects/vending-machine-purchase-human.prefab",
  - "openUITabLimits"
    - Default: "assets/prefabs/building/wall.frame.shopfront/effects/metal_transaction_complete.prefab",
  - "openUITabPlaced"
    - Default: "assets/prefabs/npc/autoturret/effects/autoturret-deploy.prefab",
  - "openUITabAdmin"
    - Default: "assets/prefabs/npc/patrol helicopter/effects/rocket_fire.prefab",
  - "overLimit"
    - Default: "assets/prefabs/npc/autoturret/effects/targetacquired.prefab",
  - "overLimitSorry"
    - Default: "assets/prefabs/locks/keypad/effects/lock.code.denied.prefab"



### Permissions
- Dictionary<string, PermissionEntry>
- See Default Config.
- These may be foreign permissions.



### PermissionEntry
- "Priority", int, default -1
- "Global Limit", int, default -1
- "Limits", Dictionary<string, LimitEntry>



### LimitEntry
- Limit Entry name may be one of:
  - Building Block shortname
  - Entity Short Name
  - Group Name (beginning with a capital letter, no spaces)
- "Price", PriceCurve?, default null
  - Sets the limits/prices for this entry
  - Ignored if "Group" is not null
- "Group", string?, default null
  - Adds this entity to the given group
- "Group Contribution", int?, default null
  - if set, each entity placed will deduct this # from the players limit.
  - This is to "weight" grouped items; see Planting.Slots/Furnaces in the degug example.
- "Group Icon", string?, default null
  - TODO: Store group members inside of the group as a List<KeyValuePair<string,int?>> This is getting messy. Bad yonneh.
  - If set, this icon will be displayed in place of the default cart asset, for groups.



### PriceCurve
- "basePrice", double
  - Base Price
- "multiplier", double
  - Per-Level Multiplier. Each purchase will multiply the previous purchase price by this number.
- "free", int
  - Number of "Free" entities of this type, that may be placed, without paying.
- "max", int
  - Number of times a player may purchase this entity.
  - -1 = Infinity
  - 0 = Not purchaseable



### "Lookup Table"
- Dictionary<string, string>
- map of partial BaseEntity.ShortPrefabName, to ItemDefinition.shortname
- all Vanilla items should be added by default. This is for Update-day fun, with new items.



### "UI Colors"
- Dictionary<string, string>
- Why would you ever want to change the colors???? :gasp:



### "Building Blocks"
- Dictionary<string, KeyValuePair<string, string>>
- Should be mostly static data, but it has been pushed out to the config, for Update-day fun, with new building blocks.

## Default Config

```json
{
  "Admin Permission": "BuildingShop.admin",
  "Commands": [
    "limit",
    "limits"
  ],
  "Command": "bshop",
  "ItemID for purchase costs": -932201673,
  "Currency Name for purchase costs": "Scrap",
  "Wipe Purchases with map?": false,
  "Value of infinity": 50,
  "Non Admin Refund Multiplier": 0.25,
  "Sounds": {
    "purchase": "assets/prefabs/deployable/vendingmachine/effects/vending-machine-purchase-human.prefab",
    "openUITabLimits": "assets/prefabs/building/wall.frame.shopfront/effects/metal_transaction_complete.prefab",
    "openUITabPlaced": "assets/prefabs/npc/autoturret/effects/autoturret-deploy.prefab",
    "openUITabAdmin": "assets/prefabs/npc/patrol helicopter/effects/rocket_fire.prefab",
    "overLimit": "assets/prefabs/npc/autoturret/effects/targetacquired.prefab",
    "overLimitSorry": "assets/prefabs/locks/keypad/effects/lock.code.denied.prefab"
  },
  "Permissions": {
    "BuildingShop.default": {
      "Priority": 0,
      "Global Limit": 500,
      "Limits": {
        "Foundations": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 50,
            "max": 25
          },
          "Group Icon": "assets/prefabs/building core/foundation/foundation.png"
        },
        "Smelting.Slots": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.5,
            "free": 10,
            "max": 10
          },
          "Group Icon": ""
        },
        "Planting.Slots": {
          "Price": {
            "basePrice": 20.0,
            "multiplier": 1.0249999761581421,
            "free": 18,
            "max": 108
          },
          "Group Icon": ""
        },
        "autoturret": {
          "Price": {
            "basePrice": 1250.0,
            "multiplier": 4.0,
            "free": 0,
            "max": 3
          }
        },
        "generator.wind.scrap": {
          "Price": {
            "basePrice": 250.0,
            "multiplier": 15.0,
            "free": 2,
            "max": 2
          }
        },
        "electric.solarpanel.large": {
          "Price": {
            "basePrice": 200.0,
            "multiplier": 2.0,
            "free": 0,
            "max": 3
          }
        },
        "industrial.conveyor": {
          "Price": {
            "basePrice": 110.0,
            "multiplier": 3.0,
            "free": 5,
            "max": 4
          }
        },
        "storageadaptor": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.0499999523162842,
            "free": 75,
            "max": 100
          }
        },
        "electric.splitter": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.5,
            "free": 10,
            "max": 10
          }
        },
        "cupboard.tool": {
          "Price": {
            "basePrice": 2000.0,
            "multiplier": 2.0,
            "free": 2,
            "max": 3
          }
        },
        "workbench1": {
          "Price": {
            "basePrice": 100.0,
            "multiplier": 2.0,
            "free": 1,
            "max": 1
          }
        },
        "workbench2": {
          "Price": {
            "basePrice": 250.0,
            "multiplier": 2.0,
            "free": 0,
            "max": 2
          }
        },
        "workbench3": {
          "Price": {
            "basePrice": 1000.0,
            "multiplier": 2.0,
            "free": 0,
            "max": 2
          }
        },
        "foundation": {
          "Group": "Foundations"
        },
        "foundation.triangle": {
          "Group": "Foundations"
        },
        "furnace": {
          "Group": "Smelting.Slots",
          "Group Contribution": 2
        },
        "furnace.large": {
          "Group": "Smelting.Slots",
          "Group Contribution": 5
        },
        "electric.furnace": {
          "Group": "Smelting.Slots",
          "Group Contribution": 3
        },
        "planter.small": {
          "Group": "Planting.Slots",
          "Group Contribution": 3
        },
        "bathtub.planter": {
          "Group": "Planting.Slots",
          "Group Contribution": 3
        },
        "planter.large": {
          "Group": "Planting.Slots",
          "Group Contribution": 9
        },
        "minecart.planter": {
          "Group": "Planting.Slots",
          "Group Contribution": 2
        },
        "rail.road.planter": {
          "Group": "Planting.Slots",
          "Group Contribution": 9
        }
      }
    },
    "BuildingShop.vip": {
      "Priority": 1,
      "Global Limit": 2000,
      "Limits": {
        "Foundations": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 200,
            "max": -1
          }
        },
        "Smelting.Slots": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.5,
            "free": 20,
            "max": 20
          }
        },
        "Planting.Slots": {
          "Price": {
            "basePrice": 20.0,
            "multiplier": 1.0249999761581421,
            "free": 108,
            "max": 216
          }
        },
        "autoturret": {
          "Price": {
            "basePrice": 1250.0,
            "multiplier": 4.0,
            "free": 3,
            "max": 3
          }
        },
        "generator.wind.scrap": {
          "Price": {
            "basePrice": 250.0,
            "multiplier": 15.0,
            "free": 4,
            "max": 2
          }
        },
        "electric.solarpanel.large": {
          "Price": {
            "basePrice": 200.0,
            "multiplier": 2.0,
            "free": 6,
            "max": 5
          }
        },
        "industrial.conveyor": {
          "Price": {
            "basePrice": 110.0,
            "multiplier": 3.0,
            "free": 10,
            "max": 6
          }
        },
        "storageadaptor": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.0499999523162842,
            "free": 200,
            "max": 200
          }
        },
        "electric.splitter": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.5,
            "free": 30,
            "max": 20
          }
        },
        "cupboard.tool": {
          "Price": {
            "basePrice": 2000.0,
            "multiplier": 2.0,
            "free": 5,
            "max": 8
          }
        },
        "workbench1": {
          "Price": {
            "basePrice": 100.0,
            "multiplier": 2.0,
            "free": 5,
            "max": 4
          }
        },
        "workbench2": {
          "Price": {
            "basePrice": 250.0,
            "multiplier": 2.0,
            "free": 4,
            "max": 5
          }
        },
        "workbench3": {
          "Price": {
            "basePrice": 1000.0,
            "multiplier": 2.0,
            "free": 4,
            "max": 5
          }
        },
        "foundation": {
          "Group": "Foundations"
        },
        "foundation.triangle": {
          "Group": "Foundations"
        },
        "furnace": {
          "Group": "Smelting.Slots",
          "Group Contribution": 2
        },
        "furnace.large": {
          "Group": "Smelting.Slots",
          "Group Contribution": 5
        },
        "electric.furnace": {
          "Group": "Smelting.Slots",
          "Group Contribution": 3
        },
        "planter.small": {
          "Group": "Planting.Slots",
          "Group Contribution": 3
        },
        "bathtub.planter": {
          "Group": "Planting.Slots",
          "Group Contribution": 3
        },
        "planter.large": {
          "Group": "Planting.Slots",
          "Group Contribution": 9
        },
        "minecart.planter": {
          "Group": "Planting.Slots",
          "Group Contribution": 2
        },
        "rail.road.planter": {
          "Group": "Planting.Slots",
          "Group Contribution": 9
        }
      }
    },
    "BuildingShop.nolimit": {
      "Priority": 999,
      "Global Limit": -1,
      "Limits": {}
    },
    "BuildingShop.debug": {
      "Priority": 9999,
      "Global Limit": 200,
      "Limits": {
        "Floors": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 200,
            "max": 100
          }
        },
        "Stairs": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 200,
            "max": 100
          }
        },
        "Foundations": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.0,
            "free": 200,
            "max": -1
          }
        },
        "Roofs": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 200,
            "max": 100
          }
        },
        "Walls": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 200,
            "max": 100
          }
        },
        "floor": {
          "Group": "Floors"
        },
        "floor.frame": {
          "Group": "Floors"
        },
        "floor.triangle": {
          "Group": "Floors"
        },
        "floor.triangle.frame": {
          "Group": "Floors"
        },
        "block.stair.lshape": {
          "Group": "Stairs"
        },
        "block.stair.spiral": {
          "Group": "Stairs"
        },
        "block.stair.spiral.triangle": {
          "Group": "Stairs"
        },
        "block.stair.ushape": {
          "Group": "Stairs"
        },
        "foundation.steps": {
          "Group": "Stairs"
        },
        "ramp": {
          "Group": "Stairs"
        },
        "foundation": {
          "Group": "Foundations"
        },
        "foundation.triangle": {
          "Group": "Foundations"
        },
        "roof": {
          "Group": "Roofs"
        },
        "roof.triangle": {
          "Group": "Roofs"
        },
        "wall": {
          "Group": "Walls"
        },
        "wall.doorway": {
          "Group": "Walls"
        },
        "wall.frame": {
          "Group": "Walls"
        },
        "wall.half": {
          "Group": "Walls"
        },
        "wall.low": {
          "Group": "Walls"
        },
        "wall.window": {
          "Group": "Walls"
        },
        "electric.fuelgenerator.small": {
          "Price": {
            "basePrice": 200.0,
            "multiplier": 2.0,
            "free": 2,
            "max": 5
          }
        },
        "electric.generator.small": {
          "Price": {
            "basePrice": 1250.0,
            "multiplier": 8.0,
            "free": 2,
            "max": 5
          }
        },
        "box.repair.bench": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 3,
            "max": 10
          }
        },
        "box.wooden.large": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 3,
            "max": 10
          }
        },
        "autoturret": {
          "Price": {
            "basePrice": 1250.0,
            "multiplier": 1.5,
            "free": 3,
            "max": 3
          }
        },
        "bbq": {
          "Price": {
            "basePrice": 25.0,
            "multiplier": 1.1000000238418579,
            "free": 3,
            "max": 10
          }
        }
      }
    }
  },
  "Lookup Table": {
    "spiderweba": "spiderweb",
    "smallcandleset": "smallcandles",
    "graveyardfence": "wall.graveyard.fence",
    "rustigeegg_d": "rustige_egg_d",
    "rustigeegg_c": "rustige_egg_c",
    "rustigeegg_e": "rustige_egg_e",
    "rustigeegg_b": "rustige_egg_b",
    "rustigeegg_a": "rustige_egg_a",
    "rustigeegg_f": "rustige_egg_f",
    "coffinstorage": "coffin.storage",
    "beartrap": "trap.bear",
    "sam_site_turret_deployed": "samsite",
    "landmine": "trap.landmine",
    "cctv_deployed": "cctv.camera",
    "waterpurifier": "water.purifier",
    "woodbox_deployed": "box.wooden",
    "weaponrack_wide": "gunrack_wide.horizontal",
    "weaponrack_stand": "gunrack_stand",
    "waterbarrel": "water.barrel",
    "vendingmachine": "vending.machine",
    "weaponrack_tall": "gunrack_tall.horizontal",
    "stocking_large_deployed": "stocking.large",
    "sign.small.wood": "sign.wooden.small",
    "stocking_small_deployed": "stocking.small",
    "small_stash_deployed": "stash.small",
    "skull_door_knocker": "skulldoorknocker",
    "sleepingbag_leather_deployed": "sleepingbag",
    "refinery_small_deployed": "small.oil.refinery",
    "repairbench_deployed": "box.repair.bench",
    "researchtable_deployed": "research.table",
    "pookie_deployed": "pookie.bear",
    "railroadplanter": "rail.road.planter",
    "sign.medium.wood": "sign.wooden.medium",
    "sign.large.wood": "sign.wooden.large",
    "sign.huge.wood": "sign.wooden.huge",
    "weaponrack_horizontal": "gunrack.horizontal",
    "hitchtrough": "hitchtroughcombo",
    "weaponrack_single2": "gunrack.single.2.horizontal",
    "weaponrack_single3": "gunrack.single.3.horizontal",
    "weaponrack_single1": "gunrack.single.1.horizontal",
    "double_doorgarland": "xmas.double.door.garland",
    "windowgarland": "xmas.window.garland",
    "doorgarland": "xmas.door.garland",
    "easter_door_wreath_deployed": "easterdoorwreath",
    "xmas_tree": "xmas.tree",
    "christmas_door_wreath_deployed": "xmasdoorwreath",
    "chippyarcademachine": "arcade.machine.chippy",
    "advendcalendar": "xmas.advent",
    "barricade.cover.wood": "barricade.wood.cover",
    "water_catcher_small": "water.catcher.small",
    "icewall": "wall.ice.wall",
    "water_catcher_large": "water.catcher.large",
    "wall.external.high.wood": "wall.external.high",
    "door.hinged.industrial.d": "door.hinged.metal",
    "doorcloser": "door.closer",
    "simplelight": "electric.simplelight",
    "electric.windmill.small": "generator.wind.scrap",
    "splitter": "electric.splitter",
    "small_fuel_generator": "electric.fuelgenerator.small",
    "solarpanel.large": "electric.solarpanel.large",
    "industrialconveyor": "industrial.conveyor",
    "gravestone.stone": "gravestone",
    "gravestone.wood": "woodcross",
    "andswitch.entity": "electric.andswitch",
    "audioalarm": "electric.audioalarm",
    "electrical.blocker": "electric.blocker",
    "button": "electric.button",
    "electricfurnace": "electric.furnace",
    "electrical.heater": "electric.heater",
    "counter": "electric.counter",
    "doorcontroller": "electric.doorcontroller",
    "cabletunnel": "electric.cabletunnel",
    "fluidsplitter": "fluid.splitter",
    "fluidswitch": "fluid.switch",
    "industrial.wall.lamp.green": "industrial.wall.light.green",
    "hbhfsensor": "electric.hbhfsensor",
    "igniter": "electric.igniter",
    "industrialcombiner": "industrial.combiner",
    "industrialcrafter": "industrial.crafter",
    "industrialsplitter": "industrial.splitter",
    "industrial.wall.lamp": "industrial.wall.light",
    "large.rechargable.battery": "electric.battery.rechargable.large",
    "laserdetector": "electric.laserdetector",
    "medium.rechargable.battery": "electric.battery.rechargable.medium",
    "orswitch.entity": "electric.orswitch",
    "poweredwaterpurifier": "powered.water.purifier",
    "pressurepad": "electric.pressurepad",
    "electrical.modularcarlift": "modularcarlift",
    "ptz_cctv_deployed": "ptz.cctv.camera",
    "electrical.random.switch": "electric.random.switch",
    "reactivetarget_deployed": "target.reactive",
    "industrial.wall.lamp.red": "industrial.wall.light.red",
    "rfbroadcaster": "electric.rf.broadcaster",
    "rfreceiver": "electric.rf.receiver",
    "smallrechargablebattery": "electric.battery.rechargable.small",
    "smartalarm": "smart.alarm",
    "smartswitch": "smart.switch",
    "storagemonitor": "storage.monitor",
    "switch": "electric.switch",
    "teslacoil": "electric.teslacoil",
    "generator.small": "electric.generator.small",
    "timer": "electric.timer",
    "xorswitch.entity": "electric.xorswitch",
    "autoturret_deployed": "autoturret",
    "bed_deployed": "bed",
    "water.pump": "waterpump",
    "poweredwaterpurifier.storage": "powered.water.purifier",
    "skulltrophy": "skull.trophy",
    "connectedspeaker": "connected.speaker",
    "romancandle-blue": "firework.romancandle.blue",
    "volcanofirework": "firework.volcano",
    "volcanofirework-red": "firework.volcano.red",
    "romancandle-violet": "firework.romancandle.violet",
    "volcanofirework-violet": "firework.volcano.violet",
    "romancandle": "firework.romancandle.red",
    "mortarorange": "firework.boomer.orange",
    "mortarred": "firework.boomer.red",
    "mortarchampagne": "firework.boomer.champagne",
    "mortarblue": "firework.boomer.blue",
    "mortargreen": "firework.boomer.green",
    "mortarviolet": "firework.boomer.violet",
    "mortarpattern": "firework.boomer.pattern",
    "mining_quarry": "mining.quarry",
    "pump_jack": "pump.jack"
  },
  "UI Colors": {
    "mainBackground": "0 0.25 0.25 0.88",
    "headerBackground": "0 0 0 0.75",
    "titleBackground": "0 0.25 0.25 0.5",
    "contentBackground": "0 0 0 0",
    "rowBackground": "0 0 0 0.8",
    "": "0 0 0 0"
  },
  "Building Blocks": {
    "floor": {
      "Key": "Floor",
      "Value": "assets/prefabs/building core/floor/floor.png"
    },
    "floor.frame": {
      "Key": "Floor Frame",
      "Value": "assets/prefabs/building core/floor.frame/floor.frame.png"
    },
    "floor.triangle": {
      "Key": "Floor Triangle",
      "Value": "assets/prefabs/building core/floor.triangle/floor.triangle.png"
    },
    "floor.triangle.frame": {
      "Key": "Floor Triangle Frame",
      "Value": "assets/prefabs/building core/floor.triangle.frame/floor.triangle.frame.png"
    },
    "foundation": {
      "Key": "Foundation",
      "Value": "assets/prefabs/building core/foundation/foundation.png"
    },
    "foundation.triangle": {
      "Key": "Triangle Foundation",
      "Value": "assets/prefabs/building core/foundation.triangle/foundation.triangle.png"
    },
    "roof": {
      "Key": "Roof",
      "Value": "assets/prefabs/building core/roof/roof.png"
    },
    "roof.triangle": {
      "Key": "Roof Triangle",
      "Value": "assets/prefabs/building core/roof.triangle/roof.triangle.png"
    },
    "block.stair.lshape": {
      "Key": "Stairs L Shape",
      "Value": "assets/prefabs/building core/stairs.l/stairs_l.png"
    },
    "block.stair.spiral": {
      "Key": "Stairs Spiral",
      "Value": "assets/prefabs/building core/stairs.spiral/stairs_spiral.png"
    },
    "block.stair.spiral.triangle": {
      "Key": "Stairs Spiral Triangle",
      "Value": "assets/prefabs/building core/stairs.spiral.triangle/stairs.triangle.spiral.png"
    },
    "block.stair.ushape": {
      "Key": "U Shaped Stairs",
      "Value": "assets/prefabs/building core/stairs.u/stairs_u.png"
    },
    "foundation.steps": {
      "Key": "Steps",
      "Value": "assets/prefabs/building core/foundation.steps/foundation.steps.png"
    },
    "ramp": {
      "Key": "A ramp",
      "Value": "assets/prefabs/building core/ramp/ramp.png"
    },
    "wall": {
      "Key": "Wall",
      "Value": "assets/prefabs/building core/wall/wall.png"
    },
    "wall.doorway": {
      "Key": "Doorway",
      "Value": "assets/prefabs/building core/wall.doorway/wall.doorway.png"
    },
    "wall.frame": {
      "Key": "Wall Frame",
      "Value": "assets/prefabs/building core/wall.frame/wall.frame.png"
    },
    "wall.half": {
      "Key": "Half Wall",
      "Value": "assets/prefabs/building core/wall.half/wall.half.png"
    },
    "wall.low": {
      "Key": "Low Wall",
      "Value": "assets/prefabs/building core/wall.low/wall.third.png"
    },
    "wall.window": {
      "Key": "Window",
      "Value": "assets/prefabs/building core/wall.window/wall.window.png"
    }
  }
}
```
