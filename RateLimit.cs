﻿/*
(Completely standard MIT License:)
Copyright 2023 "Yonneh"
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------------------------------------
23 DEC 2023 - v 1.0.0
- Initial Release.
- Project started.

*/
using System;
using System.Collections.Generic;
using CompanionServer.Handlers;
using UnityEngine;

namespace Oxide.Plugins {
    [Info("RateLimit", "Yonneh", "1.0.0")]
    [Description("Limits chat commands, console commands, and chat messages, all under one limit.")]
    public class RateLimit : RustPlugin {
        private static RateLimit _plugin;
        #region Housekeeping
        internal void Init() {
            _plugin = this;
            LoadDefaultMessages();
        }
        internal void Loaded() {
            _langMessages = lang.GetMessages(lang.GetServerLanguage(), this);
        }
        internal void Unload() {
            Destroy();
        }
        internal object OnPlayerChat(BasePlayer player, string message, ConVar.Chat.ChatChannel channel) {
            if (AddTokenAndTestLimit(player, config.chatMessageWeight)) return false;
            return null;
        }
        internal object OnPlayerCommand(BasePlayer player, string command, string[] args) {
            if (AddTokenAndTestLimit(player, config.commandWeight)) return false;
            return null;
        }
        internal object OnServerCommand(ConsoleSystem.Arg arg) {
            BasePlayer player = arg.Player();
            if (player != null && AddTokenAndTestLimit(player, config.commandWeight)) return false;
            return null;
        }
        #endregion
        #region Config
        private static ConfigData config;
        internal class ConfigData {
            public float commandWeight;
            public float chatMessageWeight;
            public float rateLimit;
            public float excessiveLimit;
            public float refillRateInSeconds;
            public string rateLimitEffect;
        }
        protected override void LoadConfig() {
            base.LoadConfig();
            try {
                config = Config.ReadObject<ConfigData>();
            } catch (Exception ex) {
                PrintError($"Error reading Config: {ex}");
                LoadDefaultConfig();
            }
            ValidateConfig();
        }
        protected override void LoadDefaultConfig() {
            PrintError("Loading Default Config");
            config = new ConfigData {
                commandWeight = 1f,
                chatMessageWeight = 2f,
                rateLimit = 5f,
                excessiveLimit = 25f,
                refillRateInSeconds = 1.25f,
                rateLimitEffect = "assets/prefabs/locks/keypad/effects/lock.code.denied.prefab",
            };
            SaveConfig();
        }
        protected override void SaveConfig() => Config.WriteObject(config);
        private void ValidateConfig() {
            if (config.refillRateInSeconds < 0.1f) config.refillRateInSeconds = 0.1f;
            if (config.refillRateInSeconds > 15f) config.refillRateInSeconds = 15f;
            if (config.rateLimit > 0) {
                if (!(_timer?.Destroyed ?? true)) _timer.DestroyToPool();
                _timer = timer.Every(config.refillRateInSeconds, OnTimer);
            }
            _reusableEffect.pooledString = config.rateLimitEffect;
        }
        #endregion
        #region Lang
        protected override void LoadDefaultMessages() {
            lang.RegisterMessages(new Dictionary<string, string>() {
                ["rateLimitKick"] = "RateLimiter: Excessive Limit ({2}) reached [{0}/{1}].",
                ["rateLimitReached"] = "[{0}/{1}({2})] Rate Limit Reached. You may try again in {3}.",
            }, this, lang.GetServerLanguage());
        }
        internal static Dictionary<string, string> _langMessages = null;
        internal string LangMessage(string langKey, params object[] args) {
            if (!_langMessages.ContainsKey(langKey)) {
                return $"Language Error: Key \"{langKey}\" not found!";
            }
            string msg = _langMessages[langKey];
            if (args != null) {
                try {
                    msg = string.Format(msg, args);
                } catch (Exception ex) {
                    PrintError($"LangMessage({langKey},{(args == null ? "null" : "[" + string.Join(", ", args) + "]")}): Message \"{msg}\", Exception {ex}");
                    return $"Internal Exception: Message \"{msg}\"";
                }
            }
            return msg;
        }
        internal static string FormatDuration(int duration) => $"{duration / 3600:00}h:{(duration / 60) % 60:00}m:{duration % 60:00}s";
        #endregion
        #region Core
        private static Effect _reusableEffect = new Effect();
        private static readonly Dictionary<ulong, Token> _users = new();
        private class Token {
            public float value;
            public DateTime nextLimit = DateTime.MinValue;
            public Token(float value) {
                this.value = value;
            }
            public bool TryMessage() {
                if (DateTime.UtcNow >= nextLimit) {
                    nextLimit = DateTime.UtcNow.AddSeconds(2);
                    return true;
                }
                return false;
            }
        }
        private static readonly List<ulong> purgeList = new();
        internal static Timer _timer = null;
        internal static void OnTimer() {
            foreach (var i in _users) {
                if (i.Value.value < 1) purgeList.Add(i.Key);
                else i.Value.value--;
            }
            foreach (ulong i in purgeList) _users.Remove(i);
            purgeList.Clear();
        }
        public static bool AddTokenAndTestLimit(BasePlayer player, float weight) {
            if (config.rateLimit > 0) {
                if (!_users.TryGetValue(player.userID, out Token _t)) _t = _users[player.userID] = new(weight);
                else _t.value += weight;
                if (_t.value >= config.rateLimit) {
                    if (config.excessiveLimit > 0 && _t.value >= config.excessiveLimit) {
                        player.Kick(_plugin.LangMessage("rateLimitKick", _t.value, config.rateLimit, config.excessiveLimit));
                        return true;
                    }
                    if (_users[player.userID].TryMessage()) player.ChatMessage(_plugin.LangMessage("rateLimitReached", _t.value, config.rateLimit, config.excessiveLimit, FormatDuration((int)(config.refillRateInSeconds * ((_t.value - config.rateLimit) + 1)))));
                    if (config.rateLimitEffect != "") {
                        _reusableEffect.Init(Effect.Type.Generic, player, 0, Vector3.zero, Vector3.forward);
                        _reusableEffect.pooledString = config.rateLimitEffect;
                        EffectNetwork.Send(_reusableEffect, player.net.connection);
                    }
                    return true;
                }
            }
            return false;
        }
        public static void Destroy() {
            if (_timer != null && !_timer.Destroyed) _timer.Destroy();
            _users.Clear();
            purgeList.Clear();
        }
        #endregion
    }
}
//296