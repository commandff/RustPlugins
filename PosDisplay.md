﻿# PosDisplay
- Release Candidate
- PosDisplay is a simple Hud, that displays your player position, nearest monument name, and your current position relative to that monument.

# UI
![](https://i.imgur.com/IEui0xe.png)
- To copy the text:
  - Press `Enter` to open chat, or Press `Tab` to open your Inventory
  - Click on the text, in the PosDisplay UI
  - Press `Ctrl+C`
- UI closes when the player disconnects(sleeps), or dies; and re-opens OnPlayerSleepEnded.
- UI Persistance is stored as a `List<ulong>` in `data/PosDisplay.json`
- `Ⓧ` button closes the UI
- `_` button refreshes the nearest monument

# Commands
- `posdisplay`: Toggle the UI
- `posdisplay show`: Show the UI
- `posdisplay close`: Close the UI
- `posdisplay update`: Update nearest monument
- `bind y posdisplay; bind u "posdisplay update"`

# Config
- requirePermission: Default: `true`
  - if changed to `false`, players will not require permission to use the UI.
- usePermission: Default: `posdisplay.use`
  - if `requirePermission` is true, users need `usePermission`.
- chatCommand: Default: `posdisplay`
- hudParentName: Default: `Overlay`
- refreshTimeSeconds: Default: `0.25`
  - update Interval for the UI. 0.25s - 5s
- rateLimit: Default: `5`. Set to `0` to disable the entire Rate Limiter.
  - if user exceeds `rateLimit` commands, in `rateLimit * refillRateInSeconds`, they will be warned (once per 2 seconds), and the command will be ignored.
- excessiveLimit: Default: `25`. Set to `0`, to disable the excessive Limit check (and subsequent kicks)
  - if user exceeds `excessiveLimit` commands, in `excessiveLimit * refillRateInSeconds`, they will be kicked from the server, and the command will be ignored.
- refillRateInSeconds: Default: `5.0`
  - Token bucket refill time
  - Default Rate Limit Settings:
    - Users can not execute more than 5 commands within 25 seconds.
    - Users will be kicked, if they exceed 25 commands within 125 seconds. (after several `rateLimitReached` messages)

# Lang
  - "noPermission": "Sorry, permission '{0}' is required, to use this command."
    - `{0}`: (string) config.usePermission
  - "openText": "{0} v{1} by {2}.   To Copy, type in chat, click here, press Ctrl+C"
    - `{0}`: (string) _plugin.Name
    - `{1}`: (VersionNumber) _plugin.Version
    - `{2}`: (string) _plugin.Author
  - "locText": "{4}    {0} ({1:F0}°, {2:F}m) {3}"
    - `{0}`: (string) Monument Display Name
    - `{1}`: (int) Player Heading to Monument
    - `{2}`: (float) Player Distance to Monument
    - `{3}`: (Vector3) Player Position Relative to Monument
    - `{4}`: (Vector3) Player Position
  - "rateLimitKick": "RateLimiter: Excessive Limit reached [{0}/{1}]."
    - `{0}`: (int) token count
    - `{1}`: (int) config.rateLimit
    - `{2}`: (int) config.excessiveLimit
  - "rateLimitReached": "[{0}/{1}] Rate Limit Reached. You may try again in (hh:mm:ss): {2:g}"
    - `{0}`: (int) token count
    - `{1}`: (int) config.rateLimit
    - `{2}`: (int) config.excessiveLimit
    - `{3}`: (string) Duration until command would work (hh:mm:ss)
  - "HudPanelColor": "0.25 0.31 0.31 0.8"
  - "HudTextColor": "0 0.8 0.5 0.95"
  - "HudAnchorMin": "0.344 0.004"
  - "HudAnchorMax": "0.641 0.022"

# Files
- `data/PosDisplay.json`, UI Persistance Data File, `List<ulong>`
- `lang/en/PosDisplay.json`, Lang File, `Dictionary<string, string>`
- `config/PosDisplay.json`, Config File
- `plugins/PosDisplay.cs`, Plugin Source